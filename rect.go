/* win32/rect.go */

package win32

import (
	"gitlab.com/win32go/win32/w32"
)

type GoPos struct {
	Right 	int
	Bottom 	int
}

type GoSize struct {
	Right 	int
	Bottom 	int
}

func GoMargin(left int, top int, right int, bottom int) *GoMarginType {
	return &GoMarginType{left, top, right, bottom}
}

type GoMarginType struct {
	Left 	int
	Top 	int
	Right 	int
	Bottom 	int
}

func (m *GoMarginType) Height() int{
	return m.Top + m.Bottom
}

func (m *GoMarginType) Width() int{
	return m.Left + m.Right
}

func GoPadding(left int, top int, right int, bottom int) *GoPaddingType {
	return &GoPaddingType{left, top, right, bottom}
}

type GoPaddingType struct {
	Left 	int
	Top 	int
	Right 	int
	Bottom 	int
}

func (m *GoPaddingType) Height() int{
	return m.Top + m.Bottom
}

func (m *GoPaddingType) Width() int{
	return m.Left + m.Right
}

func GoRect() *GoRectType {
	return &GoRectType{}
}

type GoRectType struct {
	left 	int
	top 	int
	right 	int
	bottom 	int
	width 	int
	height 	int
}

func (r *GoRectType) Bottom() int{
	return r.bottom
}

func (r *GoRectType) ContainsPoint(x, y int) bool {
	return x >= r.left && y >= r.top && x < r.right && y < r.bottom
}

func (r *GoRectType) ContainsRect(rect *GoRectType) bool {
	return rect.X() >= r.left && rect.Y() >= r.top && rect.Right() < r.right && rect.Bottom() < r.bottom
}

func (r *GoRectType) Deflate(left, top, right, bottom int) (*GoRectType) {
	r.left += left
	r.top += top
	r.right -= right
	r.bottom -= bottom
	r.width -= (left + right)
	r.height -= (top + bottom)
	return r
}

func (r *GoRectType) DeductPadding(padding *GoPaddingType) (*GoRectType) {
	var rc *GoRectType = &GoRectType{}
	rc.left = r.left + padding.Left
	rc.top = r.top + padding.Top
	rc.right = r.right - padding.Right
	rc.bottom = r.bottom - padding.Bottom
	rc.width = r.width - padding.Left - padding.Right
	rc.height = r.height - padding.Top - padding.Bottom
	return rc
}

func (r *GoRectType) DeflateRect(rect *GoRectType) (*GoRectType) {
	r.left += rect.left
	r.top += rect.top
	r.right -= rect.right
	r.bottom -= rect.bottom
	r.width -= (rect.left + rect.right)
	r.height -= (rect.top + rect.bottom)
	return r
}

func (r *GoRectType) FromInt(left, top, right, bottom int) (*GoRectType) {
	r.left = left
	r.top = top
	r.right = right
	r.bottom = bottom
	r.width = r.right - r.left
	r.height = r.bottom - r.top
	return r
}

func (r *GoRectType) FromSize(x, y, width, height int) (*GoRectType) {
	r.left = x
	r.top = y
	r.right = x + width
	r.bottom = y + height
	r.width = width
	r.height = height
	return r
}

func (r *GoRectType) FromW32Rect(rect *w32.RECT) (*GoRectType) {
	r.left = int(rect.Left)
	r.top = int(rect.Top)
	r.right = int(rect.Right)
	r.bottom = int(rect.Bottom)
	r.width = r.right - r.left
	r.height = r.bottom - r.top
	return r
}

func (r *GoRectType) Inflate(margin int) {
	r.left -= margin
	r.top -= margin
	r.right += margin
	r.bottom += margin
	r.width += margin * 2
	r.height += margin * 2
}

func (r *GoRectType) Left() int{
	return r.left
}

func (r *GoRectType) SetBottom(bottom int) {
	r.bottom = bottom
	r.height = r.bottom - r.top
}

func (r *GoRectType) SetLeft(left int) {
	r.left = left
	r.width = r.right - r.left
}

func (r *GoRectType) SetRect(rect *GoRectType) {
	r.left = rect.left
	r.top = rect.top
	r.right = rect.right
	r.bottom = rect.bottom
	r.width = rect.width
	r.height = rect.height
}

func (r *GoRectType) SetW32RECT(rect *w32.RECT) {
	r.left = int(rect.Left)
	r.top = int(rect.Top)
	r.right = int(rect.Right)
	r.bottom = int(rect.Bottom)
	r.width = r.right - r.left
	r.height = r.bottom - r.top
}

func (r *GoRectType) SetRight(right int) {
	r.right = right
	r.width = r.right - r.left
}

func (r *GoRectType) SetTop(top int) {
	r.top = top
	r.height = r.bottom - r.top
}

func (r *GoRectType) SetX(x int) {
	r.left = x
	r.height = r.bottom - r.top
}

func (r *GoRectType) SetY(y int) {
	r.top = y
	r.height = r.bottom - r.top
}

func (r *GoRectType) Right() int{
	return r.right
}

func (r *GoRectType) Top() int{
	return r.top
}

func (r *GoRectType) Translate(x int, y int) {
	r.left += x
	r.top += y
	r.right += x
	r.bottom += y
}

func (r *GoRectType) W32Rect() (*w32.RECT) {
	return &w32.RECT{int32(r.left),int32(r.top), int32(r.right), int32(r.bottom)}
}

func (r *GoRectType) X() (int) {
	return r.left
}

func (r *GoRectType) Y() (int) {
	return r.top
}

func (r *GoRectType) Width() (int) {
	return r.width
}

func (r *GoRectType) Height() (int) {
	return r.height
}

func (r *GoRectType) SetHeight(height int) {
	r.bottom = r.top + height
	r.height = height
}

func (r *GoRectType) SetWidth(width int) {
	r.right = r.left + width
	r.width = width
}
