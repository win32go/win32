/* application.go */

package win32

import (
	"log"
	"os"
	"runtime"
	"sync"
	"syscall"
	"unsafe"
	"gitlab.com/win32go/win32/w32"
)


// Window Stack
type goStack struct {
	index 			map[w32.HWND] int
	//Object 			map[w32.HWND] GoObject
	actions         map[int] *GoAction
	windows 		map[int] GoObject
	controls 		map[int] GoObject
	controlfocus	int
	windowfocus 	int
	mu      		sync.Mutex
}

/*func (s *goStack) topWindow() GoObject {
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.windows) == 0 {
		return nil
	}
	return s.windows[0]
}*/

func (s *goStack) addAction(action *GoAction, id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.actions[id] = action
}

func (s *goStack) addControl(control GoObject, id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.controls[id] = control
	s.index[control.wid().hWnd] = id
	s.controlfocus = id
}

func (s *goStack) addWindow(window GoObject, id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.windows[id] = window
	s.index[window.wid().hWnd] = id
	s.windowfocus = id
}

func (s *goStack) focusControl() (control GoObject) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.controls) == 0 {
		return nil
	}
	return s.controls[s.controlfocus]
}

func (s *goStack) focusWindow() (window GoObject) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.windows) == 0 {
		return nil
	}
	return s.windows[s.windowfocus]
}

func (s *goStack) removeAction(id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.actions, id)
}

func (s *goStack) removeControl(id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	control := s.controls[id]
	hWnd := control.wid().hWnd
	delete(s.index, hWnd)
	delete(s.controls, id)
	if len(s.controls) > 0 {
		s.controlfocus = 0
	}
}

func (s *goStack) removeWindow(id int) {
	s.mu.Lock()
	defer s.mu.Unlock()
	window := s.windows[id]
	hWnd := window.wid().hWnd
	delete(s.index, hWnd)
	delete(s.windows, id)
	if len(s.windows) > 0 {
		s.windowfocus = 0
	}
}

func (s *goStack) isLastWindow() (last bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	log.Println("isLastWindow - len*s.windows = ", len(s.windows))
	if len(s.windows) < 1 {
		return true
	}
	return false
}

func (s *goStack) topWindow() (window GoObject) {
	return s.windows[0]

}

type GoWinApplication struct {
	hInstance w32.HINSTANCE
	appName string
	iconName string
	winStack *goStack
	desktop *GoDeskTopWindow
	clipboard *GoClipboard
	goSystemColors
	goSystemMetrics
}

var goApp *GoWinApplication = nil

func GoApp()  (a *GoWinApplication) {
	if goApp != nil {
		return goApp
	}
	return nil
}

func GoApplication(appName string) (a *GoWinApplication) {
	if goApp == nil {
		ghInstance := w32.GetModuleHandle("")
		if appName == "" {
			appName = "Win32Go"
		}
		log.Print("appName:", appName)
		// run application in a single thread to avoid windows system lockups TO BE FIXED!!!
		runtime.LockOSThread()

		iconName := "win32go" //appName + ".ico"
		goApp = &GoWinApplication{ghInstance, appName, iconName, &goStack{}, nil, nil, goSystemColors{}, goSystemMetrics{}}
		_, err := goApp.registerClass(ghInstance, appName, iconName)
		if err != nil {
			log.Println("GoApplication::ERROR registering class")
		}
		// Initialise the window object, control object and action maps, also the widget index map
		goApp.winStack.actions = make(map[int] *GoAction)
		goApp.winStack.controls = make(map[int] GoObject)
		goApp.winStack.windows = make(map[int] GoObject)
		goApp.winStack.index = make(map[w32.HWND] int)
		// Retrieve handle to Windows desktop
		goApp.desktop = GoDeskTop()
		goApp.clipboard = &GoClipboard{}
		// Initialise with system colors
		goApp.setSystemColors()
		goApp.setSystemMetrics()
	}
	return goApp
}

func (a *GoWinApplication) Actions(id int) *GoAction {
	return a.winStack.actions[id]
}

func (a *GoWinApplication) AddAction(action *GoAction, id int) {
	a.winStack.addAction(action , id)
}

func (a *GoWinApplication) AddControl(control GoObject, id int) {
	a.winStack.addControl(control , id)
}

func (a *GoWinApplication) AddWindow(window GoObject, id int) {
	a.winStack.addWindow(window , id)
}

func (a *GoWinApplication) AppName() string {
	return a.appName
}

func (a *GoWinApplication) Control(hWnd w32.HWND) GoObject {
	idx := a.winStack.index[hWnd]
	return a.winStack.controls[idx]

}

func (a *GoWinApplication) Controls(id uintptr) GoObject {
	return a.winStack.controls[int(id)]
}

func (a *GoWinApplication) Exit() {
	for _, window := range a.winStack.windows {
		w32.DestroyWindow(window.wid().hWnd)
	}
}

func (a *GoWinApplication) FocusWindow() GoObject {
	return a.winStack.focusWindow()
}

func (a *GoWinApplication) GoClipBoard() (hClip *GoClipboard) {
	return a.clipboard
}

func (a *GoWinApplication) GoDeskTop() (hWin *GoDeskTopWindow) {
	return a.desktop
}

func (a *GoWinApplication) GoSystemMetrics() (hSysMetrics goSystemMetrics) {
	return a.goSystemMetrics
}

func (a *GoWinApplication) GoSystemColors() (hSysColors goSystemColors) {
	return a.goSystemColors
}

func (a *GoWinApplication) IsLastWindow() bool {
	return a.winStack.isLastWindow()
}

func (a *GoWinApplication) RemoveControl(id int) {
	a.winStack.controls[int(id)].destroy()
	a.winStack.removeControl(id)
}

func (a *GoWinApplication) RemoveWindow(id int) {
	a.winStack.windows[int(id)].destroy()
	a.winStack.removeWindow(id)
}

func (a *GoWinApplication) SetAppName(appName string) {
	a.appName = appName
}

func (a *GoWinApplication) SetIconName(iconName string) {
	a.iconName = iconName
}

func (a *GoWinApplication) SetWindowFocus(id int) {
	a.winStack.windowfocus = id
}

func (a *GoWinApplication) TopWindow() GoObject {
	return a.winStack.topWindow()
}

func (a *GoWinApplication) WorkingDirectory() string {
	path, err := os.Getwd()
	if err != nil {
	    log.Println(err)
	    return ""
	}
	return path
}





func (a *GoWinApplication) ObjectType(object GoObject) (typ string) {
	typ = ""
	switch object.(type) {
		//case *GoMainWindowView:
			//typ = "GoMainWindow"
		//case *GoButtonControl:
			//typ = "GoButton"
	}
	return
}

func (a *GoWinApplication) Run() (int) {
	var msg w32.MSG
	log.Println("running")
	for {
		//log.Println(".")
		msg = w32.MSG{}

		gotMessage, err := w32.GetMessage(&msg, 0, 0, 0)
		if gotMessage == 0 {
			log.Println("Run::wParam:", int(msg.WParam))
			return int(msg.WParam)
		} else if gotMessage == -1 {
			log.Println("GetMessage Error:", gotMessage, err)
			return -1
		}
		
			//log.Println("msg")
		w32.TranslateMessage(&msg)
		w32.DispatchMessage(&msg)
	}
	return 0
}



// Top Level Default Application Proc
func (a *GoWinApplication) DefWinProc(hWnd w32.HWND, msg uint32, wParam uintptr, lParam uintptr) uintptr {
	
	lwParamH := (lParam & 0xFFFF0000) >> 16
	lwParamL := lParam & 0xFFFF
	//lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF

	//mouseButton := int()
	mouseX := int(lwParamL)
	mouseY := int(lwParamH)
	var win GoObject
	var id int
	var ok bool

	if id, ok = a.winStack.index[hWnd]; ok {
		if id < 100 {
			win = a.winStack.windows[id]
		} else {
			win = a.winStack.controls[id]
		}
	}
	if win == nil {
		return w32.DefWindowProc(hWnd, msg, wParam, lParam)
	}
	// win := goApp.FocusWindow() // Window->GoObject which has current windows focus
	
	
	switch msg {
		case w32.WM_ACTIVATE:
			//log.Println("WM_ACTIVATE.....")
			if id < 100 {
				active := wParam != 0
				log.Println("Application::WM_ACTIVATE.....", active)
				if active {
					a.SetWindowFocus(id)
				}
				return 0
			}
			
		case w32.WM_CLOSE:
			log.Println("Application::WM_CLOSE.....")
			w := win.wid()
			if w.onCanClose != nil {
				if w.onCanClose() == false {
					return 0
				}
			}
			//if w.Parent() != nil {
			//	w32.EnableWindow(w.hWndParent, true)
			//	w32.SetForegroundWindow(w.hWndParent)
			//}
			if w.onClose != nil {
				w.onClose()
			}
			
		case w32.WM_COMMAND:
			log.Println("Application::WM_COMMAND - ", id)
			if win.isDialog() {
				log.Println("Window.isDialog()")
			} else {
				log.Println("Window.isNOTDialog()")
			}
			win.onWM_COMMAND(wParam, lParam)
			return 0
			
		case w32.WM_DESTROY:
			log.Println("Application::WM_DESTROY - ", id)
			if id < 100 {
				a.RemoveWindow(id)
			} else {
				a.RemoveControl(id)
			}
			if goApp.IsLastWindow() {
				log.Println("WM_DESTROY::isLastWindow")
				w32.PostQuitMessage(0)
			}
			return 0

		case w32.WM_ERASEBKGND:
			log.Println("Application::WM_ERASEBACKGROUND - ", id)
			//widget := win.wid()
			if win.objectType() == "GoWindowObj" {
				window := win.(*GoWindowObj)
				rect := window.GetClientRect()
				hDC := w32.HDC(wParam)

				window.paintBackground(hDC, rect)
				return 1
			} else if win.objectType() == "GoMessageBoxObj" {
				window := win.(*GoMessageBoxObj)
				rect := window.GetClientRect()
				hDC :=  w32.HDC(wParam)

				window.paintBackground(hDC, rect)
				return 1
			}
			return w32.DefWindowProc(hWnd, msg, wParam, lParam)
		case w32.WM_GETMINMAXINFO:
			//log.Println("Application::WM_GETMINMAXINFO - ", id)

			if win.objectType() == "GoWindowObj" {
				w := win.(*GoWindowObj)
				l := w.layout.wid()
				var pInfo *w32.MINMAXINFO = (*w32.MINMAXINFO)(unsafe.Pointer(lParam))
				if pInfo != nil {
					//log.Println("lwidth =", l.width, "lheight =", l.height)
					if l.SizePolicy().Horiz == ST_FixedWidth {
						winWidth := a.WindowSizeX(l.width + w.padding.Left + w.padding.Right + l.margin.Left + l.margin.Right, true)
						//log.Println("winWidth =", winWidth)
						pInfo.PtMaxTrackSize.X = int32(winWidth - 28) //  - 9
					}
					if l.SizePolicy().Horiz == ST_FixedWidth {
						winHeight := a.WindowSizeY(l.height + w.padding.Top + w.padding.Bottom + l.margin.Top + l.margin.Bottom, true, true)
						//log.Println("winHeight =", winHeight)
						pInfo.PtMaxTrackSize.Y = int32(winHeight - 28) //  - 9
					}
					return 0
				}
			}
				
		case w32.WM_KEYDOWN:
			w := win.wid()
			if w.onKeyDown != nil {
				w.onKeyDown(int(wParam))
				return 0
			}

		case w32.WM_KEYUP:
			w := win.wid()
			if w.onKeyUp != nil {
				w.onKeyUp(int(wParam))
				return 0
			}

		case w32.WM_LBUTTONDOWN, w32.WM_MBUTTONDOWN, w32.WM_RBUTTONDOWN:
			if msg == w32.WM_LBUTTONDOWN {
				log.Println("Application::WM_LBUTTONDOWN - ", id)
			} else if msg == w32.WM_MBUTTONDOWN {
				log.Println("Application::WM_MBUTTONDOWN - ", id)
			} else if msg == w32.WM_RBUTTONDOWN {
				log.Println("Application::WM_RBUTTONDOWN - ", id)
			}
			log.Println("BUTTONDOWN x:", mouseX, " y:", mouseY)
			w := win.wid()
			if w.onMouseDown != nil {
				b := LeftButton
				if msg == w32.WM_MBUTTONDOWN {
					b = MiddleButton
				}
				if msg == w32.WM_RBUTTONDOWN {
					b = RightButton
				}
				w.onMouseDown(b, mouseX, mouseY)
				return 0
			}

		case w32.WM_LBUTTONUP, w32.WM_MBUTTONUP, w32.WM_RBUTTONUP:
			if msg == w32.WM_LBUTTONUP {
				log.Println("Application::WM_LBUTTONUP - ", id)
			} else if msg == w32.WM_MBUTTONUP {
				log.Println("Application::WM_MBUTTONUP - ", id)
			} else if msg == w32.WM_RBUTTONUP {
				log.Println("Application::WM_RBUTTONUP - ", id)
			}
			log.Println("BUTTONUP x:", mouseX, " y:", mouseY)
			w := win.wid()
			if w.onMouseUp != nil {
				b := LeftButton
				if msg == w32.WM_MBUTTONUP {
					b = MiddleButton
				}
				if msg == w32.WM_RBUTTONUP {
					b = RightButton
				}
				w.onMouseUp(b, mouseX, mouseY)
				log.Println("GoApp::return 0....HWnd", w.hWnd)
				return 0
			}
			
		case w32.WM_MOUSEMOVE:
			w := win.wid()
			if w.onMouseMove != nil {
				w.onMouseMove(mouseX, mouseY)
				return 0
			}

		case w32.WM_MOVE:
			//log.Println("Application::WM_MOVE.....")
			/*log.Println("GoObject :", win.objectType())
			log.Println("lParamL :",int(lwParamL))
			log.Println("lParamH :",int(lwParamH))
			xPos := int(lwParamL)
			yPos := int(lwParamH)
			x, y := w32.ClientToScreen(hWnd, xPos, yPos)*/
			winrect := w32.GetWindowRect(hWnd)
			w := win.wid()
			w.Moved(int(winrect.Left), int(winrect.Top))

		case w32.WM_DRAWITEM:
			log.Println("Application::WM_DRAWITEM.......")
			return 0

		case w32.WM_PAINT:
			log.Println("Application::WM_PAINT.....")
			
			if win.objectType() == "GoWindowObj" {
				window := win.(*GoWindowObj)
				//w := window.layout.wid()
				//log.Println("Layout::Height =", w.height)
				var ps w32.PAINTSTRUCT
				var rect *GoRectType
				
				hDC := w32.BeginPaint(window.hWnd, &ps)
				rect = window.GetClientRect()
				//rect.Translate(window.lastHScrollValue, window.lastVScrollValue)
				//w32.SelectObject(hDC, w32.GetStockObject(w32.DC_BRUSH))
	  			//w32.SetDCBrushColor(hDC, w.backcolor.W32Color())
				//w32.Rectangle(hDC, rect.left, rect.top, rect.right, rect.bottom)
				
			log.Println("ClientRect:", rect.left, rect.top, rect.right, rect.bottom)
			//w32.SelectObject(hDC, w32.GetStockObject(w32.DC_BRUSH))
  			//w32.SetDCBrushColor(hDC, w.backcolor.W32Color())
			//w32.Rectangle(hDC, rect.left, rect.top, rect.right, rect.bottom)

				//window.paint(hDC, rect.W32Rect())
				window.paint(hDC, rect)
				//w32.DrawText(hDC, window.text, -1, rect.W32Rect(), w32.DT_LEFT | w32.DT_WORDBREAK)
				w32.EndPaint(window.hWnd, &ps)
				
				//log.Println("window.text = " + window.text)
				//w32.DrawText(window.layout.hDC, window.layout.text, -1, rect.W32Rect(), w32.DT_LEFT | w32.DT_WORDBREAK)
				//w32.DrawText(hDC, window.text, -1, rect.W32Rect(), w32.DT_LEFT | w32.DT_WORDBREAK)
				return 0
			} else if win.objectType() == "GoMessageBoxObj" {
				var ps w32.PAINTSTRUCT
				var rect *GoRectType
				
				window := win.(*GoMessageBoxObj)
				hDC := w32.BeginPaint(window.hWnd, &ps)
				rect = window.GetClientRect()
				window.paint(hDC, rect)
				w32.EndPaint(window.hWnd, &ps)
				return 0
			}
			return w32.DefWindowProc(hWnd, msg, wParam, lParam)
			
		case w32.WM_SIZE:
			log.Println("Application::WM_SIZE.....")
			log.Println("GoObject :", win.objectType())
			w := win.wid()
			log.Println("lParamL :",uint(lwParamL))
			log.Println("lParamH :",uint(lwParamH))
			if win.objectType() == "GoWindowObj" {
				window := win.(*GoWindowObj)
				layout := window.Layout()
				rc := w32.GetClientRect(w.hWnd)
				width := layout.Width()// + window.padding.left + window.padding.right + layout.margin.left + layout.margin.right
				height := layout.Height()// + window.padding.top + window.padding.bottom + layout.margin.top + layout.margin.bottom
				
				/*if layout.sizePolicy.horiz == ST_FixedWidth {

				} else if layout.sizePolicy.vert == ST_FixedHeight {

				}*/
				if layout.sizePolicy.Horiz == ST_ExpandingWidth {
					log.Println("layout.sizePolicy.horiz : ST_ExpandingWidth")
					//w := uint(lwParamL)
					//width = int(w)
					width = int(rc.Right) - window.padding.Left - window.padding.Right - layout.margin.Left - layout.margin.Right
					layout.SetWidth(width)
				}
				if layout.sizePolicy.Vert == ST_ExpandingHeight {
					log.Println("layout.sizePolicy.vert : ST_ExpandingHeight")
					//h := uint(lwParamH)
					//height = int(h)
					height = int(rc.Bottom) - window.padding.Top - window.padding.Bottom - layout.margin.Top - layout.margin.Bottom
					layout.SetHeight(height)
				} //else if layout.sizePolicy.vert == ST_FixedHeight {

				//layout.SetSize(width, height)
				layout.SetPosition(window.padding.Left + layout.margin.Left, window.padding.Top + layout.margin.Top)
				
				
				uright := width + window.padding.Left + window.padding.Right + layout.margin.Left + layout.margin.Right - int(rc.Right)
				if uright > 20 {
					w32.ShowScrollBar(w.hWnd, w32.SB_HORZ, true)
					w32.SetScrollRange(w.hWnd, w32.SB_HORZ, 0, uright, false) //int(uright / 20) + 1, false)
					w.lastHScrollValue = 0
					w32.SetScrollPos(w.hWnd, w32.SB_HORZ, w.lastHScrollValue, true)
				} else {
					w32.ShowScrollBar(w.hWnd, w32.SB_HORZ, false)
				}
				ubottom := height + window.padding.Top + window.padding.Bottom + layout.margin.Top + layout.margin.Bottom - int(rc.Bottom)
				if ubottom > 20 {
					w32.ShowScrollBar(w.hWnd, w32.SB_VERT, true)
					w32.SetScrollRange(w.hWnd, w32.SB_VERT, 0, ubottom, false) //int(ubottom / 20) + 1, false)
					w.lastVScrollValue = 0
					w32.SetScrollPos(w.hWnd, w32.SB_VERT, w.lastVScrollValue, true)
				} else {
					w32.ShowScrollBar(w.hWnd, w32.SB_VERT, false)
				}
			}
			return w32.DefWindowProc(hWnd, msg, wParam, lParam)
			
		case w32.WM_HSCROLL:
			w := win.wid()
			min, max := w32.GetScrollRange(w.hWnd, w32.SB_HORZ)
			switch wParam {
				case w32.SB_THUMBPOSITION:
					newScrollValue := int(lParamL)
					if newScrollValue != w.lastHScrollValue {
						w32.SetScrollPos(w.hWnd, w32.SB_HORZ, newScrollValue, true)
						w32.ScrollWindow(w.hWnd, 20 * (newScrollValue - w.lastHScrollValue), 0, nil, nil)
						//w32.ScrollWindowEx(w.hWnd, 0, 20 * (newScrollValue - w.lastVScrollValue), nil, nil, 0, nil, w32.SW_ERASE | w32.SW_INVALIDATE)
						w.lastHScrollValue = newScrollValue
					}
				case w32.SB_LINEDOWN:
					if w.lastHScrollValue + 20 <= max {
						w.lastHScrollValue = w.lastHScrollValue + 20
						w32.SetScrollPos(w.hWnd, w32.SB_HORZ, w.lastHScrollValue, true)
						w32.ScrollWindow(w.hWnd, -20, 0, nil, nil)
					}
				case w32.SB_LINEUP:
					if w.lastHScrollValue - 20 >= min {
						w.lastHScrollValue = w.lastHScrollValue - 20
						w32.SetScrollPos(w.hWnd, w32.SB_HORZ, w.lastHScrollValue, true)
						w32.ScrollWindow(w.hWnd, 20, 0, nil, nil)
					}
			}
			return 0

		case w32.WM_VSCROLL:
			w := win.wid()
			min, max := w32.GetScrollRange(w.hWnd, w32.SB_VERT)
			switch wParam {
				case w32.SB_THUMBPOSITION:
					newScrollValue := int(lParamL)
					if newScrollValue != w.lastVScrollValue {
						w32.SetScrollPos(w.hWnd, w32.SB_VERT, newScrollValue, true)
						w32.ScrollWindow(w.hWnd, 0, 20 * (newScrollValue - w.lastVScrollValue), nil, nil)
						//w32.ScrollWindowEx(w.hWnd, 0, 20 * (newScrollValue - w.lastVScrollValue), nil, nil, 0, nil, w32.SW_ERASE | w32.SW_INVALIDATE)
						w.lastVScrollValue = newScrollValue
					}
				case w32.SB_LINEDOWN:
					if w.lastVScrollValue + 20 <= max {
						w.lastVScrollValue = w.lastVScrollValue + 20
						w32.SetScrollPos(w.hWnd, w32.SB_VERT, w.lastVScrollValue, true)
						w32.ScrollWindow(w.hWnd, 0, -20, nil, nil)	// w32.SW_SCROLLCHILDREN | w32.SW_SMOOTHSCROLL
						//w32.ScrollWindowEx(w.hWnd, 0, -20, nil, nil, 0, nil, w32.SW_ERASE | w32.SW_INVALIDATE)
					}
				case w32.SB_LINEUP:
					if w.lastVScrollValue - 20 >= min {
						w.lastVScrollValue = w.lastVScrollValue - 20
						w32.SetScrollPos(w.hWnd, w32.SB_VERT, w.lastVScrollValue, true)
						w32.ScrollWindow(w.hWnd, 0, 20, nil, nil)
						//w32.ScrollWindowEx(w.hWnd, 0, 20, nil, nil, 0, nil, w32.SW_ERASE | w32.SW_INVALIDATE)
					}
			}
			return 0
	}
	return w32.DefWindowProc(hWnd, msg, wParam, lParam)

}

func (a *GoWinApplication) registerClass(hInstance w32.HINSTANCE, appName string, iconName string) (atom w32.ATOM, err error) {
	var wndClass w32.WNDCLASSEX
	var icon w32.HICON
	var goicon *GoIcon
	var icon_err error
	
	log.Println("Loading Icon from resource......")
	goicon, icon_err = GoIconFromResource(hInstance, iconName)
	if icon_err != nil {
		log.Println("Loading Icon from file..........")
		path := a.WorkingDirectory() + "\\"
		goicon, icon_err = GoIconFromImage(path + appName + ".ico")
		if icon_err != nil {
			log.Println("(*GoWinApplication) registerClass - LoadIcon:", icon_err)
		} else {
			a.iconName = appName + ".ico"
		}
	}
	// log.Println("(*GoWinApplication) registerClass - GoIcon:", goicon)
	
	wndClass = w32.WNDCLASSEX{}
	/* 	Size       uint32
		Style      uint32
		WndProc    uintptr
		ClsExtra   int32
		WndExtra   int32
		Instance   HINSTANCE
		Icon       HICON
		Cursor     HCURSOR
		Background HBRUSH
		MenuName   *uint16
		ClassName  *uint16
		IconSm     HICON
	*/
	wndClass.Style = w32.CS_HREDRAW | w32.CS_VREDRAW
	wndClass.WndProc = syscall.NewCallback(a.DefWinProc)
	wndClass.ClsExtra = 0
	wndClass.WndExtra = 0
	wndClass.Instance = hInstance
	if icon_err == nil {
		wndClass.Icon = goicon.hicon
	} else {
		log.Println("Loading Stock Icon..............")
		icon, icon_err = w32.LoadStockIcon(0, w32.IDI_APPLICATION)
		if icon_err.Error() == "The operation completed successfully." {
			wndClass.Icon = icon
		} else {
			return 0, icon_err
		}
	}
	
	wndClass.Cursor = w32.LoadCursor(0, w32.IDC_ARROW)
	wndClass.Background = w32.HBRUSH(w32.GetStockObject(w32.NULL_BRUSH))
	//wndClass.Background = w32.HBRUSH(w32.GetStockObject(w32.WHITE_BRUSH))
	wndClass.MenuName = syscall.StringToUTF16Ptr(appName)
	wndClass.ClassName = syscall.StringToUTF16Ptr(appName)
	
	wndClass.Size = uint32(unsafe.Sizeof(wndClass))

	atom, err = w32.RegisterClassEx(&wndClass)
	return atom, err
}

func (a *GoWinApplication) repackLayout() {
	
}

