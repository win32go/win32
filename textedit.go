/* win32go/win32/textedit.go */

package win32

import (
	"log"
	//"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoTextEdit(parent GoObject) (hTextEdit *GoTextEditObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 
	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 		"EDIT",
		windowName: 	"",
		style: 			w32.WS_CHILD | w32.ES_AUTOVSCROLL | w32.ES_MULTILINE| w32.ES_LEFT, // | w32.WS_VSCROLL,		//  | w32.SS_WHITEFRAME
		exStyle:    	0,
		state:      	w32.SW_HIDE,
		x:      		0,
		y:      		0,
		width:  		400,
		height: 		300,
		hWndParent:		p_widget.hWnd,
		id:				p_nextid,
		instance:		p_widget.instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		border: 		BorderNone,
		cursor:     	nil,	// *goCursor
		font:       	nil, 	// *GoFont
		margin:			&GoMarginType{2, 2, 2, 2},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 		"",
		window:     	false,
		widgets:    	map[int]*goWidget{},
		alpha:			0, 		//uint8
		backMode: 		OpaqueMode,
		backgroundMode:	OpaqueMode,
		onShow:        	nil, 	//func()
		onClose:       	nil, 	//func()
		onCanClose:    	nil, 	//func() bool
		onMouseMove:   	nil, 	//func(x, y int)
		onMouseWheel:  	nil, 	//func(x, y int, delta float64)
		onMouseDown:   	nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     	nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     	nil, 	//func(key int)
		onKeyUp:       	nil, 	//func(key int)
		onResize:      	nil, 	//func()
	}
	object := goObject{parent, []GoObject{}}
	e := &GoTextEditObj{widget, object, 0, 0x7FFFFFFF, Color_Black, nil}
	
	_, err := e.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(e)
		GoApp().AddControl(e, p_nextid)
	}
	// Subclass Label Window to allow custom painting of textedit
	
	/*w32.SetWindowSubclass(e.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		
		case w32.WM_CTLCOLORSTATIC:
        {
        	log.Println("WM_CTLCOLORSTATIC :*********************************************")
        	//var hbrBkgnd w32.HBRUSH = 0
        	hDC := w32.HDC(wParam)
        	//w32.SelectObject(hDC, w32.GetStockObject(w32.DC_BRUSH))
        	//w32.SetDCBrushColor(hDC, colorToW32(e.backColor))
        	w32.SetTextColor(hDC, colorToW32(e.forecolor))
        	w32.SetBkColor(hDC, colorToW32(e.backcolor))

	        //return w32.GetStockObject( w32.COLOR_3DHILIGHT )
	        return uintptr(w32.GetSysColorBrush( w32.COLOR_3DHILIGHT ))
        }
        case w32.WM_CTLCOLOREDIT:
		{
			log.Println("WM_CTLCOLOREDIT :*************************************************")
			hDC := w32.HDC(wParam) //Get handles
			w32.SetTextColor(hDC, colorToW32(e.forecolor)) // Text color
			w32.SetBkMode(hDC, w32.TRANSPARENT); // EditBox Backround Mode (note: OPAQUE can be used)
			w32.SetBkColor(hDC, colorToW32(e.backcolor)) // Backround color for EditBox
			return uintptr(w32.GetSysColorBrush( w32.COLOR_3DHILIGHT )) // Paint it
		}
			else if(LOWORD(wParam) == IDB_CALCULATE_COLOR_FORE)
			{
			iColorForeGround = RGB(iRed, iGreen, iBlue);
			RECT rc;
			GetClientRect(hDlg, &rc);
			InvalidateRect(hDlg, &rc, true);

			}
			else if(LOWORD(wParam) == IDB_CALCULATE_COLOR_BACK)
			{

			RECT rc;
			GetClientRect(hDlg, &rc);
			InvalidateRect(hDlg, &rc, true);
			hbrushEditBox = CreateSolidBrush(RGB(iRed,iGreen,iBlue));

			}
		default:
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		}
	}), 0, 0)	*/
	if sizeToView == true {
		GoTestView.SizeToControl(e)
	}
	return e
}

type GoTextEditObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	groupID		int
	limit        int
	//autoHScroll  bool
	//writesTabs   bool
	//readOnly     bool
	textColor 	GoColor
	onTextChange func()
}

func (e *GoTextEditObj) Font() *GoFont {
	return e.font
}

func (e *GoTextEditObj) SetFont(font *GoFont) {
	e.font = font
	if e.hWnd != 0 {
		w32.SendMessage(e.hWnd, w32.WM_SETFONT, uintptr(e.fontHandle()), 1)
	}
}

func (e *GoTextEditObj) SetText(text string) {
	e.text = text
	if e.hWnd != 0 {
		w32.SetWindowText(e.hWnd, text)
	}
}

func (e *GoTextEditObj) Show() {
	log.Println("GoTextEditObj.Show()")
	w32.ShowWindow(e.hWnd, w32.SW_SHOW)
	e.state = w32.SW_SHOW
	e.visible = true
	parent := e.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (e *GoTextEditObj) Text() string {
	if e.hWnd != 0 {
		e.text = w32.GetWindowText(e.hWnd)
	}
	return e.text
}

func (e *GoTextEditObj) destroy() {
	for _, ctl := range e.controls {
		ctl = nil	// delete control structure
		e.removeControl(ctl)
		//delete(f.controls, id)
	}
}

func (e *GoTextEditObj) fontHandle() w32.HFONT {
	if e.font != nil {
		return e.font.handle
	}
	/*if e.parent != nil {
		font := e.parent.Font()
		if font != nil {
			return font.handle
		}
	}*/
	return 0
}

func (e *GoTextEditObj) groupId() (id int) {
	return e.groupID
}

func (e *GoTextEditObj) isDialog() (bool) {
	return false
}

func (e *GoTextEditObj) isLayout() (bool) {
	return false
}

func (e *GoTextEditObj) objectType() (string) {
	return "GoTextEditObj"
}

func (e *GoTextEditObj) wid() (*goWidget) {
	return &e.goWidget
}