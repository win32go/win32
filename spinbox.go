/* win32go/win32/progressbar.go */

package win32

import (
	"log"
	//"runtime"
	//"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoSpinBox(parent GoObject) (hProgressBar *GoSpinBoxObj) {
	var className string
	var sizeToView bool
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	}

	p_nextid := nextControlId()
	p_widget := parent.wid()
	/*if runtime.GOARCH == "amd64" {
		className = "msctls_updown"
	} else if runtime.GOARCH == "win32" {
		className = "msctls_updown32"
	}*/
	className = "msctls_updown32"
	log.Println("className =", className)
	widget := goWidget{
		className: 		className,
		windowName: 	"",
		style: 			w32.WS_CHILD,
		exStyle:    	0,
		state:      	w32.SW_HIDE,
		x:      		0,
		y:      		0,
		width:  		400,
		height: 		24,
		hWndParent:		p_widget.hWnd,
		id:				p_nextid,
		instance:		p_widget.instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		border: 		BorderNone,
		cursor:     	nil,	// *goCursor
		font:       	nil, 	// *goFont
		margin:			&GoMarginType{2, 2, 2, 2},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_ExpandingWidth, ST_FixedHeight, true},
		text: 	 		"",
		window:     	false,
		widgets:    	map[int]*goWidget{},
		alpha:			0, 		//uint8
		onShow:        	nil, 	//func()
		onClose:       	nil, 	//func()
		onCanClose:    	nil, 	//func() bool
		onMouseMove:   	nil, 	//func(x, y int)
		onMouseWheel:  	nil, 	//func(x, y int, delta float64)
		onMouseDown:   	nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     	nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     	nil, 	//func(key int)
		onKeyUp:       	nil, 	//func(key int)
		onResize:      	nil, 	//func()
	}
	object := goObject{parent, []GoObject{}}
	sb := &GoSpinBoxObj{widget, object, 0, 0, 0, 0, nil}

	_, err := sb.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(sb)
		GoApp().AddControl(sb, p_nextid)
	}

	if sizeToView == true {
		GoTestView.SizeToControl(sb)
	}
	return sb
}

type GoSpinBoxObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	groupID			int
	value         int32
	minValue      int32
	maxValue      int32
	onValueChange func(value int)
}

func (sb *GoSpinBoxObj) SetValue(v int) {
	sb.value = int32(v)
	if sb.value < sb.minValue {
		sb.value = sb.minValue
	}
	if sb.value > sb.maxValue {
		sb.value = sb.maxValue
	}
	//if n.upDownHandle != 0 {
		w32.SendMessage(sb.hWnd, w32.UDM_SETPOS32, 0, uintptr(v))
	//}
}

func (sb *GoSpinBoxObj) Min() int {
	return int(sb.minValue)
}

func (sb *GoSpinBoxObj) SetMin(min int) {
	if sb.Value() < min {
		sb.SetValue(min)
	}
	sb.minValue = int32(min)
	//if sb.upDownHandle != 0 {
		w32.SendMessage(
			sb.hWnd,
			w32.UDM_SETRANGE32,
			uintptr(sb.minValue),
			uintptr(sb.maxValue),
		)
	//}
}

func (sb *GoSpinBoxObj) Max() int {
	return int(sb.maxValue)
}

func (sb *GoSpinBoxObj) SetMax(max int) {
	if sb.Value() > max {
		sb.SetValue(max)
	}
	sb.maxValue = int32(max)
	//if n.upDownHandle != 0 {
		w32.SendMessage(
			sb.hWnd,
			w32.UDM_SETRANGE32,
			uintptr(sb.minValue),
			uintptr(sb.maxValue),
		)
	//}
}

func (sb *GoSpinBoxObj) MinMax() (min int, max int) {
	return int(sb.minValue), int(sb.maxValue)
}

func (sb *GoSpinBoxObj) SetMinMax(min int, max int) {
	if sb.Value() < min {
		sb.SetValue(min)
	} else if sb.Value() > max {
		sb.SetValue(max)
	}
	sb.minValue = int32(min)
	sb.maxValue = int32(max)
	//if sb.upDownHandle != 0 {
		w32.SendMessage(
			sb.hWnd,
			w32.UDM_SETRANGE32,
			uintptr(sb.minValue),
			uintptr(sb.maxValue),
		)
	//}
}

func (sb *GoSpinBoxObj) OnValueChange() func(value int) {
	return sb.onValueChange
}

func (sb *GoSpinBoxObj) SetOnValueChange(f func(value int)) {
	sb.onValueChange = f
}

func (sb *GoSpinBoxObj) Value() int {
	if sb.hWnd != 0 {
		sb.value = int32(w32.SendMessage(sb.hWnd, w32.UDM_GETPOS32, 0, 0))
	}
	return int(sb.value)
}

func (sb *GoSpinBoxObj) destroy() {
	for _, ctl := range sb.controls {
		ctl = nil	// delete control structure
		sb.removeControl(ctl)
		//delete(f.controls, id)
	}
}

func (sb *GoSpinBoxObj) groupId() (id int) {
	return sb.groupID
}

func (sb *GoSpinBoxObj) handleNotification(cmd uintptr) {
	if cmd == w32.EN_CHANGE && sb.onValueChange != nil {
		sb.onValueChange(sb.Value())
	}
}

func (sb *GoSpinBoxObj) isDialog() (bool) {
	return false
}

func (sb *GoSpinBoxObj) isLayout() (bool) {
	return false
}

func (sb *GoSpinBoxObj) objectType() (string) {
	return "GoSpinBoxObj"
}

func (sb *GoSpinBoxObj) wid() (*goWidget) {
	return &sb.goWidget
}