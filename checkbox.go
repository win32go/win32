/* checkbox.go */

package win32

import (
	"log"
	"syscall"
	"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoCheckBox(parent GoObject, text string) (hCheckBox *GoCheckBoxObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 

	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 	"BUTTON",
		windowName: text,
		style: 		w32.WS_CHILD | w32.WS_TABSTOP | w32.BS_AUTOCHECKBOX | w32.BS_OWNERDRAW | w32.BS_NOTIFY,
		exStyle:    0,
		state:      w32.SW_HIDE,
		x:      	0,
		y:      	0,
		width:  	200,
		height: 	20,
		hWndParent:	p_widget.hWnd,
		id:			p_nextid,
		instance:	p_widget.instance,
		param:		0,
		hWnd: 		0,
		parent:  	p_widget, 	// *goWidget
		disabled: 	false,
		visible:   	false,
		autoSize:	false,
		border:		BorderNone,
		cursor:     nil,		// *goCursor
		cursorPos:  nil, 		// *goCursorMetrics
		font:       nil, 		// *goFont
		margin:		&GoMarginType{5, 5, 5, 5},
		padding:	&GoPaddingType{2, 2, 2, 2},
		sizePolicy:	&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 	text,
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		backgroundMode:	TransparentMode,
		onShow:        nil, 	//func()
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}
	//c.textControl.create(id, 0, "BUTTON", w32.WS_TABSTOP|w32.BS_AUTOCHECKBOX)
	//w32.SendMessage(c.handle, w32.BM_SETCHECK, toCheckState(c.checked), 0)
									   
	object := goObject{parent, []GoObject{}}
	c := &GoCheckBoxObj{widget, object, 0, false, nil}
	
	_, err := c.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent :", parent.objectType())
		log.Println("parent.addControl (GoCheckBox) :", p_nextid)
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(c)
		GoApp().AddControl(c, p_nextid)
	}
	// Subclass Label Window to allow custom painting of label
	w32.SetWindowSubclass(c.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		case w32.WM_ERASEBKGND:
			log.Println("GoCheckBox::WM_ERASEBACKGROUND - ")
			//widget := win.wid()
			rect := c.GetClientRect()
			hDC := w32.HDC(wParam)
			c.paintBackground(hDC, rect)
			return 1

		case w32.WM_PAINT:
			//log.Println("GoCheckBox:WM_PAINT.......")
			//log.Println("mousehover =", c.mousehover)
			//log.Println("mousedown =", c.mousedown)
			var ps w32.PAINTSTRUCT
			hDC := w32.BeginPaint(hWnd, &ps)
			rect := c.GetClientRect()
			rc := rect.DeductPadding(c.padding)
			//windRect := c.GetWindowRect()
			//clientRect := c.GetClientRect()
			//borderRect := windRect.Diff() - clientRect
			//rc := clientRect.DeductPadding(c.padding)
			pt := CreatePainter(hDC, rect)
			if c.backgroundMode == OpaqueMode {
				pt.SetStockBrush(c.backcolor)
				pt.SetStockPen(c.backcolor)
			} else {
				pt.SetNullBrush()
				pt.SetNullPen()
			}
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())

		    pt.SetStockPen(c.buttonText)
		    pt.SetStockBrush(Color_White)

		    pt.DrawRect(rc.left + 2, rc.top + 2, rc.bottom - rc.top - 4, rc.bottom - rc.top - 4)
		    log.Println("GoCheckBox::checked", c.checked)
		    if c.checked {
		    	pt.SetStockBrush(c.buttonText)
		    } else {
		    	pt.SetStockPen(Color_White)
		    	pt.SetStockBrush(Color_White)
		    }
		    pt.DrawRect(rc.left + 6, rc.top + 6, rc.bottom - rc.top - 12, rc.bottom - rc.top - 12)
		    rc.SetLeft(rc.left + rc.bottom - rc.top + 5)
		    pt.SetBackMode(TransparentMode)
			if c.mousehover {
				if c.mousedown {
					pt.SetTextColor(c.buttonText)
				} else {
					pt.SetTextColor(c.highlight)
				}
			}
			
			pt.DrawTextLine(c.text, rc, w32.DT_LEFT, w32.DT_VCENTER)
			//w32.DrawFocusRect(hdc, rect.W32RECT())
			pt.Destroy()
		    w32.EndPaint(hWnd, &ps)
		    return 0

		case w32.WM_MOUSEMOVE:          
			if !c.mousehover {
				log.Println("GoCheckBox:WM_MOUSEHOVER.......")
			    c.mousehover = true
			    w32.InvalidateRect(c.hWnd, nil, true)
			    //SendMessage(hWnd, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBmpHiLight );
			    tme := w32.TRACKMOUSEEVENT{}
			    tme.CbSize = uint32(unsafe.Sizeof(tme))
			    tme.DwFlags = w32.TME_LEAVE
			    tme.HwndTrack = c.hWnd
			    tme.DwHoverTime = 400
			    w32.TrackMouseEvent(&tme)
			}
			return 0

        case w32.WM_MOUSELEAVE:
        	log.Println("GoCheckBox:WM_MOUSELEAVE.......")
			c.mousehover = false
			w32.InvalidateRect(c.hWnd, nil, true)
			return 0

		default:
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		}
	}), 0, 0)

	//res := c.SetBackgroundColor(COLOR_WHITE)
	//log.Println("SetBackgroundColor res :", res)
	if sizeToView == true {
		GoTestView.SizeToControl(c)
	}
	return c
}

type GoCheckBoxObj struct {
	goWidget
	goObject
	groupID  int
	//autoSize bool
	checked  bool
	onChange func(bool)
}

func (c *GoCheckBoxObj) Checked() bool {
	return c.checked
}

func (c *GoCheckBoxObj) SetAutoSize(autoSize bool) {
	c.autoSize = autoSize
	c.setGeometry()
}

func (c *GoCheckBoxObj) SetChecked(checked bool) {
	if checked == c.checked {
		return
	}
	c.checked = checked
	if c.hWnd != 0 {
		w32.SendMessage(c.hWnd, w32.BM_SETCHECK, toCheckState(c.checked), 0)
	}
	if c.onChange != nil {
		c.onChange(c.checked)
	}
	return
}



func (c *GoCheckBoxObj) SetGroupId(groupId int) {
	c.groupID = groupId
}

func (c *GoCheckBoxObj) SetOnChange(f func(checked bool)) {
	c.onChange = f
}

func (c *GoCheckBoxObj) Show() {
	w32.ShowWindow(c.hWnd, w32.SW_SHOW)
	c.state = w32.SW_SHOW
	c.visible = true
	parent := c.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (c *GoCheckBoxObj) destroy() {
	/*for id, control := range w.controls {
		delete(w.controls, id)
	}*/
}

func (c *GoCheckBoxObj) drawItem(s *w32.DRAWITEMSTRUCT) {
	log.Println("GoRadioButtonObj::drawItemStruct..............")
	/*state := s.ItemState
	if (state & w32.ODS_SELECTED) == 1 {
		c.mousedown = true
	} else {
		c.mousedown = false
	}
	hDC := s.HDC
	w32.SetBkMode(hDC, w32.TRANSPARENT)
	rect := GoRect().FromW32Rect(&s.RcItem)
	rc := rect.DeductPadding(c.padding)
	pt := CreatePainter(hDC, rect)
	pt.SetTextColor(c.buttonText)
	pt.SetNullPen()
			
	if c.mousedown {
		pt.SetTextColor(c.buttonText)
	} else {
		pt.SetTextColor(c.highlight)
	}
			
    pt.DrawRect(rect.left, rect.top, rect.right, rect.bottom)
    pt.SetStockPen(c.buttonText)
    pt.DrawRect(rc.left, rc.top, rc.bottom - rc.top, rc.bottom - rc.top)
    log.Println("WM_PAINT::checked", c.checked)
    if c.checked {
		pt.MoveTo(rc.left + 4, rc.bottom - 6)
    	pt.LineTo(rc.left + 6, rc.bottom - 4)
    	pt.LineTo(rc.left + rc.bottom - rc.top - 4, rc.top + 2)
    }
    rc.SetLeft(rc.left + rc.bottom - rc.top + 5)
	pt.DrawTextLine(c.text, rc, w32.DT_LEFT, w32.DT_VCENTER)
    pt.Destroy()*/
}

func (c *GoCheckBoxObj) groupId() (id int) {
	return c.groupID
}

func (c *GoCheckBoxObj) handleNotification(cmd uintptr) {
	log.Println("GoCheckBoxObj::handleNotification..............")
	if cmd == w32.BN_CLICKED {
		c.checked = !c.checked
		c.Refresh()
		if c.onChange != nil {
			c.onChange(c.checked)
		}
	}
}

func (c *GoCheckBoxObj) isDialog() (bool) {
	return false
}

func (c *GoCheckBoxObj) isLayout() (bool) {
	return false
}

func (c *GoCheckBoxObj) objectType() (string) {
	return "GoCheckBoxObj"
}

func (c *GoCheckBoxObj) wid() (*goWidget) {
	return &c.goWidget
}

func toCheckState(checked bool) uintptr {
	if checked {
		return w32.BST_CHECKED
	}
	return w32.BST_UNCHECKED
}