/* win32go/win32/dialog.go */

package win32

import (
	//"errors"
	"log"
	//"strconv"
	"gitlab.com/win32go/win32/w32"
)

type GoDialogObj struct {
	goWidget		// embedded widget
	goObject 		// embedded object
	
	caption         string
	
	//ch				chan int
	
	//background    w32.HBRUSH
	lastFocus		w32.HWND
	//icon          uintptr
	//menu 			*GoMenu
	//menuStrings   []*MenuString
	
	layout 			*GoLayoutObj

	buttonId 		int

	buttonOK 		*GoButtonObj	// IDOK = 1
	buttonCancel 	*GoButtonObj	// IDCANCEL = 2
	buttonAbort 	*GoButtonObj	// IDABORT = 3
	buttonRetry 	*GoButtonObj	// IDABORT = 4
	buttonIgnore 	*GoButtonObj	// IDABORT = 5
	buttonYes 		*GoButtonObj	// IDABORT = 6
	buttonNo 		*GoButtonObj	// IDABORT = 7
	buttonClose 	*GoButtonObj	// IDABORT = 8
	buttonHelp 		*GoButtonObj	// IDABORT = 9
	buttonTryAgain 	*GoButtonObj	// IDABORT = 10
	buttonContinue 	*GoButtonObj	// IDABORT = 11

	
	
}

func (w *GoDialogObj) Caption() string {
	return w.caption
}

// Overide goWidget.Create() function for window
func (w *GoDialogObj) Create() (*goWidget, error) {
	var hWnd w32.HWND
	var err error
	
	hWnd, err = w32.CreateWindowExStr(
		w.goWidget.exStyle,
		w.goWidget.className,
		w.goWidget.windowName,
		w.goWidget.style,
		w.goWidget.x, w.goWidget.y, w.goWidget.width, w.goWidget.height,
		w.goWidget.hWndParent,
		w32.HMENU(0),
		w.goWidget.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("goWidget id = ", w.goWidget.id)
		log.Println("Failed to create widget.")
	} else {
		w.goWidget.hWnd = hWnd
		// Register Window with parent.
		//Top Level Window does not have parent.
		if w.goWidget.parent != nil {
			w.goWidget.parent.widgets[w.goWidget.id] = &(w.goWidget)
		}
		//w.ch = make(chan int)

	}
	return &(w.goWidget), err
}

func (w *GoDialogObj) Exec() (int) {
	log.Println("GoDialogObj::Exec()")

	if w.goWidget.parent != nil {
		log.Println("GoDialogObj::Parent X, Width:", w.goWidget.parent.X(), " ,", w.goWidget.parent.Width())
		log.Println("GoDialogObj::Parent Y, Height:", w.goWidget.parent.Y(), " ,", w.goWidget.parent.Height())
		midX := w.goWidget.parent.X() + (w.goWidget.parent.Width() / 2)
		midY := w.goWidget.parent.Y() + (w.goWidget.parent.Height() / 2)
		log.Println("GoDialogObj::Parent Mid X Y:", midX, " ,", midY)
		log.Println("GoDialogObj::Width:, Height", w.Width(), " ,", w.Height())
		x := midX - (w.Width() / 2)
		y := midY - (w.Height() / 2)

		log.Println("GoDialogObj::SetPosition:", x, " ,", y)
		w.SetPosition(x, y)
		w.Show()
		
		w32.EnableWindow(w.goWidget.hWndParent, false)
		log.Println("Exec(): waiting")

		response := w.run()
		
		log.Println("GoDialogObj::run(response:", response, ")")
		w32.EnableWindow(w.goWidget.hWndParent, true)
		w.Hide()
		return response
	}
	return 0
}

func (w *GoDialogObj) run() (int) {
	var msg w32.MSG
	log.Println("GoDialogObj running")
	log.Print("w.goWidget.hWnd:", w.goWidget.hWnd)
	w.buttonId = -1
	for w.buttonId < 0 {
		//log.Println(".")
		msg = w32.MSG{}

		gotMessage, err := w32.GetMessage(&msg, 0, 0, 0)
		
		if gotMessage == 0 {
			log.Println("Run::wParam:", int(msg.WParam))
			return int(msg.WParam)
		} else if gotMessage == -1 {
			log.Println("GetMessage Error:", err)
			return -1
		}
		//log.Println("hWnd:", msg.Hwnd, "Message:", msg.Message)

		w32.TranslateMessage(&msg)
		w32.DispatchMessage(&msg)
	}
	return w.buttonId
}

func (w *GoDialogObj) SetCaption(caption string) {
	w.caption = caption
	if w.hWnd != 0 {
		w32.SetWindowText(w.hWnd, caption)
	}
}

func (w *GoDialogObj) LayoutStyle() (style GoLayoutStyle) {
	return w.layout.Style()
}

func (w *GoDialogObj) Layout() (layout *GoLayoutObj) {
	return w.layout
}

func (w *GoDialogObj) SetLayoutStyle(style GoLayoutStyle) {
	w.layout.SetStyle(style)
}

func (w *GoDialogObj) SetText(text string) {
	w.text = text
	w32.InvalidateRect(w.hWnd, nil, true)
}

func (w *GoDialogObj) TextOut(text string) {
	w.text = w.text + text
	w32.InvalidateRect(w.hWnd, nil, true)
}

func (w *GoDialogObj) Update() {
	w32.UpdateWindow(w.hWnd)
}

func (w *GoDialogObj) onWM_COMMAND( wParam uintptr, lParam uintptr) {
	log.Println("Dialog::onWM_COMMAND:")
	wParamH := (wParam & 0xFFFF0000) >> 16
	wParamL := wParam & 0xFFFF
	lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF
	log.Println("Dialog::wParamH:", wParamH)
	log.Println("Dialog::wParamL:", wParamL)
	log.Println("Dialog::lParamH:", lParamH)
	log.Println("Dialog::lParamL:", lParamL)
	if lParam != 0 {
		// control clicked
		index := wParamL	// controlID
		cmd := wParamH		// BN_CLICKED
		if cmd == w32.BN_CLICKED {
			log.Println("Dialog::w32.BN_CLICKED..............")
			log.Println("Dialog::Comtrolindex:", index)
			// get widget from GoApp.winStack using controlID and trigger function widget.onClick
			// which in turn triggers function button.onClick()
			w.buttonId = GoApp().Controls(index).groupId()
			log.Println("Dialog::index:", w.buttonId)
		}
	} else {
		if wParamH == 0 {
			// low word of w contains menu ID
			/*id := int(wParamL)
			if goApp.Actions(id).OnClick() != nil {
				goApp.Actions(id).onClick()
			}*/

			

		} else if wParamH == 1 {
				// low word of w contains accelerator ID
			//index := int(wParamL)
			/*if f := w.shortcuts[index].f; f != nil {
				f()
			}*/
		}

	}
}

func (w *GoDialogObj) onWM_DRAWITEM(wParam uintptr, lParam uintptr) {
}

func (w *GoDialogObj) onWM_NOTIFY(wParam uintptr, lParam uintptr) {
}

func (w *GoDialogObj) destroy() () {
	for _, ctl := range w.controls {
		ctl = nil	// delete control structure
		w.removeControl(ctl)
		//delete(w.controls, id)	// remove from Control Stack
	}
}

func (w *GoDialogObj) groupId() (id int) {
	return 0
}

func (w *GoDialogObj) isDialog() (bool) {
	return true
}

func (w *GoDialogObj) isLayout() (bool) {
	return false
}

func (w *GoDialogObj) objectType() (string) {
	return "GoDialogObj"
}

func (w *GoDialogObj) wid() (*goWidget) {
	return &w.goWidget
}