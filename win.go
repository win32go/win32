/* win.go */

package win32

import (
	"log"
	"reflect"
	"gitlab.com/win32go/win32/w32"
)

var GoTestView *GoWindowObj

var nextMnuId int = 1000
var nextCtrlId int = 100
var nextWinId int = -1

func nextMenuId() (int) {
	nextMnuId++
	return nextMnuId
}

func nextControlId() (int) {
	nextCtrlId++
	return nextCtrlId
}

func nextWindowId() (int) {
	nextWinId++
	return nextWinId
}

type GoObject interface {
	addControl(GoObject)
	//control(id int) (GoObject)
	drawItem(drawItemStruct *w32.DRAWITEMSTRUCT)
	handleNotification(cmd uintptr)
	groupId() (int)
	isDialog() (bool)
	isLayout() (bool)
	objectType() (string)
	parentControl() (GoObject)
	removeControl(GoObject)
	
	onWM_COMMAND(w, l uintptr)
	onWM_DRAWITEM(w, l uintptr)
	onWM_NOTIFY(w, l uintptr)
	destroy()
	wid() (*goWidget)
}

func GoWindow(parent GoObject) (hWin *GoWindowObj) {
	var p_widget *goWidget = nil
	var p_hWnd w32.HWND = 0

	a := GoApp()
	if a == nil {
		log.Fatal("ERROR creating GoMainWindow: GoApplication has to be created first")
	}

	// Top window properties 
	p_instance := a.hInstance
	p_nextid := nextWindowId()
	
	// Owned window properties inherited from parent window
	if parent != nil {
		p_widget = parent.wid()
		p_hWnd = p_widget.hWnd
		p_instance = p_widget.instance
	}
	

	widget := goWidget{
		className:  	goApp.appName,
		windowName:		"",
		style:     		w32.WS_OVERLAPPEDWINDOW,
		exStyle:		0,
		state:      	w32.SW_HIDE,
		x:          	0,
		y:          	0,
		width:      	800,
		height:     	500,
		hWndParent: 	p_hWnd,
		id: 			p_nextid,
		instance: 		p_instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		font:        	nil,
		margin:			&GoMarginType{0, 0, 0, 0},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		widgets:		map[int]*goWidget{},
		backMode: 		OpaqueMode,
		backgroundMode:	OpaqueMode,
		onShow:        	nil,
		onClose:       	nil,
		onCanClose:    	nil,
		onMouseMove:   	nil,
		onMouseWheel:  	nil,
		onMouseDown:   	nil,
		onMouseUp:     	nil,
		onKeyDown:     	nil,
		onKeyUp:       	nil,
		onResize:      	nil,
	}
	object := goObject{nil, []GoObject{}}
	hWin = &GoWindowObj{widget, object, "", nil, 0, nil} //, nil, nil}
	_, err := hWin.Create()
	if err != nil {
		log.Println("Error creating GoMainWindow :", err)
	} else {
		a.AddWindow(hWin, p_nextid)
		hWin.layout = GoLayout(hWin, NoLayout)
		hWin.layout.SetBackgroundMode(TransparentMode)
		hWin.layout.Show()
	}
	//registerMainWindow(hWin)
	return hWin
}



func GoDialog(parent GoObject) (hWin *GoDialogObj) {
	var p_widget *goWidget = nil
	var p_hWnd w32.HWND = 0

	a := GoApp()
	if a == nil {
		log.Fatal("ERROR creating GoDialog: GoApplication has to be created first")
	}

	// Top window properties 
	p_instance := a.hInstance
	p_nextid := nextWindowId()
	
	// Owned window properties inherited from parent window
	if parent != nil {
		p_widget = parent.wid()
		p_hWnd = p_widget.hWnd
		p_instance = p_widget.instance
	}

	widget := goWidget{
		className:  	goApp.appName,
		windowName:		"",
		style:     		w32.WS_OVERLAPPED | w32.WS_CAPTION | w32.WS_SYSMENU,
		exStyle:		0,
		state:      	w32.SW_HIDE,
		x:          	0,
		y:          	0,
		width:      	400,
		height:     	300,
		hWndParent: 	p_hWnd,
		id: 			p_nextid,
		instance: 		p_instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		font:        	nil,
		margin:			&GoMarginType{0, 0, 0, 0},
		padding:		&GoPaddingType{10, 10, 10, 10},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 			"",
		window: 		true,
		widgets:		map[int]*goWidget{},
		alpha: 			0,
		backcolor:		0,
		forecolor:		0,
		onShow:        	nil,
		onClose:       	nil,
		onCanClose:    	nil,
		onMouseMove:   	nil,
		onMouseWheel:  	nil,
		onMouseDown:   	nil,
		onMouseUp:     	nil,
		onKeyDown:     	nil,
		onKeyUp:       	nil,
		onResize:      	nil,
	}
	object := goObject{nil, []GoObject{}}
	hWin = &GoDialogObj{widget, object, "", 0, nil, 0, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil} //, nil, nil}	
	_, err := hWin.Create()
	if err != nil {
		log.Println("Error creating GoDialog :", err)
	} else {
		a.AddWindow(hWin, p_nextid)
		
		hWin.layout = GoLayout(hWin, VBoxLayout)
		hWin.Layout().SetBorderStyle(BorderSingleLine)
		hWin.Layout().SetPadding(40,40,40,40)
		hWin.Layout().SetBackgroundMode(TransparentMode)
		//hWin.backcolor = goApp.ColorWindow
		//hWin.forecolor = goApp.ColorWindowText
		hWin.Layout().Show()

		hWin.Layout().AddLayout(HBoxLayout)

		hWin.Layout().AddLayout(HBoxLayout)

		hWin.buttonCancel = GoButton(hWin.Layout(), "Cancel")
		hWin.buttonCancel.SetSize(90, 30)
		hWin.buttonCancel.SetGroupId(2)
		//w.buttonCancel.SetOnClick(sendCancel)
		//hWin.buttonCancel.SetPosition(280, 200)
		hWin.buttonCancel.Show()

		hWin.buttonOK = GoButton(hWin.Layout(), "OK")
		hWin.buttonOK.SetSize(90, 30)
		hWin.buttonOK.SetGroupId(1)
		//w.buttonOK.SetOnClick(sendOK)
		//hWin.buttonOK.SetPosition(140, 200)
		hWin.buttonOK.Show()
	}
	//registerMainWindow(hWin)
	return hWin	
}

//GoMessageBox ( parent GoObject, style GMessageBoxStyle, caption string, message string, button0 int, button1 int, button2 int, modal bool )
func GoMessageBox(parent GoObject, style GoMessageBoxStyle, args ...interface{}) (hWin *GoMessageBoxObj) {
	var p_widget *goWidget = nil
	var p_hWnd w32.HWND = 0

	
	var caption string = ""
	var text string = ""
	var button0 int = 0
	var button1 int = 0
	var button2 int = 0
	//var icon GoMessageBoxIcon = NoIcon

	style = NoIcon
	a := GoApp()
	if a == nil {
		log.Fatal("ERROR creating GoDialog: GoApplication has to be created first")
	}

	// Top window properties 
	p_instance := a.hInstance
	p_nextid := nextWindowId()
	
	// Owned window properties inherited from parent window
	if parent != nil {
		p_widget = parent.wid()
		p_hWnd = p_widget.hWnd
		p_instance = p_widget.instance
	}

	widget := goWidget{
		className:  	goApp.appName,
		windowName:		"",
		style:     		w32.WS_OVERLAPPED | w32.WS_CAPTION | w32.WS_SYSMENU,
		exStyle:		0,
		state:      	w32.SW_HIDE,
		x:          	0,
		y:          	0,
		width:      	400,
		height:     	300,
		hWndParent: 	p_hWnd,
		id: 			p_nextid,
		instance: 		p_instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		font:        	nil,
		margin:			&GoMarginType{0, 0, 0, 0},
		padding:		&GoPaddingType{10, 10, 10, 10},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 			"",
		window: 		true,
		widgets:		map[int]*goWidget{},
		alpha: 			0,
		backMode: 		OpaqueMode,
		backgroundMode:	OpaqueMode,
		onShow:        	nil,
		onClose:       	nil,
		onCanClose:    	nil,
		onMouseMove:   	nil,
		onMouseWheel:  	nil,
		onMouseDown:   	nil,
		onMouseUp:     	nil,
		onKeyDown:     	nil,
		onKeyUp:       	nil,
		onResize:      	nil,
	}
	object := goObject{nil, []GoObject{}}

	for i, v := range args {
		log.Println("GoLabel() - arg:", i, "value:", v)
		switch v := reflect.ValueOf(v); v.Kind() {

		case reflect.String:
			log.Println("GoMessageBox() - v.String():", v.String())
			if i == 0 {caption = v.String()}
			if i == 1 {text = v.String()}
			
		case reflect.Int:
			log.Println("GoMessageBox() - v.Int():", v.Int())
			if i == 2 {button0 = int(v.Int())}
			if i == 3 {button1 = int(v.Int())}
			if i == 4 {button2 = int(v.Int())}

		default:
			log.Println("GoMexxageBox() - Not String or Int")
		}
	}
	hWin = &GoMessageBoxObj{widget, object, style, "", caption, text, 0, 0, 0, 0, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, 0} //, nil, nil}	
	_, err := hWin.Create()
	if err != nil {
		log.Println("Error creating GoDialog :", err)
	} else {
		a.AddWindow(hWin, p_nextid)
		hWin.style = style
		// Set icon for style of messagebox
		// ..................

		// ..................
		hWin.layout = GoLayout(hWin, VBoxLayout)
		hWin.Layout().SetBorderStyle(BorderSingleLine)
		hWin.Layout().SetPadding(40,40,40,40)
		hWin.Layout().SetBackgroundMode(TransparentMode)
		//hWin.backcolor = goApp.ColorWindow
		//hWin.forecolor = goApp.ColorWindowText
		hWin.Layout().Show()

		captionLayout := hWin.Layout().AddLayout(HBoxLayout)
		captionLayout.SetSizePolicy(ST_ExpandingWidth, ST_ExpandingHeight, false)
		//captionLayout.SetBackgroundColor(Color_DarkGray)
		captionLayout.Show()

		// create GoIcon and show left of caption
		// ..................

		// ..................
		
		hWin.captionLabel = GoLabel(captionLayout, caption)
		hWin.captionLabel.SetAutoSize(true)
		hWin.captionLabel.Show()
		
		messageLayout := hWin.Layout().AddLayout(HBoxLayout)
		messageLayout.SetSizePolicy(ST_ExpandingWidth, ST_ExpandingHeight, false)
		messageLayout.Show()
		
		hWin.messageLabel = GoLabel(messageLayout, text)
		hWin.messageLabel.SetAutoSize(true)
		hWin.messageLabel.Show()
		//hWin.Layout().AddLayout(HBoxLayout)
		
		buttonLayout := hWin.Layout().AddLayout(HBoxLayout)
		buttonLayout.SetHeight(60)
		buttonLayout.SetSizePolicy(ST_ExpandingWidth, ST_FixedHeight, false)
		buttonLayout.Show()

		buttonSpacer := GoLabel(buttonLayout)
		buttonSpacer.SetSizePolicy(ST_ExpandingWidth, ST_ExpandingHeight, false)

		if button0 != 0 {

			switch button0 {
			case 1:
				hWin.buttonOK = GoButton(buttonLayout, "OK")
				hWin.buttonOK.SetSize(90, 30)
				hWin.buttonOK.SetGroupId(1)
				//w.buttonOK.SetOnClick(sendOK)
				//hWin.buttonOK.SetPosition(140, 200)
				hWin.buttonOK.Show()
			case 2:
				hWin.buttonCancel = GoButton(buttonLayout, "Cancel")
				hWin.buttonCancel.SetSize(90, 30)
				hWin.buttonCancel.SetGroupId(2)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonCancel.Show()
			case 3:
				hWin.buttonAbort = GoButton(buttonLayout, "Abort")
				hWin.buttonAbort.SetSize(90, 30)
				hWin.buttonAbort.SetGroupId(3)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonAbort.Show()
			case 4:
				hWin.buttonRetry = GoButton(buttonLayout, "Retry")
				hWin.buttonRetry.SetSize(90, 30)
				hWin.buttonRetry.SetGroupId(4)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonRetry.Show()
			case 5:
				hWin.buttonIgnore = GoButton(buttonLayout, "Ignore")
				hWin.buttonIgnore.SetSize(90, 30)
				hWin.buttonIgnore.SetGroupId(5)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonIgnore.Show()
			case 6:
				hWin.buttonYes = GoButton(buttonLayout, "Yes")
				hWin.buttonYes.SetSize(90, 30)
				hWin.buttonYes.SetGroupId(6)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonYes.Show()
			case 7:
				hWin.buttonNo = GoButton(buttonLayout, "No")
				hWin.buttonNo.SetSize(90, 30)
				hWin.buttonNo.SetGroupId(7)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonNo.Show()
			case 8:
				hWin.buttonClose = GoButton(buttonLayout, "Close")
				hWin.buttonClose.SetSize(90, 30)
				hWin.buttonClose.SetGroupId(8)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonClose.Show()
			case 9:
				hWin.buttonHelp = GoButton(buttonLayout, "Help")
				hWin.buttonHelp.SetSize(90, 30)
				hWin.buttonHelp.SetGroupId(9)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonHelp.Show()
			case 10:
				hWin.buttonTryAgain = GoButton(buttonLayout, "Try Again")
				hWin.buttonTryAgain.SetSize(90, 30)
				hWin.buttonTryAgain.SetGroupId(10)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonTryAgain.Show()
			case 11:
				hWin.buttonContinue = GoButton(buttonLayout, "Continue")
				hWin.buttonContinue.SetSize(90, 30)
				hWin.buttonContinue.SetGroupId(11)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonContinue.Show()
			default:
				log.Println("GoMexxageBox() - Button Style out of range")
			}
		}
		if button1 != 0 {

			switch button1 {
			case 1:
				hWin.buttonOK = GoButton(buttonLayout, "OK")
				hWin.buttonOK.SetSize(90, 30)
				hWin.buttonOK.SetGroupId(1)
				//w.buttonOK.SetOnClick(sendOK)
				//hWin.buttonOK.SetPosition(140, 200)
				hWin.buttonOK.Show()
			case 2:
				hWin.buttonCancel = GoButton(buttonLayout, "Cancel")
				hWin.buttonCancel.SetSize(90, 30)
				hWin.buttonCancel.SetGroupId(2)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonCancel.Show()
			case 3:
				hWin.buttonAbort = GoButton(buttonLayout, "Abort")
				hWin.buttonAbort.SetSize(90, 30)
				hWin.buttonAbort.SetGroupId(3)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonAbort.Show()
			case 4:
				hWin.buttonRetry = GoButton(buttonLayout, "Retry")
				hWin.buttonRetry.SetSize(90, 30)
				hWin.buttonRetry.SetGroupId(4)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonRetry.Show()
			case 5:
				hWin.buttonIgnore = GoButton(buttonLayout, "Ignore")
				hWin.buttonIgnore.SetSize(90, 30)
				hWin.buttonIgnore.SetGroupId(5)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonIgnore.Show()
			case 6:
				hWin.buttonYes = GoButton(buttonLayout, "Yes")
				hWin.buttonYes.SetSize(90, 30)
				hWin.buttonYes.SetGroupId(6)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonYes.Show()
			case 7:
				hWin.buttonNo = GoButton(buttonLayout, "No")
				hWin.buttonNo.SetSize(90, 30)
				hWin.buttonNo.SetGroupId(7)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonNo.Show()
			case 8:
				hWin.buttonClose = GoButton(buttonLayout, "Close")
				hWin.buttonClose.SetSize(90, 30)
				hWin.buttonClose.SetGroupId(8)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonClose.Show()
			case 9:
				hWin.buttonHelp = GoButton(buttonLayout, "Help")
				hWin.buttonHelp.SetSize(90, 30)
				hWin.buttonHelp.SetGroupId(9)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonHelp.Show()
			case 10:
				hWin.buttonTryAgain = GoButton(buttonLayout, "Try Again")
				hWin.buttonTryAgain.SetSize(90, 30)
				hWin.buttonTryAgain.SetGroupId(10)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonTryAgain.Show()
			case 11:
				hWin.buttonContinue = GoButton(buttonLayout, "Continue")
				hWin.buttonContinue.SetSize(90, 30)
				hWin.buttonContinue.SetGroupId(11)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonContinue.Show()
			default:
				log.Println("GoMexxageBox() - Button Style out of range")
			}
		}
		if button2 != 0 {

			switch button2 {
			case 1:
				hWin.buttonOK = GoButton(buttonLayout, "OK")
				hWin.buttonOK.SetSize(90, 30)
				hWin.buttonOK.SetGroupId(1)
				//w.buttonOK.SetOnClick(sendOK)
				//hWin.buttonOK.SetPosition(140, 200)
				hWin.buttonOK.Show()
			case 2:
				hWin.buttonCancel = GoButton(buttonLayout, "Cancel")
				hWin.buttonCancel.SetSize(90, 30)
				hWin.buttonCancel.SetGroupId(2)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonCancel.Show()
			case 3:
				hWin.buttonAbort = GoButton(buttonLayout, "Abort")
				hWin.buttonAbort.SetSize(90, 30)
				hWin.buttonAbort.SetGroupId(3)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonAbort.Show()
			case 4:
				hWin.buttonRetry = GoButton(buttonLayout, "Retry")
				hWin.buttonRetry.SetSize(90, 30)
				hWin.buttonRetry.SetGroupId(4)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonRetry.Show()
			case 5:
				hWin.buttonIgnore = GoButton(buttonLayout, "Ignore")
				hWin.buttonIgnore.SetSize(90, 30)
				hWin.buttonIgnore.SetGroupId(5)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonIgnore.Show()
			case 6:
				hWin.buttonYes = GoButton(buttonLayout, "Yes")
				hWin.buttonYes.SetSize(90, 30)
				hWin.buttonYes.SetGroupId(6)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonYes.Show()
			case 7:
				hWin.buttonNo = GoButton(buttonLayout, "No")
				hWin.buttonNo.SetSize(90, 30)
				hWin.buttonNo.SetGroupId(7)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonNo.Show()
			case 8:
				hWin.buttonClose = GoButton(buttonLayout, "Close")
				hWin.buttonClose.SetSize(90, 30)
				hWin.buttonClose.SetGroupId(8)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonClose.Show()
			case 9:
				hWin.buttonHelp = GoButton(buttonLayout, "Help")
				hWin.buttonHelp.SetSize(90, 30)
				hWin.buttonHelp.SetGroupId(9)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonHelp.Show()
			case 10:
				hWin.buttonTryAgain = GoButton(buttonLayout, "Try Again")
				hWin.buttonTryAgain.SetSize(90, 30)
				hWin.buttonTryAgain.SetGroupId(10)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonTryAgain.Show()
			case 11:
				hWin.buttonContinue = GoButton(buttonLayout, "Continue")
				hWin.buttonContinue.SetSize(90, 30)
				hWin.buttonContinue.SetGroupId(11)
				//w.buttonCancel.SetOnClick(sendCancel)
				//hWin.buttonCancel.SetPosition(280, 200)
				hWin.buttonContinue.Show()
			default:
				log.Println("GoMexxageBox() - Button Style out of range")
			}
		}
	}
	return hWin
}

func GoPopupWindow(parent GoObject) (hWin *GoWindowObj) {
	var p_widget *goWidget = nil
	var p_hWnd w32.HWND = 0

	a := GoApp()
	if a == nil {
		log.Fatal("ERROR creating GoPopupWindow: GoApplication has to be created first")
	}

	// Top window properties 
	p_instance := a.hInstance
	p_nextid := nextWindowId()
	
	// Owned window properties inherited from parent window
	if parent != nil {
		p_widget = parent.wid()
		p_hWnd = p_widget.hWnd
		p_instance = p_widget.instance
	}
	

	widget := goWidget{
		className:  	goApp.appName,
		windowName:		"",
		style:     		w32.WS_POPUPWINDOW | w32.WS_CAPTION,
		exStyle:		0,
		state:      	w32.SW_HIDE,
		x:          	0,
		y:          	0,
		width:      	300,
		height:     	300,
		hWndParent: 	p_hWnd,
		id: 			p_nextid,
		instance: 		p_instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		font:        	nil,
		margin:			&GoMarginType{0, 0, 0, 0},
		padding:		&GoPaddingType{10, 10, 10, 10},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		widgets:		map[int]*goWidget{},
		alpha: 			0,
		backcolor:		0,
		forecolor:		0,
		onShow:        	nil,
		onClose:       	nil,
		onCanClose:    	nil,
		onMouseMove:   	nil,
		onMouseWheel:  	nil,
		onMouseDown:   	nil,
		onMouseUp:     	nil,
		onKeyDown:     	nil,
		onKeyUp:       	nil,
		onResize:      	nil,
	}
	object := goObject{nil, []GoObject{}}
	hWin = &GoWindowObj{widget, object, "", nil, 0, nil} //, nil, nil}	
	_, err := hWin.Create()
	if err != nil {
		log.Println("Error creating GoPopupWindow :", err)
	} else {
		a.AddWindow(hWin, p_nextid)
		hWin.layout = GoLayout(hWin, NoLayout)
		hWin.backcolor = goApp.ColorWindow
		hWin.forecolor = goApp.ColorWindowText
		hWin.Layout().Show()
	}
	//registerMainWindow(hWin)
	return hWin
}



func GoDeskTop() (hdesktop *GoDeskTopWindow) {

	ret := w32.GetDesktopWindow()
	hWnd := w32.HWND(ret)
	screen := goWidget{
		className: 	"",
		windowName: "",
		style: 		0,
		exStyle:    0,
		state:      0,
		x:      	0,
		y:      	0,
		width:  	0,
		height: 	0,
		hWndParent:	0,
		id:			0,
		instance:	0,
		param:		0,
		hWnd: 		0,
		parent:  	nil,
		disabled: 	false,
		visible:   	true,
		cursor:     nil,	// *goCursor
		font:       nil, 	// *goFont
		text: 	 	"",
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		backcolor:		0,
		forecolor:		0,
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}
	//screen := goWidget{"", "", 0, 0, 0, w32.CW_USEDEFAULT, w32.CW_USEDEFAULT, w32.CW_USEDEFAULT, w32.CW_USEDEFAULT, 0, 0, 0, 0, 0, nil, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	
	object := goObject{nil, []GoObject{}}
	devicecaps := goDeviceCaps{}
	hdesktop = &GoDeskTopWindow{screen, object, devicecaps}
	hdesktop.hWnd = hWnd
	hdesktop.instance = goApp.hInstance
	hdesktop.getDeviceCaps()
	return hdesktop
}



func StockDialogWindow() (hWin *GoWindowObj) {
	//var GoMainView *GoWindowObj
	GoTestView = GoWindow(nil)
	GoTestView.Show()
	return GoTestView
}