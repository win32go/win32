/* win32go/win32/lineedit.go */

package win32

import (
	"log"
	//"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoLineEdit(parent GoObject) (hLineEdit *GoLineEditObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 
	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 		"EDIT",
		windowName: 	"",
		style: 			w32.WS_CHILD | w32.ES_AUTOHSCROLL, // | w32.WS_VSCROLL,		//  | w32.SS_WHITEFRAME
		exStyle:    	w32.WS_EX_CLIENTEDGE,
		state:      	w32.SW_HIDE,
		x:      		0,
		y:      		0,
		width:  		400,
		height: 		28,
		hWndParent:		p_widget.hWnd,
		id:				p_nextid,
		instance:		p_widget.instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		border: 		BorderNone,
		cursor:     	nil,	// *goCursor
		font:       	nil, 	// *GoFont
		margin:			&GoMarginType{2, 2, 2, 2},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 		"",
		window:     	false,
		widgets:    	map[int]*goWidget{},
		alpha:			0, 		//uint8
		onShow:        	nil, 	//func()
		onClose:       	nil, 	//func()
		onCanClose:    	nil, 	//func() bool
		onMouseMove:   	nil, 	//func(x, y int)
		onMouseWheel:  	nil, 	//func(x, y int, delta float64)
		onMouseDown:   	nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     	nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     	nil, 	//func(key int)
		onKeyUp:       	nil, 	//func(key int)
		onResize:      	nil, 	//func()
	}
	object := goObject{parent, []GoObject{}}
	l := &GoLineEditObj{widget, object, 0, 0x7FFFFFFF, nil}
	
	_, err := l.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(l)
		GoApp().AddControl(l, p_nextid)
	}
	// Subclass Label Window to allow custom painting of label
	//l.SetBorderStyle(PanelBorderRaised)	
	if sizeToView == true {
		GoTestView.SizeToControl(l)
	}
	return l
}

type GoLineEditObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	groupID		int
	limit        int
	//autoHScroll  bool
	//writesTabs   bool
	//readOnly     bool
	onTextChange func()
}

func (l *GoLineEditObj) Font() *GoFont {
	return l.font
}

func (l *GoLineEditObj) SetFont(font *GoFont) {
	l.font = font
	if l.hWnd != 0 {
		w32.SendMessage(l.hWnd, w32.WM_SETFONT, uintptr(l.fontHandle()), 1)
	}
}

func (l *GoLineEditObj) SetText(text string) {
	l.text = text
	if l.hWnd != 0 {
		w32.SetWindowText(l.hWnd, text)
	}
}

func (l *GoLineEditObj) Show() {
	log.Println("GoLineEditObj.Show()")
	w32.ShowWindow(l.hWnd, w32.SW_SHOW)
	l.state = w32.SW_SHOW
	l.visible = true
	parent := l.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (l *GoLineEditObj) Text() string {
	if l.hWnd != 0 {
		l.text = w32.GetWindowText(l.hWnd)
	}
	return l.text
}

func (l *GoLineEditObj) destroy() {
	for _, ctl := range l.controls {
		ctl = nil	// delete control structure
		l.removeControl(ctl)
		//delete(f.controls, id)
	}
}

func (l *GoLineEditObj) fontHandle() w32.HFONT {
	if l.font != nil {
		return l.font.handle
	}
	/*if l.parent != nil {
		font := l.parent.Font()
		if font != nil {
			return font.handle
		}
	}*/
	return 0
}

func (l *GoLineEditObj) groupId() (id int) {
	return l.groupID
}

func (l *GoLineEditObj) isDialog() (bool) {
	return false
}

func (l *GoLineEditObj) isLayout() (bool) {
	return false
}

func (l *GoLineEditObj) objectType() (string) {
	return "GoLineEditObj"
}

func (l *GoLineEditObj) wid() (*goWidget) {
	return &l.goWidget
}