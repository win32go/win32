# Win32

Win32go main module.

The Qt framework is a splendid resource for developing in c++, and continually exploding into newer areas of developing. It has become very much overweight and resource hungry. I had been working on a few backend projects which were well suited to construction, using Golang language.

A recent project that I was involved in demanded a raw Windows Gui. I was not looking for a browser based solution. Two projects for windows Gui appeared in the searches.

1. Windows GUI Library. https://github.com/gonutz/wui - There is a more recent branch 'v2' within the same repository.

2. Windows Application Library Kit (WALK). https://github.com/lxn/walk - a well developed framework to produce Gui Applications

Both frameworks are based around the well tried principals of Windows Gui programming, creating a main window and running the main Windows event loop from the main window. Further Windows will require a new Windows class and their own event loop.


On Windows the top window is the screen DC. The **GoApplication** module retrieves the handle to the Windows screen object. From the screen object **GoApplication** has access to many system properties. The **GoApplication** module registers the main Windows class and also runs the main Windows event loop.

All new windows in Win32 are registered to the **GoApplication**, and can run from the main Windows event loop. This makes it simple to create an application with 2 or 3 main windows.

The **GoApplication** module doe not maintain a conventional Windows Stack. In stead a map of windows is maintained, mapping a **GoObject** to the Windows Handle (HWND). Each window is also indexed and a seperate mapping is maintained for indexing the Windows Handle (HWND) to the window's index.

The goal of the project is to try to make a GUI framework, that would feel comfortable to a programmer coming from Qt.

Initially the intention was to create a completely modular framework, allowing development of each aspect of the Windows GUI, individually. Whilst this is still the goal, to ease initial development it was decided to use standard golang packages.
