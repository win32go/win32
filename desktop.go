/* desktop.go */

package win32

import "gitlab.com/win32go/win32/w32"

type goDeviceCaps struct {
	aspectX 	int
	aspectY 	int
	aspectXY 	int
	clientHeight int
	clientWidth int
	height 		int
	width 		int
	horizRes 	int
	vertRes 	int
	horizSize 	int
	vertSize	int
}

type GoDeskTopWindow struct {
	goWidget		
	goObject
	goDeviceCaps
}

//func (d *GoDeskTopWindow) GetHWnd() (w32.HWND) {
//	return d.hWnd
//}

func (d *GoDeskTopWindow) wid() (*goWidget) {
	return &d.goWidget
}

func (d *GoDeskTopWindow) Screen() (*goDeviceCaps) {
	return &d.goDeviceCaps
}

func (d *GoDeskTopWindow) getDeviceCaps() {
	hWnd := d.goWidget.hWnd
	hDC := w32.GetDC(hWnd)
	d.goDeviceCaps.clientHeight = w32.GetSystemMetrics(w32.SM_CYFULLSCREEN)
	d.goDeviceCaps.clientWidth = w32.GetSystemMetrics(w32.SM_CXFULLSCREEN)
	d.goDeviceCaps.height = w32.GetSystemMetrics(w32.SM_CYSCREEN)
	d.goDeviceCaps.width = w32.GetSystemMetrics(w32.SM_CXSCREEN)
	d.goDeviceCaps.vertSize = w32.GetDeviceCaps(hDC, w32.VERTSIZE)
	d.goDeviceCaps.horizSize = w32.GetDeviceCaps(hDC, w32.HORZSIZE)
	d.goDeviceCaps.vertRes = w32.GetDeviceCaps(hDC, w32.LOGPIXELSY)
	d.goDeviceCaps.horizRes = w32.GetDeviceCaps(hDC, w32.LOGPIXELSX)
}

func (d *GoDeskTopWindow) ClientHeight() int {
	return d.goDeviceCaps.clientHeight
}

func (d *GoDeskTopWindow) ClientWidth() int {
	return d.goDeviceCaps.clientWidth
}

func (d *GoDeskTopWindow) Height() int {
	return d.goDeviceCaps.height
}

func (d *GoDeskTopWindow) HorizontalRes() int {
	return d.goDeviceCaps.horizRes
}

func (d *GoDeskTopWindow) HorizontalSize() int {
	return d.goDeviceCaps.horizSize
}

func (d *GoDeskTopWindow) VerticalRes() int {
	return d.goDeviceCaps.vertRes
}

func (d *GoDeskTopWindow) VerticalSize() int {
	return d.goDeviceCaps.vertSize
}

func (d *GoDeskTopWindow) Width() int {
	return d.goDeviceCaps.width
}
