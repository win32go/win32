/* win32go/image.go */

package win32

import (
  //"encoding/base64"
  //"errors"
  //"fmt"
  "image"
  "image/draw"
  "log"
  "os"
  "reflect"
  //"strings"
  "unsafe"
  _ "image/gif"
  _ "image/png"
  _ "image/jpeg"
	"gitlab.com/win32go/win32/w32"
)

/*func loadImage(ifile string) (*GoImageObj) {
  reader, open_err := os.Open(ifile)
  if open_err != nil {
     log.Println("File Open Error: ", open_err)
     return nil
  }
  defer reader.Close()
  img, _, decode_err := image.Decode(reader)
  if decode_err != nil {
    log.Println("File Open Error: ", decode_err)
    return nil
  }
  //bounds := m.Bounds()
  image := createImage(img)
  return image
}*/



func GoImage(ifile string) *GoImageObj {
  reader, open_err := os.Open(ifile)
  if open_err != nil {
     log.Println("File Open Error: ", open_err)
     return nil
  }
  defer reader.Close()
  img, _, decode_err := image.Decode(reader)
  if decode_err != nil {
    log.Println("File Open Error: ", decode_err)
    return nil
  }
  //bounds := m.Bounds()
  image := createImage(img)
  return image
}

// GoImage takes a handle to a bitmap (HBITMAP) and makes it an
// Image that you can use in Canvas.DrawImage.
/*func GoImage(bitmap uintptr, width int, height int) *GoImageObj {
  return &GoImageObj{
    bitmap: w32.HBITMAP(bitmap),
    width:  width,
    height: height,
  }
}*/

type GoImageObj struct {
  bitmap w32.HBITMAP
  width  int
  height int
}

func (im *GoImageObj) Width() int {
  return im.width
}

func (im *GoImageObj) Height() int {
  return im.height
}

func (im *GoImageObj) Size() (w, h int) {
  return im.width, im.height
}

func (im *GoImageObj) Bounds() *GoRectType {
  return GoRect().FromSize(0, 0, im.width, im.height)
}


/*func (im *GoImageObj) Save() bool{
  
}*/

func createImage(img image.Image) (*GoImageObj) {
  var bmp w32.BITMAPINFO
  bmp.BmiHeader.BiSize = uint32(unsafe.Sizeof(bmp.BmiHeader))
  bmp.BmiHeader.BiWidth = int32(img.Bounds().Dx())
  bmp.BmiHeader.BiHeight = -int32(img.Bounds().Dy())
  bmp.BmiHeader.BiPlanes = 1
  bmp.BmiHeader.BiBitCount = 32
  bmp.BmiHeader.BiCompression = w32.BI_RGB

  var bits unsafe.Pointer
  bitmap := w32.CreateDIBSection(0, &bmp, 0, &bits, 0, 0)
  rgba := toRGBA(img)
  pixels := rgba.Pix
  var dest []byte
  hdrp := (*reflect.SliceHeader)(unsafe.Pointer(&dest))
  hdrp.Data = uintptr(bits)
  hdrp.Len = len(pixels)
  hdrp.Cap = hdrp.Len
  // swap red and blue because we need BGR and not RGB on Windows
  for i := 0; i < len(pixels); i += 4 {
    dest[i+0] = pixels[i+2]
    dest[i+1] = pixels[i+1]
    dest[i+2] = pixels[i+0]
    dest[i+3] = pixels[i+3]
  }
  return &GoImageObj{
    bitmap: bitmap,
    width:  img.Bounds().Dx(),
    height: img.Bounds().Dy(),
  }
}

func toRGBA(img image.Image) *image.RGBA {
  if rgba, ok := img.(*image.RGBA); ok {
    return rgba
  }
  rgba := image.NewRGBA(img.Bounds())
  draw.Draw(rgba, rgba.Bounds(), img, rgba.Bounds().Min, draw.Src)
  return rgba
}