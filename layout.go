/* win32/layout.go */

package win32

import (
	"log"
	"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

var layoutWndProcPtr uintptr = syscall.NewCallback(layoutWndProc)

type GoLayoutStyle int

const (
	NoLayout 	GoLayoutStyle = iota
	HBoxLayout
	VBoxLayout
	HVBoxLayout
)

func GoLayout(parent GoObject, style GoLayoutStyle) (hLayout *GoLayoutObj) {
	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 	"STATIC",
		windowName: "",
		style: 		w32.WS_CHILD,
		exStyle:    0,
		state:      w32.SW_HIDE,
		x:      	0,
		y:      	0,
		width:  	p_widget.Width(),
		height: 	p_widget.Height(),
		hWndParent:	p_widget.hWnd,
		id:			p_nextid,
		instance:	p_widget.instance,
		param:		0,
		hWnd: 		0,
		parent:  	p_widget,
		disabled: 	false,
		visible:   	false,
		border: 	BorderNone,
		cursor:     nil,	// *goCursor
		font:       nil, 	// *goFont
		margin:		&GoMarginType{0, 0, 0, 0},
		padding:	&GoPaddingType{2, 2, 2, 2},
		sizePolicy:	&GoSizePolicy{ST_ExpandingWidth, ST_ExpandingHeight, false},
		spacing: 	5,
		text: 	 	"",
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		backMode: 		TransparentMode,
		backgroundMode:	TransparentMode,
		onShow:        nil, 	//func()
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}

	object := goObject{parent, []GoObject{}} ////map[int]GoObject{}}
	l := &GoLayoutObj{widget, object, style, false}
	
	_, err := l.Create()

	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent :", parent.objectType())
		log.Println("parent.addControl (GoLayout) :", p_nextid)
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(l)
		GoApp().AddControl(l, p_nextid)
	}

	//SUBCLASSPROC subClassWinProc;
	//w32.SetWindowSubclass(f.hWnd, subClassWinProc, f.id, NULL)
	
	w32.SetWindowSubclass(l.hWnd, layoutWndProcPtr, 0, 0)
	//l.SetBorderStyle(LayoutBorderSingleLine)
	return l
}



type GoLayoutObj struct {
	goWidget
	goObject
	style 		GoLayoutStyle
	topLevel 	bool
}

func (l *GoLayoutObj) AddControl(control GoObject) {
	p := control.parentControl()
	if p != nil {
		log.Println("GoLayoutObj::Addcontrol parent.removeControl..........")
		p.removeControl(control)
		//goApp.RemoveControl(control.wid().id)
	}
	oldhWnd := w32.SetParent(control.wid().hWnd, l.wid().hWnd)
	if oldhWnd != 0 {
		log.Println("GoLayoutObj::Addcontrol layout.addControl..........")
		control.wid().SetParent(l.wid())
		l.addControl(control)
		//goApp.AddControl(control, control.wid().id)
	} else {
		log.Println("w32.SetParent ERROR..........")
	}
}

func (l *GoLayoutObj) AddLayout(style GoLayoutStyle) (*GoLayoutObj) {
	layout := GoLayout(l, style)
	return layout
}

func (l *GoLayoutObj) Margin() (int, int, int, int) {
	return l.margin.Left, l.margin.Top, l.margin.Right, l.margin.Bottom
}

// pass to layout 

func (l *GoLayoutObj) Padding() (int, int, int, int) {
	return l.padding.Left, l.padding.Top, l.padding.Right, l.padding.Bottom
}

// WindowMode is used to control the Top Level Window sizing policy
//GoLayout::Fixed - The main widget's size is set to sizeHint(); it cannot be resized at all.
//GoLayout::Minimum - The main widget's minimum size is set to minimumSize(); it cannot be smaller.
//GoLayout::FreeResize - The widget is not constrained.
/*func (l *GoLayoutObj) WindowMode() (int){
	return l.mode
}*/





/*func (l *GoLayoutObj) SetBorderStyle(s goBorderStyle) {
	l.border = s
	if l.hWnd != 0 {
		style := uint(w32.GetWindowLongPtr(l.hWnd, w32.GWL_STYLE))
		style = style &^ w32.WS_BORDER &^ w32.WS_DLGFRAME
		style |= layoutBorderStyle(s)
		w32.SetWindowLongPtr(l.hWnd, w32.GWL_STYLE, uintptr(style))

		exStyle := uint(w32.GetWindowLongPtr(l.hWnd, w32.GWL_EXSTYLE))
		exStyle = exStyle &^ w32.WS_EX_STATICEDGE &^ w32.WS_EX_CLIENTEDGE
		exStyle |= layoutBorderStyleEx(s)
		w32.SetWindowLongPtr(l.hWnd, w32.GWL_EXSTYLE, uintptr(exStyle))
		w32.InvalidateRect(l.wid().parent.hWnd, nil, true)
	}
}*/

func (l *GoLayoutObj) Resize() {
	l.repack()
	
	if l.onResize != nil {
		l.onResize(l.GetClientRect())
	}
}

func (l *GoLayoutObj) SetMargin(left, top, right, bottom int) {
	l.margin = &GoMarginType{left, top, right, bottom}
	//l.repack()
}

func (l *GoLayoutObj) SetPadding(left, top, right, bottom int) {
	l.padding = &GoPaddingType{left, top, right, bottom}
	//l.repack()
}

func (l *GoLayoutObj) SetSpacing(spacing int) {
	l.spacing = spacing
	//l.repack()
}

func (l *GoLayoutObj) SetStyle(style GoLayoutStyle) {
	l.style = style
	//l.repack()
}

func (l *GoLayoutObj) Show() {
	log.Println("GoLayoutObj.Show()")
	w32.ShowWindow(l.hWnd, w32.SW_SHOW)
	l.state = w32.SW_SHOW
	l.visible = true
	parent := l.parentControl()
	if parent.objectType() == "GoLayoutObj" {
		parent.(*GoLayoutObj).onWM_SIZE(0, 0)
	} //else if parent.objectType() == "GoWindowView" {
		//parent.(*GoWindowView).onWM_SIZE(0, 0)
	//}
}

func (l *GoLayoutObj) Spacing() (spacing int) {
	return l.spacing
}

func (l *GoLayoutObj) Style() (style GoLayoutStyle) {
	return l.style
}







func (l *GoLayoutObj) destroy() {
	//w32.RemoveWindowSubClass(f.hWnd, subClassWinProc, f.id)
	for _, ctl := range l.controls {	
		ctl = nil 				// delete control structure
		l.removeControl(ctl) 	// remove from Control Stack
	}
	w32.RemoveWindowSubclass(l.hWnd, layoutWndProcPtr, 0)
}

func (l *GoLayoutObj) drawItem(drawItemStruct *w32.DRAWITEMSTRUCT) {

}

func (l *GoLayoutObj) groupId() (id int) {
	return 0
}

func (l *GoLayoutObj) handleNotification(cmd uintptr) {
}

func (l *GoLayoutObj) isDialog() (bool) {
	return false
}

func (l *GoLayoutObj) isLayout() (bool) {
	return true
}

func (l *GoLayoutObj) isTopLevel() (bool) {
	return l.topLevel
}

func layoutWndProc(hWnd w32.HWND, msg uint32, wParam, lParam uintptr, subclassID uintptr, refData uintptr) uintptr {
	//log.Println("LayoutWndProc:: - ")
	lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF
	var layout *GoLayoutObj
	ctrl := GoApp().Control(hWnd)
	layout = ctrl.(*GoLayoutObj)
	switch msg {
		case w32.WM_ERASEBKGND:
			log.Println("Layout::WM_ERASEBACKGROUND - ")
			hDC := w32.HDC(wParam)
			rect := layout.GetClientRect()

			layout.paintBackground(hDC, rect)
			return 1
		case w32.WM_LBUTTONUP, w32.WM_MBUTTONUP, w32.WM_RBUTTONUP:
			log.Println("GoLayout:WM_WM_LBUTTONUP.......")
			mouseX := int(lParamL)
			mouseY := int(lParamH)
			//w := win.wid()
			/*if w.onMouseUp != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONUP {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONUP {
					b = MouseButtonRight
				}
				w.onMouseUp(b, mouseX, mouseY)
				log.Println("GoApp::return 0....HWnd", w.hWnd)
				return 0
			} else {*/
				log.Println("WM_WM_LBUTTONUP x:", mouseX, " y:", mouseY)
				return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
			//}
		/*case w32.WM_MOUSEMOVE:
			//log.Println("GoLayout:WM_WM_MOUSEMOVE.......")
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)

			//w := win.wid()
			if w.onMouseMove != nil {
				w.onMouseMove(mouseX, mouseY)
				return 0
			}*/
		case w32.WM_PAINT:
			log.Println("GoLayout:WM_PAINT.......")
			var ps w32.PAINTSTRUCT
			//var rect *GoRectType
			hDC := w32.BeginPaint(hWnd, &ps)
			rect := layout.GetClientRect()

			layout.paint(hDC, rect)
		    //layout.paint(hDC, rect.W32Rect())
		    w32.EndPaint(hWnd, &ps)
			return 0
		case w32.WM_SIZE:
			log.Println("GoLayout:WM_SIZE.......", hWnd)
			layout.onWM_SIZE(wParam, lParam)
			return 0
		case w32.WM_COMMAND:
			log.Println("GoLayout:WM_COMMAND")
			layout.onWM_COMMAND(wParam, lParam)
			return 0
			//return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		case w32.WM_DRAWITEM:
			//log.Println("GoLayout:WM_DRAWITEM")
			layout.onWM_DRAWITEM(wParam, lParam)
			return 0
		case w32.WM_NOTIFY:
			log.Println("GoLayout:WM_NOTIFY")
			layout.onWM_NOTIFY(wParam, lParam)
			return 0
		case w32.WM_CTLCOLORSTATIC:
			log.Println("Layout:WM_CTLCOLORSTATIC :*********************************************")
        	hDC := w32.HDC(wParam)
        	hWnd := w32.HWND(lParam)
        	win := GoApp().Control(hWnd)
        	w := win.wid()
        	//w32.SelectObject(hDC, w32.GetStockObject(w32.DC_BRUSH))
        	//w32.SetDCBrushColor(hDC, colorToW32(e.backColor))
        	w32.SetTextColor(hDC, colorToW32(w.forecolor))
        	w32.SetBkColor(hDC, colorToW32(w.backcolor))

	        //return w32.GetStockObject( w32.COLOR_3DHILIGHT )
	        return uintptr(w32.GetSysColorBrush( w32.COLOR_3DHILIGHT ))
			//return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		case w32.WM_CTLCOLOREDIT:
			log.Println("Layout::WM_CTLCOLOREDIT....................................................")
			hDC := w32.HDC(wParam)
        	hWnd := w32.HWND(lParam)
        	win := GoApp().Control(hWnd)
        	w := win.wid()
        	hGDIObj := w32.GetStockObject(w32.DC_BRUSH)
        	w32.SelectObject(hDC, hGDIObj)
        	w32.SetDCBrushColor(hDC, colorToW32(w.backgroundcolor)) //w.backcolor))
			w32.SetTextColor(hDC, colorToW32(w.forecolor))
        	w32.SetBkColor(hDC, colorToW32(w.backcolor))
        	w32.SetBkMode(hDC, int(w.backMode))
			return uintptr(w32.HBRUSH(hGDIObj)) //GetSysColorBrush( w32.COLOR_GRAYTEXT ))
		default:
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
	}
}

func (w *GoLayoutObj) objectType() (string) {
	return "GoLayoutObj"
}

func (l *GoLayoutObj) onWM_SIZE(wParam, lParam uintptr) {
	l.repack()
	
	if l.onResize != nil {
		l.onResize(l.GetClientRect())
	}
}

func (l *GoLayoutObj) repack() {
	//log.Println("GoLayout:repack()")
	//log.Println("GoLayoutObj.width =", l.width, "padding.left =", l.padding.left, "padding.right =", l.padding.right)
	var rcClient *GoRectType
	var rcWind *GoRectType

	rcClient = l.GetClientRect()
	rcWind = l.GetWindowRect()
	log.Println("rcWind : ", rcWind.left, rcWind.top, rcWind.width, rcWind.height)
	log.Println("rcClient : ", rcClient.left, rcClient.top, rcClient.width, rcClient.height)
	log.Println("lsize : ", 0, 0, l.width, l.height)

	var iwidth int = rcClient.width - l.padding.Left - l.padding.Right
	var iheight int = rcClient.height - l.padding.Top - l.padding.Bottom
	var expwidth int = iwidth
	var expheight int = iheight
	var expandingControls int = 0
	var ctrl *goWidget
	if l.style == HBoxLayout {
		//log.Println("GoLayoutObj(HBOXLAYOUT)")
		
		for _, ctl := range l.controls {
			switch ctl.objectType() {
				case "GoLayoutObj":
					ctrl = ctl.wid()
				case "GoButtonObj":
					ctrl = ctl.wid()
				case "GoLabelObj":
					ctrl = ctl.wid()
				case "GoLineEditObj":
					ctrl = ctl.wid()
				case "GoCheckBoxObj":
					ctrl = ctl.wid()
				case "GoPanelObj":
					ctrl = ctl.wid()
				case "GoRadioButtonObj":
					ctrl = ctl.wid()
				case "GoTextEditObj":
					ctrl = ctl.wid()
				case "GoViewObj":
					ctrl = ctl.wid()
			}
			if ctrl.sizePolicy.Horiz == ST_FixedWidth {
				expwidth = expwidth - ctrl.margin.Left - ctrl.width - ctrl.margin.Right - l.spacing
			} else if ctrl.sizePolicy.Horiz == ST_ExpandingWidth {
				expandingControls++
				if expandingControls > 1 {
					expwidth = expwidth - l.spacing
				}
			}
		}
	} else if l.style == VBoxLayout {
		//log.Println("GoLayoutObj(VBOXLAYOUT)")
		var ctrl *goWidget
		for _, ctl := range l.controls {
			switch ctl.objectType() {
				case "GoLayoutObj":
					ctrl = ctl.wid()
				case "GoButtonObj":
					ctrl = ctl.wid()
				case "GoLabelObj":
					ctrl = ctl.wid()
				case "GoLineEditObj":
					ctrl = ctl.wid()
				case "GoCheckBoxObj":
					ctrl = ctl.wid()
				case "GoPanelObj":
					ctrl = ctl.wid()
				case "GoRadioButtonObj":
					ctrl = ctl.wid()
				case "GoTextEditObj":
					ctrl = ctl.wid()
				case "GoViewObj":
					ctrl = ctl.wid()
			}
			if ctrl.sizePolicy.Vert == ST_FixedHeight {
				//expheight = expheight - ctrl.margin.Top - ctrl.height - ctrl.margin.Bottom - l.spacing
				expheight = expheight - ctrl.height - l.spacing
			} else if ctrl.sizePolicy.Vert == ST_ExpandingHeight {
				expandingControls++
				if expandingControls > 1 {
					expheight = expheight - l.spacing
				}
			}
		}
	}
	log.Println("iwidth =", iwidth, "expwidth =", expwidth)
	log.Println("iheight =", iheight, "expheight =", expheight)
	var posX int = l.padding.Left
	var posY int = l.padding.Top
	for _, ctl := range l.controls {
		switch ctl.objectType() {
			case "GoLayoutObj":
				ctrl = ctl.wid()
			case "GoButtonObj":
				ctrl = ctl.wid()
			case "GoLabelObj":
				ctrl = ctl.wid()
			case "GoLineEditObj":
					ctrl = ctl.wid()
			case "GoCheckBoxObj":
				ctrl = ctl.wid()
			case "GoPanelObj":
				ctrl = ctl.wid()
			case "GoRadioButtonObj":
				ctrl = ctl.wid()
			case "GoTextEditObj":
				ctrl = ctl.wid()
			case "GoViewObj":
				ctrl = ctl.wid()
		}
		if l.style == HBoxLayout {
			//log.Println("layout(HBOXLAYOUT)")
			posX += ctrl.margin.Left
			if ctrl.sizePolicy.Vert == ST_ExpandingHeight {
				ctrl.height = iheight - ctrl.margin.Top - ctrl.margin.Bottom //  - l.padding.top - l.padding.bottom
			}
			if ctrl.sizePolicy.Horiz == ST_FixedWidth {
				ctrl.SetPosition(posX, posY + ctrl.margin.Top)
				posX += ctrl.width + ctrl.margin.Right + l.spacing
			} else if ctrl.sizePolicy.Horiz == ST_ExpandingWidth {
				ctrl.width = ((expwidth / expandingControls) - ctrl.margin.Left - ctrl.margin.Right)
				//log.Println("ctrl.width =", ctrl.width)
				ctrl.SetPosition(posX, posY + ctrl.margin.Top)
				posX += ctrl.width + ctrl.margin.Right + l.spacing
			}
		} else if l.style == VBoxLayout {
			log.Println("layout(VBOXLAYOUT)")
			posY += ctrl.margin.Top
			if ctrl.sizePolicy.Horiz == ST_ExpandingWidth {
				ctrl.width = iwidth - ctrl.margin.Left - ctrl.margin.Right //  - l.padding.left - l.padding.right
			}
			if ctrl.sizePolicy.Vert == ST_FixedHeight {
				ctrl.SetPosition(posX + ctrl.margin.Left, posY)
				posY += ctrl.height + ctrl.margin.Bottom + l.spacing
			} else if ctrl.sizePolicy.Vert == ST_ExpandingHeight {
				ctrl.height = ((expheight / expandingControls) - ctrl.margin.Top - ctrl.margin.Bottom)
				ctrl.SetPosition(posX + ctrl.margin.Left, posY)
				posY += ctrl.height + ctrl.margin.Bottom + l.spacing
			}
		}
	}
}

func (l *GoLayoutObj) wid() (*goWidget) {
	return &l.goWidget
}