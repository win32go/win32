/* win32/brush.go */

/*	
	GoBrushStyle:

	NoBrush 		 =  0
	SolidPattern 	 =  1
	Dense1Pattern 	 =  2
	Dense2Pattern 	 =  3
	Dense3Pattern 	 =  4
	Dense4Pattern 	 =  5
	Dense5Pattern 	 =  6
	Dense6Pattern 	 =  7
	Dense7Pattern 	 =  8
	HorPattern 		 =  9
	VerPattern 		 = 10
	CrossPattern 	 = 11
	BDiagPattern 	 = 12
	FDiagPattern 	 = 13
	DiagCrossPattern = 14
	CustomPattern 	 = 24
*/



package win32

import (
	//"log"
	"gitlab.com/win32go/win32/w32"
)

/*type LOGBRUSH struct {
	LbStyle uint32
	LbColor COLORREF
	LbHatch uintptr
}*/
func CreateSolidBrush(color GoColor, ref string) (*GoBrush) {
	gobrush := &GoBrush{color: color, style: SolidPattern, ref: ref}
	//gobrush.hbrush = w32.CreateSolidBrush(color.W32Color())
	return gobrush
}

func CreateBrush(color GoColor, style GoBrushStyle, ref string) (*GoBrush) {
	if ref == "" {
		return nil
	}
	gobrush := &GoBrush{color: color, style: style, ref: ref}
	//gobrush.hbrush = w32.CreatePen(style, width, color.W32Color())
	
	return gobrush
}

type GoBrush struct {
	hbrush 	w32.HBRUSH
	color 	GoColor
	style 	GoBrushStyle
	ref		string
}

func (b *GoBrush) Create() (w32.HBRUSH) {
	b.hbrush = b.createBrush(b.style, colorToW32(b.color))
	return b.hbrush
}

func (b *GoBrush) createBrush(style GoBrushStyle, color w32.COLORREF) (w32.HBRUSH) {
	if style == SolidPattern {
		b.hbrush = w32.CreateSolidBrush(color)
	}
	return b.hbrush
}