/* panel.go */

package win32

import (
	"log"
	"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoPanel(parent GoObject) (hPanel *GoPanelObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 

	p_nextid := nextControlId()
	p_widget := parent.wid()

	widget := goWidget{
		className: 	"STATIC",
		windowName: "",
		style: 		w32.WS_CHILD | w32.SS_OWNERDRAW, // | w32.WS_THICKFRAME,
		exStyle:    0,
		state:      w32.SW_HIDE,
		x:      	0,
		y:      	0,
		width:  	400,
		height: 	300,
		hWndParent:	p_widget.hWnd,
		id:			p_nextid,
		instance:	p_widget.instance,
		param:		0,
		hWnd: 		0,
		parent:  	p_widget,
		disabled: 	false,
		visible:   	false,
		border: 	BorderNone,
		cursor:     nil,	// *goCursor
		font:       nil, 	// *goFont
		margin:		&GoMarginType{0, 0, 0, 0},
		padding:	&GoPaddingType{0, 0, 0, 0},
		sizePolicy:	&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 	"",
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		onShow:        nil, 	//func()
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}

	object := goObject{parent, []GoObject{}}
	p := &GoPanelObj{widget, object, nil}
	
	_, err := p.Create()

	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p)
		GoApp().AddControl(p, p_nextid)
		p.layout = GoLayout(p, NoLayout)
		p.layout.Show()
	}
	// Subclass Frame Window to allow passage of messages back up to top level windows
	w32.SetWindowSubclass(p.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		case w32.WM_PAINT:
			log.Println("GoPanel:WM_PAINT.......")
			var ps w32.PAINTSTRUCT
			hdc := w32.BeginPaint(hWnd, &ps)
		    rect := p.GetClientRect()
		    pt := CreatePainter(hdc, rect)
		    pt.SetStockBrush(p.backcolor)
		    pt.SetNullPen()
		    pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
		    //w32.DrawFocusRect(hdc, rect.W32RECT())
		    pt.Destroy()
		    w32.EndPaint(hWnd, &ps)
			return 0
		case w32.WM_COMMAND:
			log.Println("GoPanel:WM_COMMAND")
			p.onWM_COMMAND(wParam, lParam)
			return 0
		case w32.WM_DRAWITEM:
			log.Println("GoPanel:WM_DRAWITEM")
			p.onWM_DRAWITEM(wParam, lParam)
			return 0
		case w32.WM_NOTIFY:
			log.Println("GoPanel:WM_NOTIFY")
			p.onWM_NOTIFY(wParam, lParam)
			return 0
		case w32.WM_SIZE:
			log.Println("GoPanel:WM_SIZE.....")
			layout := p.Layout()
			rc := w32.GetClientRect(p.hWnd)
			layout.SetSize(int(rc.Right), int(rc.Bottom))
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		default:
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		}
	}), 0, 0)
	//p.SetBorderStyle(BorderRaised)
	if sizeToView == true {
		GoTestView.SizeToControl(p)
	}
	return p
}

type GoPanelObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	layout		*GoLayoutObj
}

func (f *GoPanelObj) AddControl(control GoObject) {
	f.layout.AddControl(control)
}

func (f *GoPanelObj) AddLayout(style GoLayoutStyle) (*GoLayoutObj) {
	layout := GoLayout(f.layout, style)
	return layout
}

func (f *GoPanelObj) Layout() (layout *GoLayoutObj) {
	return f.layout
}

func (f *GoPanelObj) LayoutMarginSetLeft(left int) {
	f.layout.margin.Left = left
}

func (f *GoPanelObj) LayoutMarginSetTop(top int) {
	f.layout.margin.Top = top
}

func (f *GoPanelObj) LayoutMarginSetRight(right int) {
	f.layout.margin.Right = right
}

func (f *GoPanelObj) LayoutMarginSetBottom(bottom int) {
	f.layout.margin.Bottom = bottom
}

func (f *GoPanelObj) LayoutPaddingSetLeft(left int) {
	f.layout.padding.Left = left
}

func (f *GoPanelObj) LayoutPaddingSetTop(top int) {
	f.layout.padding.Top = top
}

func (f *GoPanelObj) LayoutPaddingSetRight(right int) {
	f.layout.padding.Right = right
}

func (f *GoPanelObj) LayoutPaddingSetBottom(bottom int) {
	f.layout.padding.Bottom = bottom
}

func (f *GoPanelObj) LayoutStyle() (style GoLayoutStyle) {
	return f.layout.Style()
}

func (f *GoPanelObj) SetLayoutHeight(height int) {
	f.layout.height = height
	f.layout.setGeometry()
}

func (f *GoPanelObj) SetLayoutMargin(left int, top int, right int, bottom int) {
	f.layout.margin = &GoMarginType{left, top, right, bottom}
}

func (f *GoPanelObj) SetLayoutPadding(left int, top int, right int, bottom int) {
	f.layout.padding = &GoPaddingType{left, top, right, bottom}
}

func (f *GoPanelObj) SetLayoutSizePolicy(horiz GoSizeType, vert GoSizeType, fixed bool) {
	sizePolicy := GetSizePolicy(horiz, vert, fixed)
	f.layout.sizePolicy = sizePolicy
}

func (f *GoPanelObj) SetLayoutStyle(style GoLayoutStyle) {
	f.layout.SetStyle(style)
}

func (f *GoPanelObj) SetLayoutWidth(width int) {
	f.layout.width = width
	f.layout.setGeometry()
}

func (p *GoPanelObj) Show() {
	log.Println("GoPanelObj.Show()")
	w32.ShowWindow(p.hWnd, w32.SW_SHOW)
	p.state = w32.SW_SHOW
	p.visible = true
	parent := p.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}

}

func (f *GoPanelObj) Update() {
	w32.UpdateWindow(f.layout.hWnd)
}

func (f *GoPanelObj) destroy() {
	for _, ctl := range f.controls {
		ctl = nil	// delete control structure
		f.removeControl(ctl)
		//delete(f.controls, id)
	}
}

func (r *GoPanelObj) drawItem(drawItemStruct *w32.DRAWITEMSTRUCT) {

}

func (f *GoPanelObj) groupId() (id int) {
	return 0
}

func (r *GoPanelObj) handleNotification(cmd uintptr) {
}

func (f *GoPanelObj) isDialog() (bool) {
	return false
}

func (f *GoPanelObj) isLayout() (bool) {
	return false
}

func (l *GoPanelObj) objectType() (string) {
	return "GoPanelObj"
}

func (f *GoPanelObj) wid() (*goWidget) {
	return &f.goWidget
}



