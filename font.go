package win32

import (
	"errors"
	"strconv"
	"unicode/utf16"

	"gitlab.com/win32go/win32/w32"
)

// Stock Logical Font Objects - (Declared in namespace.go)
/*const (
	OEM_FIXED_FONT      = 10
	ANSI_FIXED_FONT     = 11
	ANSI_VAR_FONT       = 12
	SYSTEM_FONT         = 13
	DEVICE_DEFAULT_FONT = 14
	DEFAULT_PALETTE     = 15
	SYSTEM_FIXED_FONT   = 16
	DEFAULT_GUI_FONT    = 17
)*/

// Font weight constants - (Declared in namespace.go)
/*const (
	FW_Default    = 0
	FW_Thin       = 100
	FW_ExtraLight = 200
	FW_UltraLight = FW_ExtraLight
	FW_Light      = 300
	FW_Normal     = 400
	FW_Regular    = 400
	FW_Medium     = 500
	FW_SemiBold   = 600
	FW_DemiBold   = FW_SemiBold
	FW_Bold       = 700
	FW_ExtraBold  = 800
	FW_UltraBold  = FW_ExtraBold
	FW_Heavy      = 900
	FW_Black      = FW_Heavy
)*/

const (
	OEM_FixedFont      = 10
	ANSI_FixedFont     = 11
	ANSI_VariFont      = 12
	System_Font        = 13
	Default_DeviceFont = 14
	System_FixedFont   = 16
	Default_GUIFont    = 17
)

type GoFontDesc struct {
	Name       string
	Height     int
	Weight     int
	Italic     bool
	Underlined bool
	StrikedOut bool
}

func CreateFont(family string, pointSize int, weight int, italic bool) (*GoFont, error) {
	desc := GoFontDesc{family, pointSize, weight, italic, false, false}
	byteBool := func(b bool) byte {
		if b {
			return 1
		}
		return 0
	}
	logfont := w32.LOGFONT{
		Height:         int32(desc.Height),
		Width:          0,
		Escapement:     0,
		Orientation:    0,
		Weight:         int32(weight),
		Italic:         byteBool(desc.Italic),
		Underline:      byteBool(desc.Underlined),
		StrikeOut:      byteBool(desc.StrikedOut),
		CharSet:        w32.DEFAULT_CHARSET,
		OutPrecision:   w32.OUT_CHARACTER_PRECIS,
		ClipPrecision:  w32.CLIP_CHARACTER_PRECIS,
		Quality:        w32.DEFAULT_QUALITY,
		PitchAndFamily: w32.DEFAULT_PITCH | w32.FF_DONTCARE,
	}
	copy(logfont.FaceName[:], utf16.Encode([]rune(desc.Name)))

	handle := w32.CreateFontIndirect(&logfont)
	if handle == 0 {
		return nil, errors.New("ui.NewFont: unable to create font, please check your description")
	}
	ref := family + ":" + strconv.Itoa(pointSize)
	return &GoFont{Desc: desc, handle: handle, ref: ref}, nil
}

type GoFont struct {
	Desc   	GoFontDesc
	handle 	w32.HFONT
	ref		string
}