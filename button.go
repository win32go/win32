/* button.go */

package win32

import (
	//"bufio"
	"log"
	//"os"
	"syscall"
	"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoButton(parent GoObject, text string) (hButton *GoButtonObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 

	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 	"BUTTON", 
		windowName: text,
		style: 		w32.WS_CHILD | w32.BS_PUSHBUTTON | w32.BS_OWNERDRAW | w32.BS_NOTIFY,
		exStyle:    0,
		state:      w32.SW_HIDE,
		x:      	0,
		y:      	0,
		width:  	100,
		height: 	20,
		hWndParent:	p_widget.hWnd,
		id:			p_nextid,
		instance:	p_widget.instance,
		param:		0,
		hWnd: 		0,
		parent:  	p_widget,
		disabled: 	false,
		visible:   	false,
		autoSize:	false,
		border:		BorderNone,
		cursor:     nil,	// *goCursor
		font:       nil, 	// *goFont
		margin:		&GoMarginType{5, 5, 5, 5},
		padding:	&GoPaddingType{10, 5, 10, 5},
		sizePolicy:	&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 	text,
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		onShow:        nil, 	//func()
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}
	
	object := goObject{parent, []GoObject{}}
	b := &GoButtonObj{widget, object, 0, nil, nil, nil, nil, nil, nil}
	
	_, err := b.Create()

	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		b.SetBackgroundMode(TransparentMode)
		log.Println("parent :", parent.objectType())
		log.Println("parent.addControl (GoButton) :", p_nextid)
		parent.addControl(b)
		goApp.AddControl(b, p_nextid)
	}

	// Subclass Label Window to allow custom painting of label
	w32.SetWindowSubclass(b.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		case w32.WM_PAINT:
			var ps w32.PAINTSTRUCT
			hDC := w32.BeginPaint(hWnd, &ps)
			if b.backgroundMode == TransparentMode {
				w32.SetBkMode(hDC, w32.TRANSPARENT)
			}
			rect := b.GetClientRect()
			rc := rect.DeductPadding(b.padding)
			pt := CreatePainter(hDC, rect)
			pt.SetTextColor(b.buttonText)
			if b.mousehover {
				if b.mousedown {
					pt.SetStockBrush(b.facecolor)
					pt.SetNullPen()
				} else {
					pt.SetTextColor(b.highlightText)
					pt.SetStockBrush(b.highlight)
					pt.SetStockPen(b.highlight)
					//pt.SetNullPen()
				}
			} else {
				pt.SetStockBrush(b.facecolor)
				pt.SetStockPen(b.facecolor)
				//pt.SetNullPen()
			}
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
			pt.DrawTextLine(b.text, rc, w32.DT_CENTER, w32.DT_VCENTER)  
			//w32.DrawFocusRect(hdc, rect.W32RECT())
			pt.Destroy()
		    w32.EndPaint(hWnd, &ps)
			return 0
		case w32.WM_DRAWITEM:
			log.Println("GoButton:WM_DRAWITEM.......")
/*case WM_DRAWITEM:
	LPDRAWITEMSTRUCT lpDrawItem;
	HBRUSH hBrush;
	int state;

	lpDrawItem = (LPDRAWITEMSTRUCT) lParam;
	SetTextAlign(lpDrawItem->hDC, TA_CENTER | TA_BOTTOM);
	hBrush = CreateSolidBrush(RGB(255, 0, 0));
	SelectObject(lpDrawItem->hDC, hBrush);
	SetPolyFillMode(lpDrawItem->hDC, ALTERNATE);
	SetBkColor(lpDrawItem->hDC, RGB(255, 0, 0));

	if (lpDrawItem->CtlType != ODT_BUTTON)
		return FALSE;

	state = lpDrawItem->itemState;

	if(state & ODS_SELECTED)
	{
		Ellipse(lpDrawItem->hDC, 0, 0, 20, 20);
		TextOut(lpDrawItem->hDC, 10, 18, "X", 1);
	}
	else 
	{
		Ellipse(lpDrawItem->hDC, 0, 0, 20, 20);
		TextOut(lpDrawItem->hDC, 10, 18, "5", 1);
	}

	DeleteObject(hBrush);
	return true;*/
		case w32.WM_MOUSEMOVE:          
			if !b.mousehover {
				log.Println("GoButton:WM_MOUSEHOVER.......")
			    b.mousehover = true
			    w32.InvalidateRect(b.hWnd, nil, true)
			    //SendMessage(hWnd, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBmpHiLight );
			    tme := w32.TRACKMOUSEEVENT{}
			    tme.CbSize = uint32(unsafe.Sizeof(tme))
			    tme.DwFlags = w32.TME_LEAVE
			    tme.HwndTrack = b.hWnd
			    tme.DwHoverTime = 400
			    w32.TrackMouseEvent(&tme)

			}
			return 0
        case w32.WM_MOUSELEAVE:
        	log.Println("GoButton:WM_MOUSELEAVE.......")
			b.mousehover = false
			w32.InvalidateRect(b.hWnd, nil, true)
			return 0
		case w32.WM_COMMAND:
			log.Println("GoButton:WM_COMMAND.......")
			return 0
		}
		return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
	}), 0, 0)


	if sizeToView == true {
		GoTestView.SizeToControl(b)
	}
	return b
}

type GoButtonObj struct {
	goWidget
	goObject
	groupIdx int
	//autoSize bool
	onClick func()
	onDisable func()
	onDoubleClick func()
	onHiLite func()
	onPaint func()
	onUnHiLite func()
}

func (b *GoButtonObj) AutoSize() (autoSize bool) {
	return b.autoSize
}

func (b *GoButtonObj) SetAutoSize(autoSize bool) {
	b.autoSize = autoSize
	b.setGeometry()
}

func (b *GoButtonObj) SetGroupId(groupId int) {
	b.groupIdx = groupId
}

func (b *GoButtonObj) Show() {
	w32.ShowWindow(b.hWnd, w32.SW_SHOW)
	b.state = w32.SW_SHOW
	b.visible = true
	parent := b.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (b *GoButtonObj) OnClick() func() {
	return b.onClick
}

func (b *GoButtonObj) SetOnClick(f func()) {
	b.onClick = f
}

func (b *GoButtonObj) OnDisable() func() {
	return b.onDisable
}

func (b *GoButtonObj) SetOnDisable(f func()) {
	b.onDisable = f
}

func (b *GoButtonObj) OnDoubleClick() func() {
	return b.onDoubleClick
}

func (b *GoButtonObj) SetOnDoubleClick(f func()) {
	b.onDoubleClick = f
}

func (b *GoButtonObj) OnHiLite() func() {
	return b.onHiLite
}

func (b *GoButtonObj) SetOnHiLite(f func()) {
	b.onHiLite = f
}

func (b *GoButtonObj) OnPaint() func() {
	return b.onPaint
}

func (b *GoButtonObj) SetOnPaint(f func()) {
	b.onPaint = f
}

func (b *GoButtonObj) OnUnHiLite() func() {
	return b.onUnHiLite
}

func (b *GoButtonObj) SetOnUnHiLite(f func()) {
	b.onUnHiLite = f
}
/*func (b *GoButtonObj) Create() {
	b.goWidget.Create()
}*/

/*func (b *GoButtonObj) Show() {
	w32.ShowWindow(b.hWnd, w32.SW_SHOW)
}*/

/*func (b *GoButtonObj) Update() {
	w32.UpdateWindow(b.hWnd)
}*/

/*func (b *GoButtonObj) obj() (*goObject) {
	return &b.goObject
}*/

/*func (b *GoButtonObj)addControl(id int, control GoObject) {
	b.goObject.addControl(id, control) 
}

func (b *GoButtonObj)control(id int) (GoObject) {
	return b.goObject.control(id) 
}

func (b *GoButtonObj)parentControl() (GoObject) {
	return b.goObject.parentControl() 
}

func (b *GoButtonObj)removeControl(control GoObject) {
	b.goObject.removeControl(control)
}*/

func (b *GoButtonObj) destroy() {
	/*for id, control := range w.controls {
		delete(w.controls, id)
	}*/
}

func (b *GoButtonObj) groupId() (id int) {
	return b.groupIdx
}

func (b *GoButtonObj) isDialog() (bool) {
	return false
}

func (b *GoButtonObj) isLayout() (bool) {
	return false
}

func (b *GoButtonObj) objectType() (string) {
	return "GoButtonObj"
}

func (b *GoButtonObj) wid() (*goWidget) {
	return &b.goWidget
}

func (b *GoButtonObj) drawItem(s *w32.DRAWITEMSTRUCT) {
	log.Println("GoButtonObj::drawItemStruct..............")
	//CtlType    uint32
	//CtlID      uint32
	//ItemID     uint32
	//ItemAction uint32
	//ItemState  uint32
	//HwndItem   HWND
	//HDC        HDC
	//RcItem     RECT
	//ItemData   uintptr
	state := s.ItemState
	if (state & w32.ODS_SELECTED) == 1 {
		b.setBorder(BorderSunkenThick)
		b.mousedown = true
		w32.InvalidateRect(b.hWnd, nil, true)
	} else {
		b.setBorder(b.border)
		b.mousedown = false
		w32.InvalidateRect(b.hWnd, nil, true)
	}
	/*hDC := s.HDC
	w32.SetBkMode(hDC, w32.TRANSPARENT)
	rect := GoRect().FromW32Rect(&s.RcItem)
	rc := rect.DeductPadding(b.padding)
	pt := CreatePainter(hDC, rect)
	if (state & w32.ODS_SELECTED) == 1 {
		pt.SetStockBrush(b.facecolor)
		pt.SetStockPen(b.grayText)
	} else {
		pt.SetStockBrush(b.backcolor)
		pt.SetStockPen(b.hotlight)
	}
	//pt.SetNullPen()
	pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
	if (state & w32.ODS_SELECTED) == 1 {
		pt.SetStockPen(b.buttonText)
	} else {
		pt.SetStockPen(b.forecolor)
	}
	pt.DrawTextLine(b.text, rc, w32.DT_CENTER, w32.DT_VCENTER)  
	//w32.DrawFocusRect(hdc, rect.W32RECT())
	pt.Destroy()*/
}

func (b *GoButtonObj) handleNotification(cmd uintptr) {
	log.Println("GoButtonObj::handleNotification..............")
	//buf := bufio.NewReader(os.Stdin)
    //buf.ReadBytes('\n')
	if cmd == w32.BN_CLICKED && b.onClick != nil {
		b.onClick()
	} else if cmd == w32.BN_DISABLE && b.onDisable != nil {
		b.onDisable()
	} else if cmd == w32.BN_DOUBLECLICKED && b.onDoubleClick != nil {
		b.onDoubleClick()
	} else if cmd == w32.BN_HILITE && b.onHiLite != nil {
		b.onHiLite()
	} else if cmd == w32.BN_PAINT && b.onPaint != nil {
		b.onPaint()
	} else if cmd == w32.BN_UNHILITE && b.onUnHiLite != nil {
		b.onUnHiLite()
	}
}