/* w32/msimg32.go */

package w32

import (
	//"errors"
	//"fmt"
	
	//"log"
	"syscall"
	"unsafe"
)

var (
	msimg32 = syscall.NewLazyDLL("msimg32.dll")

	procAlphaBlend = msimg32.NewProc("AlphaBlend")
)

func AlphaBlend(
	dest HDC, destX, destY, destW, destH int,
	src HDC, srcX, srcY, srcW, srcH int,
	f BLENDFUNC,
) bool {
	ret, _, _ := procAlphaBlend.Call(
		uintptr(dest),
		uintptr(destX),
		uintptr(destY),
		uintptr(destW),
		uintptr(destH),
		uintptr(src),
		uintptr(srcX),
		uintptr(srcY),
		uintptr(srcW),
		uintptr(srcH),
		uintptr(*((*uintptr)(unsafe.Pointer(&f)))),
	)
	return ret != 0
}