
/* Win32Go/win32/icon.go */

package win32

import (
	"errors"
	"path/filepath"
	"log"
	
	"gitlab.com/win32go/win32/w32"
)

// Load icon from windows resource file packaged with application files
func GoIconFromResource(hInstance w32.HINSTANCE, iconName string) (*GoIcon, error){
	handle, load_err := w32.LoadIcon(hInstance, iconName)
	if load_err.Error() != "The operation completed successfully." {
		log.Println(iconName, "ERROR:", load_err)
		return nil, load_err
	} else if handle == 0 {
		log.Println(iconName, "ERROR: unable to create icon, please check your description")
		return nil, errors.New(iconName + " ERROR: Unable to create icon, please check your description")
	}

	return &GoIcon{path: iconName, hicon: w32.HICON(handle)}, nil
}

// Load icon from existing file packaged with application files
func GoIconFromImage(iconPath string) (*GoIcon, error) {
	absIconPath, path_err := filepath.Abs(iconPath)
	if path_err != nil {
		return nil, path_err
	}
	//log.Println("win32.GoIcon: iconPath: ", absIconPath)
	//p, err := syscall.UTF16PtrFromString(absIconPath)
	//if err != nil {
		//return nil, errors.New("win32.GoIcon: bad pointer to string iconPath")
	//}
	
	handle, load_err := w32.LoadImage(
		0,
		absIconPath,
		w32.IMAGE_ICON,
		0, 0,
		w32.LR_LOADFROMFILE|w32.LR_DEFAULTSIZE,
	)
	if load_err.Error() != "The operation completed successfully." {
		log.Println("win32.GoIcon: ERROR: ", load_err)
	}
	if handle == 0 {
		return nil, errors.New("win32.GoIcon: unable to create icon, please check your description")
	}

	return &GoIcon{path: iconPath, hicon: w32.HICON(handle)}, nil
}

type GoIcon struct {
	path   string
	hicon w32.HICON
}

//func (i *GoIcon) LadIconFromResource(goicon, icon_err = 