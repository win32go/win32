/* win32go/win32/window.go */

package win32

import (
	"log"
	//"strconv"
	"unsafe"
	"gitlab.com/win32go/win32/w32"
)

const IDM_DOIT uint32 = 1
const IDM_QUIT uint32 = 2

type GoWindowObj struct {
	goWidget		// embedded widget
	goObject 		// embedded object
	
	title         	string
	//icon          uintptr
	menu 			*GoMenu
	lastFocus		w32.HWND
	layout			*GoLayoutObj

	//scrollbarH 		*goScrollBar
	//scrollbarV 		*goScrollBar
}

func (w *GoWindowObj) AddControl(control GoObject) {
	log.Println("GoWindow::AddControl - ", control.objectType())
	w.layout.AddControl(control)
}

func (w *GoWindowObj) AddLayout(style GoLayoutStyle) (*GoLayoutObj) {
	log.Println("GoWindow::AddCLayout - ", style)
	layout := GoLayout(w.layout, style)
	return layout
}

func (w *GoWindowObj) AddMenuBar() (*GoMenu) {
	menu, err := GoMenuBar(w)
	if err != nil {
		log.Println("GoWindow::AddMenuBar - ", err)
		return nil
	}
	log.Println("GoWindow::AddMenuBar")
	w.menu = menu
	return w.menu
}


// Overide goWidget.Create() function for window
func (w *GoWindowObj) Create() (*goWidget, error) {
	var hWnd w32.HWND
	var err error
	
	hWnd, err = w32.CreateWindowExStr(
		w.goWidget.exStyle,
		w.goWidget.className,
		w.goWidget.windowName,
		w.goWidget.style,
		w.goWidget.x, w.goWidget.y, w.goWidget.width, w.goWidget.height,
		w.goWidget.hWndParent,
		w32.HMENU(0),
		w.goWidget.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("goWidget id = ", w.goWidget.id)
		log.Println("Failed to create widget.")
	} else {
		w.goWidget.hWnd = hWnd
		// Top Level Window does not have parent
		if w.goWidget.parent != nil {
			w.goWidget.parent.widgets[w.goWidget.id] = &(w.goWidget)
		}
		if cursor, err := GoCursor(w.goWidget.hWnd); err == nil {
			w.goWidget.cursor = cursor
		}
		w.goWidget.backcolor = goApp.ColorWindow 			// Background of pages, panes, popups, and windows.
		w.goWidget.backgroundcolor = goApp.ColorWindow 		// Background of pages, panes, popups, and windows.
		w.goWidget.buttonText = goApp.ColorButtonText 		// Foreground color for button text and ui.
		w.goWidget.facecolor = goApp.Color3DFace  			// Background color for button and ui.
		w.goWidget.forecolor = goApp.ColorWindowText 		// Headings, body copy, lists, placeholder text,
															// app and window borders, any UI that can't be interacted with.
		w.goWidget.grayText = goApp.ColorGrayText 			// Foreground color for disabled button text.
		w.goWidget.highlight = goApp.ColorHighlight 		// Background color for slected button or active ui.
		w.goWidget.highlightText = goApp.ColorHighlightText	// Foreground color for slected button text or active ui.
		w.goWidget.hotlight = goApp.ColorHotlight 			// Hyperlink color.
	}

	return &(w.goWidget), err
}

/*func (w *GoWindowObj) Exec() () {
	if w.parent != nil {
		w32.SetFocus(w.)
	}
}*/

/*func (w *GoWindowObj) DrawText(text string) {	
	rect := w32.GetClientRect(w.hWnd)
	hDC := w32.GetDC(w.hWnd)
	w32.DrawText(hDC, text, -1, rect, w32.DT_LEFT | w32.DT_WORDBREAK)
	w32.ReleaseDC(w.hWnd, hDC)
}*/

func (w *GoWindowObj) LayoutStyle() (style GoLayoutStyle) {
	return w.layout.Style()
}

func (w *GoWindowObj) Layout() (layout *GoLayoutObj) {
	return w.layout
}

func (w *GoWindowObj) LayoutMargin() (margin *GoMarginType) {
	return w.layout.margin
}

func (w *GoWindowObj) LayoutMarginSetLeft(left int) {
	w.layout.margin.Left = left
}

func (w *GoWindowObj) LayoutMarginSetTop(top int) {
	w.layout.margin.Top = top
}

func (w *GoWindowObj) LayoutMarginSetRight(right int) {
	w.layout.margin.Right = right
}

func (w *GoWindowObj) LayoutMarginSetBottom(bottom int) {
	w.layout.margin.Bottom = bottom
}

func (w *GoWindowObj) LayoutPaddingSetLeft(left int) {
	w.layout.padding.Left = left
}

func (w *GoWindowObj) LayoutPaddingSetTop(top int) {
	w.layout.padding.Top = top
}

func (w *GoWindowObj) LayoutPaddingSetRight(right int) {
	w.layout.padding.Right = right
}

func (w *GoWindowObj) LayoutPaddingSetBottom(bottom int) {
	w.layout.padding.Bottom = bottom
}

func (w *GoWindowObj) SetLayoutHeight(height int) {
	w.layout.height = height
	w.layout.setGeometry()
}

func (w *GoWindowObj) SetLayoutMargin(left int, top int, right int, bottom int) {
	w.layout.margin = &GoMarginType{left, top, right, bottom}
}

func (w *GoWindowObj) SetLayoutPadding(left int, top int, right int, bottom int) {
	w.layout.padding = &GoPaddingType{left, top, right, bottom}
}

func (w *GoWindowObj) SetLayoutSizePolicy(horiz GoSizeType, vert GoSizeType, fixed bool) {
	sizePolicy := GetSizePolicy(horiz, vert, fixed)
	w.layout.sizePolicy = sizePolicy
}

func (w *GoWindowObj) SetLayoutStyle(style GoLayoutStyle) {
	w.layout.SetStyle(style)
}

func (w *GoWindowObj) SetText(text string) {
	w.layout.text = text
	//w.text = text
	w32.InvalidateRect(w.layout.hWnd, nil, true)
	//w32.InvalidateRect(w.hWnd, nil, true)
}
// type RECT struct {Left, Top, Right, Bottom int32}

func (w *GoWindowObj) SetLayoutWidth(width int) {
	w.layout.width = width
	w.layout.setGeometry()
}

func (w *GoWindowObj) SizeToControl(control GoObject) {

	widget := control.wid()
	if w == nil {
		log.Println("Window does not exist!")
	}
	log.Println("WindowView", w)
	// RECT structures
	clirect := w.GetClientRect()
	log.Println("Window ClientRect - left:", clirect.Left(), "top:", clirect.Top(), "right:", clirect.Right(), "bottom:", clirect.Bottom())
	winrect := w32.GetWindowRect(w.hWnd)
	ctlrect := w32.GetWindowRect(widget.hWnd)
	winFrameWidth := int(winrect.Right - winrect.Left)
	winFrameHeight := int(winrect.Bottom - winrect.Top)
log.Println("Window Frame - width:", winFrameWidth, "height:", winFrameHeight)
	cliFrameWidth := clirect.Right() - clirect.Left()
	cliFrameHeight := clirect.Bottom() - clirect.Top()
log.Println("Window Client - width:", cliFrameWidth, "height:", cliFrameHeight)
	ctlFrameWidth := int(ctlrect.Right - ctlrect.Left)
	ctlFrameHeight := int(ctlrect.Bottom - ctlrect.Top)
log.Println("Control Frame - width:", ctlFrameWidth, "height:", ctlFrameHeight)
	w.x = int(winrect.Left)
	w.y = int(winrect.Top)
	w.width = winFrameWidth - cliFrameWidth + ctlFrameWidth
	w.height = winFrameHeight - cliFrameHeight + ctlFrameHeight + 20  // Margin 20
	log.Println("Window - x:", w.x, "y:", w.y, "width:", w.width, "height:", w.height)
	w32.MoveWindow(
		w.hWnd,
		w.x, w.y, w.width, w.height,
		true,
	)
	/*w32.SetWindowPos(
			w.hWnd, 0,
			w.x, w.y, w.width, w.height,
			w32.SWP_NOOWNERZORDER|w32.SWP_NOZORDER,
	)*/
}

func (w *GoWindowObj) SetTitle(title string) {
	w.title = title
	if w.hWnd != 0 {
		w32.SetWindowText(w.hWnd, title)
	}
}

func (w *GoWindowObj) TextOut(text string) {
	w.layout.text = w.layout.text + text
	//w.text = w.text + text
	w32.InvalidateRect(w.layout.hWnd, nil, true)
	//w32.InvalidateRect(w.hWnd, nil, true)
}

func (w *GoWindowObj) Title() string {
	return w.title
}

func (w *GoWindowObj) Update() {
	w32.UpdateWindow(w.layout.hWnd)
}

/*func (w *GoWindowObj) WndProc(hWnd w32.HWND, msg uint32, wParam uintptr, lParam uintptr) uintptr {
	mouseX := int(lParam & 0xFFFF)
	mouseY := int(lParam&0xFFFF0000) >> 16
	switch msg {
		case w32.WM_ACTIVATE:
			active := wParam != 0
			if active {
				if w.lastFocus != 0 {
					w32.SetFocus(w.lastFocus)
				}
			} else {
				w.lastFocus = w32.GetFocus()
			}
			return 0
		case w32.WM_CLOSE:
			if w.onCanClose != nil {
				if w.onCanClose() == false {
					return 0
				}
			}
			if w.Parent() != nil {
				w32.EnableWindow(w.hWndParent, true)
				w32.SetForegroundWindow(w.hWndParent)
			}
			if w.onClose != nil {
				w.onClose()
			}
			return 0
		
		case w32.WM_COMMAND:
			w.onWM_COMMAND(wParam, lParam)
		case w32.WM_DESTROY:
			w32.PostQuitMessage(0)
			return 0	
			//switch uint32(wparam) {
			//	case IDM_DOIT:
			//		hDC = w32.GetDC(hWnd)
			//		w32.TextOut(hDC, 10, 20, "Ok, I did it!")
			//		w32.ReleaseDC(hWnd, hDC)
			//	case IDM_QUIT:
			//		w32.DestroyWindow(hWnd)
			//}
		case w32.WM_KEYDOWN:
			if w.onKeyDown != nil {
				w.onKeyDown(int(wParam))
				return 0
			}
		case w32.WM_KEYUP:
			if w.onKeyUp != nil {
				w.onKeyUp(int(wParam))
				return 0
			}
		case w32.WM_LBUTTONDOWN, w32.WM_MBUTTONDOWN, w32.WM_RBUTTONDOWN:
			if w.onMouseDown != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONDOWN {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONDOWN {
					b = MouseButtonRight
				}
				w.onMouseDown(b, mouseX, mouseY)
			}
			return 0
		case w32.WM_LBUTTONUP, w32.WM_MBUTTONUP, w32.WM_RBUTTONUP:
			if w.onMouseUp != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONUP {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONUP {
					b = MouseButtonRight
				}
				w.onMouseUp(b, mouseX, mouseY)
			}
			return 0
		case w32.WM_MOUSEMOVE:
			if w.onMouseMove != nil {
				w.onMouseMove(mouseX, mouseY)
				return 0
			}
		}
		ret := w32.DefWindowProc(hWnd, msg, wParam, lParam)
		return ret
	}

	func (w *GoWindowObj) handleNotification(cmd uintptr) {
	log.Println("GoMainWindow::handleNotification..............")
}*/

func (w *GoWindowObj) onWM_COMMAND( wParam uintptr, lParam uintptr) {
	log.Println("onWM_COMMAND..............")
	//log.Println("wParam....ControlID....", wParam)
	wParamH := (wParam & 0xFFFF0000) >> 16
	wParamL := wParam & 0xFFFF
	lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF
	log.Println("wParamL....ControlID....", wParamL)
	log.Println("wParamH....Notification....", wParamH)
	log.Println("lParamL....WindowHandle....", lParamL)
	log.Println("lParamH....", lParamH)
	if lParam != 0 {
		// control clicked
		index := wParamL	// controlID
		cmd := wParamH		// BN_CLICKED
		log.Println("Window::cmd....", cmd)
		/*if cmd == w32.STN_CLICKED {
			log.Println("Window::STN_CLICKED.....")
		}*/
		if cmd == w32.BN_HILITE {
			log.Println("Window::BN_HILITE........")
			goApp.Controls(index).handleNotification(cmd)
		} else if cmd == w32.BN_UNHILITE {
			log.Println("Window:BN_UNHILITE........")
			goApp.Controls(index).handleNotification(cmd)
		} else if cmd == w32.BN_CLICKED {
			log.Println("Window::BN_CLICKED..............")

			// get widget from GoApp.winStack using controlID and trigger function widget.onClick
			// which in turn triggers function button.onClick()
			goApp.Controls(index).handleNotification(cmd)
		}
		/*if index < uintptr(len(w.controls)) {
			w.controls[index].handleNotification(cmd)
		}*/
	} else {
		if wParamH == 0 {
			// low word of w contains menu ID
			id := int(wParamL)
			if goApp.Actions(id).OnActivate() != nil {
				goApp.Actions(id).onActivate()
			}

			/*switch id {
				case IDM_DOIT:
					hDC := w32.GetDC(mw.hWnd)
					w32.TextOut(hDC, 10, 20, "Ok, I did it!")
					w32.ReleaseDC(mw.hWnd, hDC)
				case IDM_QUIT:
					w32.DestroyWindow(w.hWnd)
			}*/
			/*if 0 <= id && id < len(w.menuStrings) {
				f := w.menuStrings[id].onClick
				if f != nil {
					f()
				}
			}*/ 
		} else if wParamH == 1 {
				// low word of w contains accelerator ID
			//index := int(wParamL)
			/*if f := w.shortcuts[index].f; f != nil {
				f()
			}*/
		}

	}
}

func (w *GoWindowObj) onWM_DRAWITEM(wParam uintptr, lParam uintptr) {
	log.Println("GoWindowObj::onWM_DRAWITEM..............")
	//log.Println("wParam....ControlID....", wParam)
	//wParamH := (wParam & 0xFFFF0000) >> 16
	wParamL := wParam & 0xFFFF
	//lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	//lParamL := lParam & 0xFFFFFFFF
	//log.Println("wParamL....ControlID....", wParamL)
	//log.Println("wParamH....Notification....", wParamH)
	//log.Println("lParamL....WindowHandle....", lParamL)
	//log.Println("lParamH....", lParamH)
	index := wParamL	// controlID
	//index := wParam
	drawItemStruct := ((*w32.DRAWITEMSTRUCT)(unsafe.Pointer(lParam)))
	goApp.Controls(index).drawItem(drawItemStruct)



	/*if 0 <= index && index < uintptr(len(w.controls)) {
		if p, ok := w.controls[index].(*PaintBox); ok {
			if p.onPaint != nil {
				drawItem := ((*w32.DRAWITEMSTRUCT)(unsafe.Pointer(lParam)))
				// create a back buffer
				p.backBuffer.setMinSize(drawItem.HDC, p.width, p.height)
				bmpOld := w32.SelectObject(
					p.backBuffer.dc,
					w32.HGDIOBJ(p.backBuffer.bmp),
				)
				c := &Canvas{
					hdc:    p.backBuffer.dc,
					width:  p.width,
					height: p.height,
				}
				if p.parent != nil {
					c.SetFont(p.parent.Font())
				}
				c.ClearDrawRegions()
				p.onPaint(c)

				// blit the backbuffer to the front
				w32.BitBlt(
					drawItem.HDC, 0, 0, p.width, p.height,
					p.backBuffer.dc, 0, 0, w32.SRCCOPY,
				)
				w32.SelectObject(p.backBuffer.dc, bmpOld)
			}
		}
	}*/







	/*var drawItem w32.DRAWITEMSTRUCT
	var state int

	lpDrawItem = (LPDRAWITEMSTRUCT) lParam;
	SetTextAlign(lpDrawItem->hDC, TA_CENTER | TA_BOTTOM);
	hBrush = CreateSolidBrush(RGB(255, 0, 0));
	SelectObject(lpDrawItem->hDC, hBrush);
	SetPolyFillMode(lpDrawItem->hDC, ALTERNATE);
	SetBkColor(lpDrawItem->hDC, RGB(255, 0, 0));

	if (lpDrawItem->CtlType != ODT_BUTTON)
		return FALSE;

	state = lpDrawItem->itemState;

	if(state & ODS_SELECTED)
	{
		Ellipse(lpDrawItem->hDC, 0, 0, 20, 20);
		TextOut(lpDrawItem->hDC, 10, 18, "X", 1);
	}
	else 
	{
		Ellipse(lpDrawItem->hDC, 0, 0, 20, 20);
		TextOut(lpDrawItem->hDC, 10, 18, "5", 1);
	}

	DeleteObject(hBrush);
	return true;*/

}

func (w *GoWindowObj) onWM_NOTIFY(wParam uintptr, lParam uintptr) {
}

func (w *GoWindowObj) destroy() () {
	for _, ctl := range w.controls {
		ctl = nil 				// delete control structure
		w.removeControl(ctl) 	// remove from Control Stack
	}
}

func (w *GoWindowObj) groupId() (id int) {
	return 0
}

func (w *GoWindowObj) isDialog() (bool) {
	return false
}

func (w *GoWindowObj) isLayout() (bool) {
	return false
}

func (w *GoWindowObj) objectType() (string) {
	return "GoWindowObj"
}

func (w *GoWindowObj) drawItem(drawItemStruct *w32.DRAWITEMSTRUCT) {
	//CtlType    uint32
	//CtlID      uint32
	//ItemID     uint32
	//ItemAction uint32
	//ItemState  uint32
	//HwndItem   HWND
	//HDC        HDC
	//RcItem     RECT
	//ItemData   uintptr
}

func (w *GoWindowObj) wid() (*goWidget) {
	return &w.goWidget
}