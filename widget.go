/* widget.go */
// The goWidget structure is a base object for all win32 user interfrace objects.
// The goWidget implements all the default window properties and methods, including geometry, mode, and event handlers.
// A goWidget without a parent goWidget is always the goWidget of a top-level window.

package win32

import (
	"log"
	"reflect"
	"syscall"
	"unsafe"
	"unicode/utf8"
	"gitlab.com/win32go/win32/w32"
)

type GoSizeType int

const(
	ST_FixedWidth GoSizeType		= 0x0000
	ST_FixedHeight GoSizeType		= 0x0001
	ST_MinimumWidth GoSizeType		= 0x0002
	ST_MinimuHeight GoSizeType		= 0x0004
	ST_MaximumWidth GoSizeType		= 0x0008
	ST_MaximumHeight GoSizeType		= 0x0010
	ST_PreferredWidth GoSizeType	= 0x0020
	ST_PreferredHeight GoSizeType	= 0x0040
	ST_ExpandingWidth GoSizeType	= 0x0080
	ST_ExpandingHeight GoSizeType 	= 0x0100
)

func GetSizePolicy(horiz GoSizeType, vert GoSizeType, fixed bool) (*GoSizePolicy) {
	return &GoSizePolicy{horiz, vert, fixed}
}

type GoSizePolicy struct {
	Horiz 	GoSizeType
	Vert 	GoSizeType
	Fixed 	bool
}

type GoBorderStyle int

const (
	BorderNone GoBorderStyle = iota
	BorderSingleLine
	BorderSunken
	BorderSunkenThick
	BorderRaised
)

func borderStyleEx(b GoBorderStyle) uint {
	if b == BorderSunken {
		return w32.WS_EX_STATICEDGE
	}
	if b == BorderSunkenThick {
		return w32.WS_EX_CLIENTEDGE
	}
	return 0
}

func borderStyle(b GoBorderStyle) uint {
	if b == BorderSingleLine {
		return w32.WS_BORDER
	}
	if b == BorderRaised {
		return w32.WS_DLGFRAME
	}
	return 0
}

func borderSize(b GoBorderStyle) (int) {
	switch b {
		case BorderNone:
			return 0
		case BorderSingleLine:
			return 1
		case BorderSunken:
			return 2
		case BorderSunkenThick:
			return 4
		case BorderRaised:
			return 4
	}
	return 0
}

type goWidget struct {
	className 	string
	windowName 	string
	style 		uint32
	exStyle     uint32
	state       int32
	x       	int
	y       	int
	width   	int
	height  	int
	minWidth 	int
	minHeight 	int
	maxWidth 	int
	maxHeight 	int
	hWndParent 	w32.HWND
	id 			int
	instance 	w32.HINSTANCE
	param		uint32
	hWnd 		w32.HWND
	parent  	*goWidget
	disabled 	bool
	visible   	bool
	autoSize	bool 				// control sizes to contain text
	border 		GoBorderStyle
	cursor      *goCursor 			// cursor to be displayed over widget
	cursorPos 	*goCursorMetrics 	// cursor position
	font        *GoFont 			// font used for widget content
	margin 		*GoMarginType 			// clear margin surrounding widget
	padding  	*GoPaddingType			// padding surrounding widget content
	sizePolicy	*GoSizePolicy 		// widget sizing policy - GoSizePolicy{horiz, vert, fixed}
	spacing 	int
	text 	 	string 				// text dispalyed in widget
	window      bool 				// window = true, control = false
	widgets     map[int]*goWidget

	layout 		*GoLayoutObj
	lastHScrollValue 	int
	lastVScrollValue 	int
	mousehover 	bool 				// mouse is hovering over widget
	mousedown 	bool

	alpha 			uint8
	backMode 		GoBkMode
	backgroundMode 	GoBkMode
	backgroundImage *GoImageObj
	// windows 10 System Colors
	backcolor 		GoColor 	// COLOR_WINDOW
	backgroundcolor GoColor 	// COLOR_WINDOW
	buttonText 		GoColor 	// COLOR_BTNTEXT Foreground color for button text.
	facecolor		GoColor  	// COLOR_BTNFACE Background color for button.
	forecolor 		GoColor 	// COLOR_WINDOWTEXT
	grayText 		GoColor 	// COLOR_GRAYTEXT Foreground color for disabled button text.
	highlight 		GoColor 	// COLOR_HIGHLIGHT Background color for slected button.
	highlightText 	GoColor 	// COLOR_HIGHLIGHTTEXT Foreground color for slected button text.
	hotlight 		GoColor 	// COLOR_HOTLIGHT Hyperlink color.

	onShow        func()
	onClose       func()
	onCanClose    func() bool
	onMouseMove   func(x, y int)
	onMouseWheel  func(x, y int, delta float64)
	onMouseDown   func(button GoButtonState, x, y int)
	onMouseUp     func(button GoButtonState, x, y int)
	onKeyDown     func(key int)
	onKeyUp       func(key int)
	onResize      func(clientRect *GoRectType)
	onPaint 	  func(*GoPainter)
	onPaintBackground	func(*GoPainter)
}

/***********************************************************************************/
// Public functions
/***********************************************************************************/

func (w *goWidget) BackgroundMode() (mode GoBkMode) {
	return w.backgroundMode
}

func (w *goWidget) BackMode() (mode GoBkMode) {
	return w.backMode
}

func (w *goWidget) Close() {
	if w.hWnd != 0 {
		w32.SendMessage(w.hWnd, w32.WM_CLOSE, 0, 0)
	}
}

func (w *goWidget) Create() (*goWidget, error) {
	var menu w32.HMENU
	var hWnd w32.HWND
	var err error
	
	// if widget has no parent then it is top level window
	// and has to have HMenu declared or nil (0)
	//if w.parent == nil {
		//menu = w32.HMENU(0)
	//} else {
		menu = w32.HMENU(w.id)
	//}
	hWnd, err = w32.CreateWindowExStr(
		w.exStyle,
		w.className,
		w.windowName,
		w.style,
		w.x, w.y, w.width, w.height,
		w.hWndParent,
		menu,
		w.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("widget id = ", w.id)
		log.Println("Failed to create widget.")
	} else {
		w.hWnd = hWnd
		// Top Level Window does not have parent
		if w.parent != nil {
			w.parent.widgets[w.id] = w
		}
		if cursor, err := GoCursor(w.hWnd); err == nil {
			w.cursor = cursor
		}
		// Create Widget Palette
		w.backcolor = goApp.ColorWindow				// Background used for painting text, patterns, etc.
		w.backgroundcolor = goApp.ColorWindow 		// Background of pages, panes, popups, and windows.
		w.buttonText = goApp.ColorButtonText 		// Foreground color for button text and ui.
		w.facecolor = goApp.Color3DFace  			// Background color for button and ui.
		w.forecolor = goApp.ColorWindowText 		// Headings, body copy, lists, placeholder text,
													// app and window borders, any UI that can't be interacted with.
		w.grayText = goApp.ColorGrayText 			// Foreground color for disabled button text.
		w.highlight = goApp.ColorHighlight 			// Background color for slected button or active ui.
		w.highlightText = goApp.ColorHighlightText 	// Foreground color for slected button text or active ui.
		w.hotlight = goApp.ColorHotlight 			// Hyperlink color.
	}
	return w, err
}

func (w *goWidget) CreateText() (*goWidget, error) {
	var menu w32.HMENU
	var hWnd w32.HWND
	var err error
	
	// if widget has no parent then it is top level window
	// and has to have HMenu declared or nil (0)
	//if w.parent == nil {
		menu = w32.HMENU(w.id)
	//} else {
		//menu = w32.HMENU(w.id)
	//}
	hWnd, err = w32.CreateWindowExStr(
		w.exStyle,
		w.className,
		w.windowName,
		w.style,
		w.x, w.y, w.width, w.height,
		w.hWndParent,
		menu,
		w.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("widget id = ", w.id)
		log.Println("Failed to create widget.")
	} else {
		w.hWnd = hWnd
		// Top Level Window does not have parent
		if w.parent != nil {
			w.parent.widgets[w.id] = w
		}
	}
	return w, err
}

// CursorPosition returns the current cursor position, respectively the current
// selection.
//
// If no selection is active, the returned start and end values are the same.
// They then indicate the index of the UTF-8 character in this EditLine's Text()
// before which the caret is currently set.
//
// If a selection is active, start is the index of the first selected UTF-8
// character in Text() and end is the position one character after the end of
// the selection. The selected text is thus
//
//     w.Text()[start:end]

func (w *goWidget) CursorPosition() (start int, end int) {
	if w.hWnd != 0 {
		var start, end uint32
		w32.SendMessage(
			w.hWnd,
			w32.EM_GETSEL,
			uintptr(unsafe.Pointer(&start)),
			uintptr(unsafe.Pointer(&end)),
		)
		w.cursorPos.Start, w.cursorPos.End = int(start), int(end)
	}
	return w.cursorPos.Start, w.cursorPos.End
}

func (w *goWidget) ExtendedStyle() uint32 {
	return w.exStyle
}

func (w *goWidget) Focus() {
	// TODO Allow this before showing a window.
	if w.hWnd != 0 {
		w32.SetFocus(w.hWnd)
	}
}

func (w *goWidget) Font() *GoFont {
	return w.font
}

func (w *goWidget) fontHandle() w32.HFONT {
	if w.font != nil {
		return w.font.handle
	}
	if w.parent != nil {
		font := w.parent.Font()
		if font != nil {
			return font.handle
		}
	}
	return 0
}

func (w *goWidget) GetClientRect() (*GoRectType) {
	clirect := w32.GetClientRect(w.hWnd)
	goRect := GoRect().FromW32Rect(clirect)
	//goRect.SetW32RECT(clirect)
	//goRect.Deflate(w.margin)
	return goRect
}

func (w *goWidget) GetDC() (hDC w32.HDC) {
	hDC = w32.GetDC(w.hWnd)
	return hDC
}

func (w *goWidget) GetWindowRect() (*GoRectType) {
	clirect := w32.GetWindowRect(w.hWnd)
	goRect := GoRect().FromW32Rect(clirect)
	//goRect.SetW32RECT(clirect)
	//goRect.Deflate(w.margin)
	return goRect
}

func (w *goWidget) HasFocus() bool {
	return w.hWnd != 0 && w32.GetFocus() == w.hWnd
}

func (w *goWidget) Height() (height int) {
	return w.height
}

func (w *goWidget) Hide() {
	if w32.ShowWindow(w.hWnd, w32.SW_HIDE) == false {
		w.state = w32.SW_HIDE
		w.visible = false
	}
}

func (w *goWidget) IsVisible() (bool) {
	w.visible = w32.IsWindowVisible(w.hWnd)
	return w.visible
}

func (w *goWidget) IsWindow() (window bool) {
	window = w32.IsWindow(w.hWnd)
	return window
} 

func (w *goWidget) OnResize() func(clientRect *GoRectType) {
	return w.onResize
}
// func (*goWidget) MarginSetLeft(left int)
// Sets the left margin for the spacing of this goWidget ui object within its parent.
// see the goWidget geometry property.
func (w *goWidget) MarginSetLeft(left int) {
	w.margin.Left = left
}

// func (*goWidget) MarginSetTop(top int)
// Sets the top margin for the spacing of this goWidget ui object within its parent.
// see the goWidget geometry property.
func (w *goWidget) MarginSetTop(top int) {
	w.margin.Top = top
}

// func (*goWidget) MarginSetRight(right int)
// Sets the right margin for the spacing of this goWidget ui object within its parent.
// see the goWidget geometry property.
func (w *goWidget) MarginSetRight(right int) {
	w.margin.Right = right
}

// func (*goWidget) MarginSetBottom(bottom int)
// Sets the bottom margin for the spacing of this goWidget ui object within its parent.
// see the goWidget geometry property.
func (w *goWidget) MarginSetBottom(bottom int) {
	w.margin.Bottom = bottom
}

func (w *goWidget) Moved(x int, y int) {
	w.x = x; w.y = y

}

//func (*goWidget) PaddingSetLeft(left int)
// Sets the top padding for the spacing of child goWidget ui objects.
// see the goWidget geometry property.
func (w *goWidget) PaddingSetLeft(left int) {
	w.padding.Left = left
}

// func (*goWidget) PaddingSetTop(top int)
// Sets the top padding for the spacing of child goWidget ui objects.
// see the goWidget geometry property.
func (w *goWidget) PaddingSetTop(top int) {
	w.padding.Top = top
}

// func (*goWidget) PaddingSetRight(right int)
// Sets the right padding for the spacing of child goWidget ui objects.
// see the goWidget geometry property.
func (w *goWidget) PaddingSetRight(right int) {
	w.padding.Right = right
}

// func (*goWidget) PaddingSetBottom(bottom int)
// Sets the bottom padding for the spacing of child goWidget ui objects.
// see the goWidget geometry property.
func (w *goWidget) PaddingSetBottom(bottom int) {
	w.padding.Bottom = bottom
}

// func (*goWidget) paint(hDC w32.HDC, rect *GoRectType)
func (w *goWidget) paint(hDC w32.HDC, rect *GoRectType) {
	//log.Println("w.onPaint =", w.onPaint)
	rect.Translate(w.lastHScrollValue, w.lastVScrollValue)
	log.Println("TranslatedRect:", rect.left, rect.top, rect.right, rect.bottom)
	// Paint Background........................................
	pt := CreatePainter(hDC, rect)
	/*if w.focusRect {
	    w32.DrawFocusRect(hdc, rect.W32RECT())
	}*/
	log.Println("w.onPaint =", w.onPaint)
	// If User Paint Function is set
	if w.onPaint != nil {
		log.Println("w.onPaint != nil")
		//gopainter := CreatePainter(hDC, gorect)
		//gopainter := CreatePainter(hDC, rect)
		//gopainter.SaveState()
		// Paint User Paint Function...........................
		//pt.RestoreState()
		w.onPaint(pt)
	}
	pt.Destroy()
	//log.Println("DrawText............")
	//w32.DrawText(hDC, w.text, -1, rect, w32.DT_LEFT | w32.DT_WORDBREAK)
}

// func (*goWidget) paintBackground(hDC w32.HDC, rect *GoRectType) 
func (w *goWidget) paintBackground(hDC w32.HDC, rect *GoRectType) {
	log.Println("paintBackground hDC -", hDC, " rect(", rect.Width(), rect.Height(), " )")
	//rect.Translate(w.lastHScrollValue, w.lastVScrollValue)
	pt := CreatePainter(hDC, rect)

	// If User PaintBackground Function is set
	if w.onPaintBackground != nil {
		w.onPaintBackground(pt)
	} else {
		if w.backgroundMode != TransparentMode {
			log.Println("OPAQUEMODE")
			pt.SetStockBrush(w.backgroundcolor)
			pt.SetNullPen()
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
		}
	}
	pt.Destroy()
}


// func (*goWidget) Parent() (parent *goWidget)
// Returns the parent of this goWidget ui object, or nil if it does not have any parent goWidget ui object.
func (w *goWidget) Parent() (parent *goWidget) {
	return w.parent
}

func (w *goWidget) parentFontChanged() {
	w.SetFont(w.font)
}

// func (*goWidget) Position() (x int, y int)
// Returns the position of the goWidget ui object within its parent goWidget ui object.
func (w *goWidget) Position() (x int, y int) {
	return w.x, w.y
}

// func (*goWidget) Refresh()
// Invalidates the goWidget ui object's client area and sets it for a repaint.
func (w *goWidget) Refresh() {
	log.Println("Widget::Refresh()......................")
	rect := w32.GetClientRect(w.hWnd)
	w32.InvalidateRect(w.hWnd, rect, false)
}

func (w *goWidget) Resize() {
	w.setGeometry()
}

func (w *goWidget) ReleaseDC(hDC w32.HDC) {
	if hDC != 0 {
		w32.ReleaseDC(w.hWnd, hDC)
	}
}

func (w *goWidget) SelectAll() {
	w.setCursor(0, -1)
}

// func (*goWidget) SetBackgroundMode(mode GoBkMode)
// Sets the background mode used for painting the background of the widget. See the "backgroundMode" property for details.
// The BackgroundMode can be set to either TransparentMode or OpaqueMode.
func (w *goWidget) SetBackgroundMode(mode GoBkMode) {
	w.backgroundMode = mode
}

// func (*goWidget) SetBackgroundColor(color GoColor)
// func (*goWidget) SetBackgroundColor(color uint32)
// Sets the background color of the goWidget ui object to color.
// This is the color used to erase the goWidget client region following a windows WM_ERASEBACKGROUND event.
func (w *goWidget) SetBackgroundColor(color interface{}) { //color *goColor) {
	//log.Println("goWidget.SetBackgroundColor() - type =", reflect.ValueOf(color).Kind().String())
	switch c := reflect.ValueOf(color); c.Kind() {
	
	case reflect.Uint32:
		log.Println("goWidget.SetBackgroundColor() - GoColor")
		w.backgroundcolor = color.(GoColor)
		w.SetBackgroundMode(OpaqueMode)
		w.SetBackColor(color)
		w.Refresh()
	default:
		log.Println("goWidget.SetBackgroundColor() - Not GoColor or GoColor")
	}
	
}

func (w *goWidget) SetBackgroundImage(image *GoImageObj) {
	w.backgroundImage = image
	w.SetBackgroundMode(TransparentMode)
}

// func (*goWidget) SetBackgroundColor(color GoColor)
// func (*goWidget) SetBackgroundColor(color uint32)
// Sets the back infill color of the goWidget ui object to color.
// This is the infill color used for painting text and patterns to the foreground of the widget.
func (w *goWidget) SetBackColor(color interface{}) { //color *goColor) {
	//log.Println("goWidget.SetBackgroundColor() - type =", reflect.ValueOf(color).Kind().String())
	switch c := reflect.ValueOf(color); c.Kind() {
	
	case reflect.Uint32:
		log.Println("goWidget.SetBackColor() - GoColor")
		w.backcolor = color.(GoColor)
		w.SetBackMode(OpaqueMode)
	default:
		log.Println("goWidget.SetBackColor() - Not GoColor or GoColor")
	}
	
}

// func (*goWidget) SetBackMode(mode GoBkMode)
// Sets the background mode used for painting text and patterns to the foreground of the widget. See the "backMode" property for details.
// The BackMode can be set to either TransparentMode or OpaqueMode.
func (w *goWidget) SetBackMode(mode GoBkMode) {
	w.backMode = mode
}

func (w *goWidget) SetBorderStyle(style GoBorderStyle) {
	w.border = style
	w.setBorder(style)
	w.setGeometry()
}

func (w *goWidget) SetCursorPos(pos int) {
	w.setCursor(pos, pos)
}

func (w *goWidget) SetExtendedStyle(x uint32) {
	w.exStyle = x
	if w.hWnd != 0 {
		w32.SetWindowLongPtr(w.hWnd, w32.GWL_EXSTYLE, uintptr(w.exStyle))
		w32.ShowWindow(w.hWnd, w.state) // for the new style to take effect
		w.exStyle = uint32(w32.GetWindowLongPtr(w.hWnd, w32.GWL_EXSTYLE))
		//w.readBounds()
	}
}

func (w *goWidget) SetFont(font *GoFont) {
	w.font = font
	if w.hWnd != 0 {
		w32.SendMessage(w.hWnd, w32.WM_SETFONT, uintptr(w.fontHandle()), 1)
	}
}

func (w *goWidget) SetHeight(height int) {
	if w.autoSize == false {
		w.height = height
		w.setGeometry()
	}
}

//func (w *goWidget) SetLeftMargin(left int) {
//	w.margin.Left = left
//}

func (w *goWidget) SetMargin(left int, top int, right int, bottom int) {
	w.margin = &GoMarginType{left, top, right, bottom}
}

func (w *goWidget) SetPadding(left int, top int, right int, bottom int) {
	w.padding = &GoPaddingType{left, top, right, bottom}
	w.setGeometry()
}

func (w *goWidget) SetParent(parent *goWidget) {
	w.parent = parent
}

func (w *goWidget) SetPosition(x int, y int) {
	w.x = x; w.y = y
	w.setGeometry()
}

func (w *goWidget) SetSelection(start int, end int) {
	w.setCursor(start, end)
}

func (w *goWidget) SetSize(width int, height int) {
	if w.autoSize == false {
		w.width = width; w.height = height
		w.setGeometry()
	}
}

func (w *goWidget) SetSizePolicy(horiz GoSizeType, vert GoSizeType, fixed bool) {
	sizePolicy := &GoSizePolicy{horiz, vert, fixed}
	w.sizePolicy = sizePolicy
}

/*func (w *goWidget) SetState(s uint) {
	w.state = s
	if w.handle != 0 {
		w32.ShowWindow(w.handle, w.state)
	}
}*/

func (w *goWidget) SetStyle(ws uint32) {
	w.style = ws
	if w.hWnd != 0 {
		w32.SetWindowLongPtr(w.hWnd, w32.GWL_STYLE, uintptr(w.style))
		w32.ShowWindow(w.hWnd, w.state) // for the new style to take effect
		w.style = uint32(w32.GetWindowLongPtr(w.hWnd, w32.GWL_STYLE))
		//w.readBounds()
	}
}

func (w *goWidget) SetTextColor(color interface{}) {
	switch c := reflect.ValueOf(color); c.Kind() {
	
	case reflect.Uint32:
		log.Println("goWidget.SetTextColor() - GoColor")
		w.forecolor = color.(GoColor)
	default:
		log.Println("goWidget.SetTextColor() - Not GoColor or GoColor")
	}
}

func (w *goWidget) SetVisible(visible bool) () {
	if visible == true {
		w.Show()
	} else {
		w.Hide()
	}	
}


// func (*goWidget) SetWidth(width int)
// Sets the goWidget ui obhject width.
// This function will be ignored if the goWidget sizePolicy is set to ui.ST_ExpandingWidth.
func (w *goWidget) SetWidth(width int) {
	if w.autoSize == false {
		w.width = width
		w.setGeometry()
	}
}

func (w *goWidget) SetX(x int) {
	w.x = x
	w.setGeometry()
}

func (w *goWidget) SetY(y int) {
	w.y = y
	w.setGeometry()
}

// func (*goWidget) Show()
// Shows thw goWidget ui object and its children
func (w *goWidget) Show() {
	if w32.ShowWindow(w.hWnd, w32.SW_SHOW) == true {
		w.state = w32.SW_SHOW
		w.visible = true
	}
}

// func (*goWidget) ShowNormal()
// Restores the goWidget ui object after it has been maximized or minimized.
// Calling this function only affects the top level GoWindowObj.
// See also ShowMinimized(), ShowMaximized(), Show(), Hide(), and visible.
func (w *goWidget) ShowNormal() {
	if w32.ShowWindow(w.hWnd, w32.SW_SHOWNORMAL) == true {
		w.state = w32.SW_NORMAL
		w.visible = true
	}
}

// func (*goWidget) ShowMaximized()
// Shows the goWidget ui object maximised.
// Calling this function only affects the top level GoWindowObj.
// See also ShoewNormal(), ShowMinimized(), Show(), Hide(), and visible.
func (w *goWidget) ShowMaximized() {
	if w32.ShowWindow(w.hWnd, w32.SW_MAXIMIZE) == true {
		w.state = w32.SW_MAXIMIZE
		w.visible = true
	}
}

// func (*goWidget) ShowMinimized()
// Shows the goWidget ui object minimised, as an icon.
// Calling this function only affects the top level GoWindowObj.
// See also ShowNormal(), ShowMaximized(), Show(), Hide(), and visible.
func (w *goWidget) ShowMinimized() {
	if w32.ShowWindow(w.hWnd, w32.SW_MINIMIZE) == true {
		w.state = w32.SW_MINIMIZE
		w.visible = true
	}
}

// func (*goWidget) Size() (width int, height int)
// Returns the size of the goWidget ui object excluding any window frame.
func (w *goWidget) Size() (width int, height int) {
	return w.width, w.height
}

// func (*goWidget) SizePolicy() (sizePolicy *GoSizePolicy)
// Returns the layout behavior of the goWidget ui object. See the sizePolicy property for details.
func (w *goWidget) SizePolicy() (sizePolicy *GoSizePolicy) {
	return w.sizePolicy
}

/*func (w *goWidget) State() {
	var p w32.WINDOWPLACEMENT
	if w32.GetWindowPlacement(w.handle, &p) {
		w.state = int(p.ShowCmd)
	}
}*/

// func (*goWidget) Style() (style uint32)
// Returns the GUI style for this goWidget ui object.
func (w *goWidget) Style() (style uint32) {
	return w.style
}

func (w *goWidget) Update() {
	w32.UpdateWindow(w.hWnd)
}

func (w *goWidget) X() (x int) {
	return w.x
}

func (w *goWidget) Y() (y int) {
	return w.y
}

func (w *goWidget) Width() (width int) {
	return w.width
}


/***********************************************************************************/
// Public Actions
/***********************************************************************************/

// SetOnCanClose is passed a function that is called when the window is about to
// be closed, e.g. when the user hits Alt+F4. If f returns true the window is
// closed, if f returns false, the window stays open.
func (w *goWidget) SetOnCanClose(f func() bool) {
	w.onCanClose = f
}

func (w *goWidget) SetOnClose(f func()) {
	w.onClose = f
}

func (w *goWidget) SetOnKeyDown(f func(key int)) {
	w.onKeyDown = f
}

func (w *goWidget) SetOnKeyUp(f func(key int)) {
	w.onKeyUp = f
}

func (w *goWidget) SetOnMouseDown(f func(button GoButtonState, x, y int)) {
	w.onMouseDown = f
}

func (w *goWidget) SetOnMouseMove(f func(x, y int)) {
	w.onMouseMove = f
}

func (w *goWidget) SetOnMouseWheel(f func(x, y int, delta float64)) {
	w.onMouseWheel = f
}

func (w *goWidget) SetOnMouseUp(f func(button GoButtonState, x, y int)) {
	w.onMouseUp = f
}

func (w *goWidget) SetOnPaint(f func(p *GoPainter)) {
	w.onPaint = f
}

func (w *goWidget) SetOnResize(f func(clientRect *GoRectType)) {
	w.onResize = f
}

func (w *goWidget) SetOnShow(f func()) {
	w.onShow = f
}


/***********************************************************************************/
// Private functions
/***********************************************************************************/

func (w *goWidget) clampCursorToText() {
	// If called before we have a window, we have to handle clamping of the
	// positions ourselves.
	n := utf8.RuneCountInString(w.text)

	if w.cursorPos.End == -1 {
		w.cursorPos.End = n
	}

	if w.cursorPos.Start < 0 {
		w.cursorPos.Start = 0
	}
	if w.cursorPos.Start > n {
		w.cursorPos.Start = n
	}

	if w.cursorPos.End < 0 {
		w.cursorPos.End = 0
	}
	if w.cursorPos.End > n {
		w.cursorPos.End = n
	}

	if w.cursorPos.End < w.cursorPos.Start {
		w.cursorPos.Start, w.cursorPos.End = w.cursorPos.End, w.cursorPos.Start
	}
}

func (w *goWidget) extents() (rect *GoRectType) {
	return GoRect().FromInt(w.x, w.y, w.x + w.width, w.y + w.height)
}

func (w *goWidget) setCursor(start int, end int) {
	w.cursorPos.Start = start
	w.cursorPos.End = end

	if w.hWnd != 0 {
		w32.SendMessage(
			w.hWnd,
			w32.EM_SETSEL,
			uintptr(uint32(w.cursorPos.Start)),
			uintptr(uint32(w.cursorPos.End)),
		)
		w32.SendMessage(w.hWnd, w32.EM_SCROLLCARET, 0, 0)
	} else {
		w.clampCursorToText()
	}
}

func (w *goWidget) setBorder(bs GoBorderStyle) {
	if w.hWnd != 0 {
		style := uint(w32.GetWindowLongPtr(w.hWnd, w32.GWL_STYLE))
		style = style &^ w32.WS_BORDER &^ w32.WS_DLGFRAME
		style |= borderStyle(bs)
		w32.SetWindowLongPtr(w.hWnd, w32.GWL_STYLE, uintptr(style))

		exStyle := uint(w32.GetWindowLongPtr(w.hWnd, w32.GWL_EXSTYLE))
		exStyle = exStyle &^ w32.WS_EX_STATICEDGE &^ w32.WS_EX_CLIENTEDGE
		exStyle |= borderStyleEx(bs)
		w32.SetWindowLongPtr(w.hWnd, w32.GWL_EXSTYLE, uintptr(exStyle))
		w32.SetWindowPos(
			w.hWnd, 0,
			w.x, w.y, w.width, w.height,
			w32.SWP_NOOWNERZORDER|w32.SWP_NOZORDER|w32.SWP_FRAMECHANGED|w32.SWP_NOMOVE|w32.SWP_NOSIZE,
		)
	}
}

func (w *goWidget) setGeometry() {
	var borderSizeX int = 0
	var borderSizeY int = 0
	var textExtent w32.SIZE
	if w.autoSize == true {
		//l.autoSize = false
		//l.sizePolicy = &GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true}
		if w.border != BorderNone {
			rcWind := w32.GetWindowRect(w.hWnd)
			rcClient := w32.GetClientRect(w.hWnd)
			borderSizeX = int(rcWind.Right - rcWind.Left - rcClient.Right)
			borderSizeY = int(rcWind.Bottom - rcWind.Top - rcClient.Bottom)
			log.Println("SetBorderStyle sizeX =", borderSizeX)
			log.Println("SetBorderStyle sizeY =", borderSizeY)
		//w32.MapWindowPoints(0, l.Parent().hWndParent, rect, 2)
		//w32.InvalidateRect(l.Parent().hWndParent, rect, true)
		}

		hDC := w32.GetDC(w.hWnd)
		w32.GetTextExtentPoint32(hDC, syscall.StringToUTF16Ptr(w.text), len(w.text), &textExtent)
		w32.ReleaseDC(w.hWnd, hDC)
		w.width = int(textExtent.CX) + w.padding.Left + w.padding.Right  + borderSizeX
		w.height = int(textExtent.CY) + w.padding.Top + w.padding.Bottom  + borderSizeY
		log.Println("setGeometry width =", w.width)
		log.Println("setGeometry height =", w.height)
		if w.style & w32.BS_AUTORADIOBUTTON == w32.BS_AUTORADIOBUTTON { 
			w.width += (int(textExtent.CY) + 5)
		} else if w.style & w32.BS_AUTOCHECKBOX == w32.BS_AUTOCHECKBOX {
			w.width += (int(textExtent.CY) + 5)
		}
		//l.SetSize(int(textExtent.CX) + w.padding.left + w.padding.right + (borderSizeX * 2), int(textExtent.CY) + w.padding.top + w.padding.bottom + (borderSizeY * 2))
	}
	if w.width < 0 {
		w.width = 0
	}
	if w.height < 0 {
		w.height = 0
	}
	if w.hWnd != 0 {
		// The window will receive a WM_SIZE message which inturn triggers function  
		// GoLayout.onWM_SIZE() which will handle the layout ofchild controls.
		w32.SetWindowPos(
			w.hWnd, 0,
			w.x, w.y, w.width, w.height,
			w32.SWP_NOOWNERZORDER|w32.SWP_NOZORDER,
		)
	}
}

