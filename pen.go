/* win32go/pen.go */

package win32

import (
	"log"
	"gitlab.com/win32go/win32/w32"
)

func CreatePen(color GoColor, width int, style int, ref string) (*GoPen) {
	if ref == "" {
		return nil
	}
	gopen := &GoPen{color: color, width: width, style: style, ref: ref}
	//gopen.hpen = w32.CreatePen(style, width, color.W32Color())
	
	return gopen
}

type GoPen struct {
	hpen 	w32.HPEN
	color GoColor
	width int
	style int
	ref  	string
}

func (p *GoPen) Close() () {
	log.Println("GoPen::Close............")
  w32.DeleteObject(w32.HGDIOBJ(p.hpen))
}

func (p *GoPen) Create() (w32.HPEN) {
	p.hpen = w32.CreatePen(p.style, p.width, colorToW32(p.color))
	return p.hpen
}




/*typedef  enum
 {
   PS_SOLID = 0x00000000,
   PS_DASH = 0x00000001,
   PS_DOT = 0x00000002,
   PS_DASHDOT = 0x00000003,
   PS_DASHDOTDOT = 0x00000004,
   PS_NULL = 0x00000005,
   PS_INSIDEFRAME = 0x00000006
 } PenStyle;*/

/*
PS_SOLID
The pen is solid.
PS_DASH
The pen is dashed. This style is valid only when the pen width is one or less in device units.
PS_DOT
The pen is dotted. This style is valid only when the pen width is one or less in device units.
PS_DASHDOT
The pen has alternating dashes and dots. This style is valid only when the pen width is one or less in device units.
PS_DASHDOTDOT
The pen has alternating dashes and double dots. This style is valid only when the pen width is one or less in device units.
PS_NULL
The pen is invisible.
PS_INSIDEFRAME:  The pen is solid. When this pen is used with a bounding rectangle, the dimensions of the figure are shrunk so that it fits entirely in the bounding rectangle and takes into account the width of the pen. This applies only to geometric pens.
*/