/* syscolor */

package win32

import (
	"gitlab.com/win32go/win32/w32"
)

// These are predefined colors defined by the current Windows theme.
/*var (
	

	// Desktop.
	ColorBackground = sysColor(w32.COLOR_BACKGROUND)

	// Desktop.
	ColorDesktop = sysColor(w32.COLOR_DESKTOP)

	// Background color of multiple document interface (MDI) applications.
	ColorAppWorkspace = sysColor(w32.COLOR_APPWORKSPACE)

)*/

/********************************************************************************/
/*    GoWindow color scheme                                                     */
/*                                                                              */
/********************************************************************************/

// **ColorWindowFrame - window frame.

// ColorWindow - window background.
	// The associated foreground colors are COLOR_WINDOWTEXT and COLOR_HOTLITE.

// ColorWindowText - text in windows.
	// The associated background color is COLOR_WINDOW.

// ColorHotlight - Color for a hyperlink or hot-tracked item.
	// The associated background color is COLOR_WINDOW.

// **ColorActiveBorder - active window border.

// **ColorInactiveBorder - inactive window border.

// **ColorActiveCaption - active window title bar background.
	// The associated foreground color is COLOR_CAPTIONTEXT.
	// Specifies the left side color in the color gradient of an active window's
	// title bar if the gradient effect is enabled.

// **ColorCaptionText - Text in caption, size box, and scroll bar arrow box.
	// The associated background color is COLOR_ACTIVECAPTION.

// **ColorInactiveCaption - Inactive window caption.
	// The associated foreground color is COLOR_INACTIVECAPTIONTEXT.
	// Specifies the left side color in the color gradient of an inactive window's title bar
	// if the gradient effect is enabled.

// **ColorInactiveCaptionText - Color of text in an inactive caption.
	//  The associated background color is COLOR_INACTIVECAPTION.

// **ColorGradientActiveCaption - right side color in the color gradient of an active window's title bar.
	// COLOR_ACTIVECAPTION specifies the left side color.
	// Use SPI_GETGRADIENTCAPTIONS with the SystemParametersInfo function to determine
	// whether the gradient effect is enabled.

// **ColorGradientInactiveCaption - right side color in the color gradient of an inactive window's title bar.
	// COLOR_INACTIVECAPTION specifies the left side color.

/********************************************************************************/
/*    GoMenuBar color scheme                                                    */
/*                                                                              */
/********************************************************************************/

// **ColorMenu - Menu background.
	// The associated foreground color is COLOR_MENUTEXT.

// **ColorMenuText - Text in menus.
	// The associated background color is COLOR_MENU.

// **ColorMenuBar - The background color for the menu bar when menus appear as flat menus
	// (see SystemParametersInfo). However, COLOR_MENU continues to specify the
	// background color of the menu popup. Windows 2000: This value is not
	// supported.

// **ColorMenuHighlight - The color used to highlight menu items when the menu appears as a flat menu
	// (see SystemParametersInfo). The highlighted menu item is outlined
	// with COLOR_HIGHLIGHT. Windows 2000: This value is not supported.


/********************************************************************************/
/*     GoButton color scheme                                                    */
/*                                                                              */
/********************************************************************************/

// Color3DFace - Face color for three-dimensional display elements.
	// and for dialog box backgrounds.

// **ColorButtonFace - Face color for push buttons and for dialog box backgrounds.
	// The associated foreground color is COLOR_BTNTEXT.

// ColorButtonText - Text on push buttons.
	// The associated background color is COLOR_BTNFACE.

// ColorGrayText - Grayed (disabled) text.
	// This color is set to 0 if the current display driver does not support a solid gray color.

// **ColorButtonHighlight - Highlight color for push buttons.
	// For edges facing the light source.

/********************************************************************************/
/*     3D Controls color scheme                                                 */
/*                                                                              */
/********************************************************************************/

// Color3DFace - Face color for three-dimensional display elements.
	// and for dialog box backgrounds.

// **Color3DLight = Light color for three-dimensional display elements.
	// For edges facing the light source.

// **Color3DHighlight - Highlight color for three-dimensional display elements.
	// For edges facing the light source.

// **Color3DShadow = Shadow color for three-dimensional display elements.
	// For edges facing away from the light source.

// **Color3DDarkShadow - Dark shadow for three-dimensional display elements.


/********************************************************************************/
/*     GoScrollBar color scheme                                                 */
/*                                                                              */
/********************************************************************************/

// **ColorScrollBar - Scroll bar gray area.

/********************************************************************************/
/*     GoTooltips and hyperlinked color scheme                                  */
/*                                                                              */
/********************************************************************************/

// **ColorInfoText = Text color for tooltip controls.
	// The associated background color is COLOR_INFOBK.

// **ColorInfoBackground - Background color for tooltip controls.
	// The associated foreground color is COLOR_INFOTEXT.

// ColorHighlight - Item(s) selected in a control.
	// The associated foreground color is COLOR_HIGHLIGHTTEXT.

// ColorHighlightText = Text of item(s) selected in a control.
	// The associated background color is COLOR_HIGHLIGHT.

func GoSystemColors() (*goSystemColors) {
	sysColors := &goSystemColors{}
	sysColors.setSystemColors()
	return sysColors
}

type goSystemColors struct {
	ColorScrollBar  GoColor
	ColorBackground  GoColor
	ColorDesktop  GoColor
	ColorActiveCaption  GoColor
	ColorInactiveCaption  GoColor
	ColorMenu  GoColor
	ColorWindow  GoColor 				//**
	ColorWindowFrame  GoColor
	ColorMenuText  GoColor
	ColorWindowText  GoColor 			//**
	ColorCaptionText  GoColor
	ColorActiveBorder  GoColor
	ColorInactiveBorder  GoColor
	ColorAppWorkspace  GoColor
	ColorHighlight  GoColor 			//**
	ColorHighlightText  GoColor 		//**
	Color3DFace  GoColor 				//**
	ColorButtonFace  GoColor
	ColorGrayText  GoColor 				//**
	ColorButtonText  GoColor 			//**
	ColorInactiveCaptionText  GoColor
	Color3DHighlight  GoColor
	ColorButtonHighlight  GoColor
	Color3DShadow  GoColor
	Color3DDarkShadow  GoColor
	Color3DLight  GoColor
	ColorInfoText  GoColor
	ColorInfoBackground  GoColor
	ColorHotlight  GoColor 				//**
	ColorGradientActiveCaption  GoColor
	ColorGradientInactiveCaption  GoColor
	ColorMenuHighlight  GoColor
	ColorMenuBar  GoColor
}

func (sc *goSystemColors)setSystemColors() {
	/********** WINDOWS 10 System Colors *********/
	// Background of pages, panes, popups, and windows.
	sc.ColorWindow = sysColor(w32.COLOR_WINDOW)
	// Headings, body copy, lists, placeholder text, app and window borders, any UI that can't be interacted with.
	sc.ColorWindowText = sysColor(w32.COLOR_WINDOWTEXT)
	// Hyperlinks.
	sc.ColorHotlight = sysColor(w32.COLOR_HOTLIGHT)
	// Inactive (disabled) UI.
	sc.ColorGrayText = sysColor(w32.COLOR_GRAYTEXT)
	// Foreground color for text or UI that is in selected, interacted with (hover, pressed), or in progress.
	sc.ColorHighlightText = sysColor(w32.COLOR_HIGHLIGHTTEXT)
	// Background or accent color for UI that is in selected, interacted with (hover, pressed), or in progress.
	sc.ColorHighlight = sysColor(w32.COLOR_HIGHLIGHT)
	// Foreground color for buttons and any UI that can be interacted with.
	sc.ColorButtonText = sysColor(w32.COLOR_BTNTEXT)
	// Background color for buttons and any UI that can be interacted with.
	sc.Color3DFace = sysColor(w32.COLOR_3DFACE)

	/********** WINDOWS XP System Colors *********/

	// MDI App Workspace display. **********************************

	sc.ColorAppWorkspace = sysColor(w32.COLOR_APPWORKSPACE)

	// Window App display. **********************************

	// Background color.
	sc.ColorBackground = sysColor(w32.COLOR_BACKGROUND)
	// Desktop background color.
	sc.ColorDesktop = sysColor(w32.COLOR_DESKTOP)


	// WINDOW display elements. **********************************

	// Frame color of pages, panes, popups, and windows.
	sc.ColorWindowFrame = sysColor(w32.COLOR_WINDOWFRAME)

	// Active window border.
	sc.ColorActiveBorder = sysColor(w32.COLOR_ACTIVEBORDER)
	// Active window title bar background.
	sc.ColorActiveCaption = sysColor(w32.COLOR_ACTIVECAPTION)
	// Active right side color in the color gradient of an active window's title bar background.
	sc.ColorGradientActiveCaption = sysColor(w32.COLOR_GRADIENTACTIVECAPTION)
	// Active text in caption, size box, and scroll bar arrow box.
	sc.ColorCaptionText = sysColor(w32.COLOR_CAPTIONTEXT)

	// Inactive window border.
	sc.ColorInactiveBorder = sysColor(w32.COLOR_INACTIVEBORDER)
	// Inactive window title bar background.
	sc.ColorInactiveCaption = sysColor(w32.COLOR_INACTIVECAPTION)
	// Inactive right side color in the color gradient of an inactive window's title bar background.
	sc.ColorGradientInactiveCaption = sysColor(w32.COLOR_GRADIENTINACTIVECAPTION)
	// Inactive text in caption, size box, and scroll bar arrow box.
	sc.ColorInactiveCaptionText = sysColor(w32.COLOR_INACTIVECAPTIONTEXT)


	// MENU display elements. **********************************

	// MenuBar background color.
	sc.ColorMenuBar = sysColor(w32.COLOR_MENUBAR)
	// Menu background color.
	sc.ColorMenu = sysColor(w32.COLOR_MENU)
	// Menu foreground text. 
	sc.ColorMenuText = sysColor(w32.COLOR_MENUTEXT)
	// Highlight menu items when the menu appears as a flat menu
	sc.ColorMenuHighlight = sysColor(w32.COLOR_MENUHILIGHT)


	// BUTTON display elements. **********************************

	// Face color for push buttons and for dialog box backgrounds.
	sc.ColorButtonFace = sysColor(w32.COLOR_BTNFACE)


	// 3D display elements. **************************************

	// Light color for edges facing the light source.
	sc.Color3DLight = sysColor(w32.COLOR_3DLIGHT)
	// Highlight color for edges facing the light source.
	sc.Color3DHighlight = sysColor(w32.COLOR_3DLIGHT)
	// Shadow color for edges facing away from the light source. 
	sc.Color3DShadow = sysColor(w32.COLOR_3DSHADOW)
	// Dark shadow color for edges facing away from the light source. 
	sc.Color3DDarkShadow = sysColor(w32.COLOR_3DDKSHADOW)
	
	

	// SCROLLBAR display elements. **********************************

	sc.ColorScrollBar = sysColor(w32.COLOR_SCROLLBAR)
	if sc.ColorScrollBar == 0 {
		sc.ColorScrollBar = Color_Gray
	}

	// INFOTEXT display elements. **********************************

	sc.ColorInfoText = sysColor(w32.COLOR_INFOTEXT)
	sc.ColorInfoBackground = sysColor(w32.COLOR_INFOBK)

}

func (sc *goSystemColors) SetScrollBarColor(color  GoColor) {
	sc.ColorScrollBar = color
}

func (sc *goSystemColors) SetBackgroundColor(color  GoColor) {
	sc.ColorBackground = color
}

func (sc *goSystemColors) SetDesktopColor(color  GoColor) {
	sc.ColorDesktop = color
}

func (sc *goSystemColors) SetActiveCaptionColor(color  GoColor) {
	sc.ColorActiveCaption = color
}

func (sc *goSystemColors) SetInactiveCaptionColor(color  GoColor) {
	sc.ColorInactiveCaption = color
}

func (sc *goSystemColors) SetMenuColor(color  GoColor) {
	sc.ColorMenu = color
}

func (sc *goSystemColors) SetWindowColor(color  GoColor) {
	sc.ColorWindow = color
}

func (sc *goSystemColors) SetWindowFrameColor(color  GoColor) {
	sc.ColorWindowFrame = color
}

func (sc *goSystemColors) SetMenuTextColor(color  GoColor) {
	sc.ColorMenuText = color
}

func (sc *goSystemColors) SetWindowTextColor(color  GoColor) {
	sc.ColorWindowText = color
}

func (sc *goSystemColors) SetCaptionTextColor(color  GoColor) {
	sc.ColorCaptionText = color
}

func (sc *goSystemColors) SetActiveBorderColor(color  GoColor) {
	sc.ColorActiveBorder = color
}

func (sc *goSystemColors) SetInactiveBorderColor(color  GoColor) {
	sc.ColorInactiveBorder = color
}

func (sc *goSystemColors) SetAppWorkspaceColor(color  GoColor) {
	sc.ColorAppWorkspace = color
}

func (sc *goSystemColors) SetHighlightColor(color  GoColor) {
	sc.ColorHighlight = color
}

func (sc *goSystemColors) SetHighlightTextColor(color  GoColor) {
	sc.ColorHighlightText = color
}

func (sc *goSystemColors) Set3DFaceColor(color  GoColor) {
	sc.Color3DFace = color
}

func (sc *goSystemColors) SetButtonFaceColor(color  GoColor) {
	sc.ColorButtonFace = color
}

func (sc *goSystemColors) SetGrayTextColor(color  GoColor) {
	sc.ColorGrayText = color
}

func (sc *goSystemColors) SetButtonTextColor(color  GoColor) {
	sc.ColorButtonText = color
}

func (sc *goSystemColors) SetInactiveCaptionTextColor(color  GoColor) {
	sc.ColorInactiveCaptionText = color
}

func (sc *goSystemColors) Set3DHighlightColor(color  GoColor) {
	sc.Color3DHighlight = color
}

func (sc *goSystemColors) SetButtonHighlightColor(color  GoColor) {
	sc.ColorButtonHighlight = color
}

func (sc *goSystemColors) Set3DDarkShadowColor(color  GoColor) {
	sc.Color3DDarkShadow = color
}

func (sc *goSystemColors) Set3DLightColor(color  GoColor) {
	sc.Color3DLight = color
}

func (sc *goSystemColors) SetInfoTextColor(color  GoColor) {
	sc.ColorInfoText = color
}

func (sc *goSystemColors) SetInfoBackgroundColor(color  GoColor) {
	sc.ColorInfoBackground = color
}

func (sc *goSystemColors) SetHotlightColor(color  GoColor) {
	sc.ColorHotlight = color
}

func (sc *goSystemColors) SetGradientActiveCaptionColor(color  GoColor) {
	sc.ColorGradientActiveCaption = color
}

func (sc *goSystemColors) SetGradientInactiveCaptionColor(color  GoColor) {
	sc.ColorGradientInactiveCaption = color
}

func (sc *goSystemColors) SetMenuHighlightColor(color GoColor) {
	sc.ColorMenuHighlight = color
}

func (sc *goSystemColors) SetMenuBarColor(color GoColor) {
	sc.ColorMenuBar = color
}
	
func sysColor(index int) GoColor {
	color := w32.GetSysColor(index)
	//c := GoColor().fromW32Color(color)
	return colorFromW32(color)
}
