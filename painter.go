/* win32go/painter.go */

package win32

import (
  //"errors"
  "log"
  "math"
	"gitlab.com/win32go/win32/w32"
)

// DrawText[Ex] format flags
/*const (
  DT_Top                  = 0x00000000
  DT_Left                 = 0x00000000
  DT_Center               = 0x00000001
  DT_RIGHT                = 0x00000002
  DT_VCENTER              = 0x00000004
  DT_BOTTOM               = 0x00000008
  DT_WORDBREAK            = 0x00000010
  DT_SINGLELINE           = 0x00000020
  DT_EXPANDTABS           = 0x00000040
  DT_TABSTOP              = 0x00000080
  DT_NOCLIP               = 0x00000100
  DT_EXTERNALLEADING      = 0x00000200
  DT_CALCRECT             = 0x00000400
  DT_NOPREFIX             = 0x00000800
  DT_INTERNAL             = 0x00001000
  DT_EDITCONTROL          = 0x00002000
  DT_PATH_ELLIPSIS        = 0x00004000
  DT_END_ELLIPSIS         = 0x00008000
  DT_MODIFYSTRING         = 0x00010000
  DT_RTLREADING           = 0x00020000
  DT_WORD_ELLIPSIS        = 0x00040000
  DT_NOFULLWIDTHCHARBREAK = 0x00080000
  DT_HIDEPREFIX           = 0x00100000
  DT_PREFIXONLY           = 0x00200000
)*/


func CreatePainter(hDC w32.HDC, rect *GoRectType) *GoPainter {
  gopainter := &GoPainter{rect, hDC, 0, 0, 0, map[string]*GoBrush{}, map[string]*GoFont{}, map[string]*GoPen{}, 0}
  //gopainter.pens = make(map[string]*GoPen)
  //gopainter.brushes = make(map[string]*GoBrush)
  //log.Println("CreatePainter::SelectObject::GetStockObject(w32.DC_PEN)")
  //gopainter.stockPen = w32.SelectObject(hDC, w32.GetStockObject(w32.DC_PEN))
	//gopainter.stockBrush = w32.SelectObject(hDC, w32.GetStockObject(w32.DC_BRUSH))
  /*savedDC :=*/ gopainter.SaveDC()
  return gopainter
}

type GoPainter struct {
  *GoRectType
  hDC w32.HDC
  //posX 	int
	//posY 	int
  //irect *GoRect
  hStockFont w32.HGDIOBJ
  //stockBrush w32.HGDIOBJ
  //stockPen w32.HGDIOBJ
  savedDC int
	hFont 	w32.HGDIOBJ
	//pen   *GoPen
  //brush *GoBrush
  brushes map[string]*GoBrush
  fonts map[string]*GoFont
	pens	map[string]*GoPen
  clippingRect w32.HRGN
	//backgroundMode bool
	//rasterOp int	
}

func (p *GoPainter) Rect() (rect *GoRectType) {
  return rect
}

func (p *GoPainter) ClipRect(rect *GoRectType) {
  if p.clippingRect != 0{
      w32.DeleteObject(w32.HGDIOBJ(p.clippingRect))
  }
  p.clippingRect = w32.CreateRectRgn(rect.left, rect.top, rect.right, rect.bottom)
  w32.SelectClipRgn(p.hDC, p.clippingRect)
}

func (p *GoPainter) Destroy() (bool) {
  //log.Println("GoPainter::Destroy")
  var success bool
  if p.RestoreDC(p.savedDC) {
    success = true
  } else {
    log.Println("GoPainter::Failed to restoreDC.")
  }
  for _, pen := range p.pens {
    success = w32.DeleteObject(w32.HGDIOBJ(pen.hpen))
    if !success {
      log.Println("GoPainter::Failed to delete pen.")
    }
  }
  for _, brush := range p.brushes {
    success = w32.DeleteObject(w32.HGDIOBJ(brush.hbrush))
    if !success {
      log.Println("GoPainter::Failed to delete brush.")
    }
  }
  if p.clippingRect != 0 {
    success = w32.DeleteObject(w32.HGDIOBJ(p.clippingRect))
  }
  /*if success == true {
    log.Println("GoPainter::Closed succesfully")
  }*/
  return success
}


func (p *GoPainter) DrawArc(x, y, width, height int, fromClockAngle, dAngle float64) {
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(c.hdc, w32.COLORREF(color))
  p.arcLike(x, y, width, height, fromClockAngle, dAngle, w32.Arc)
}

func (p *GoPainter) DrawEllipse(x, y, width, height int) () {
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(c.hdc, w32.COLORREF(color))
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.NULL_BRUSH))
  w32.Ellipse(p.hDC, x, y, x + width, y + height)
}

func (p *GoPainter) DrawFocusRect(x, y, width, height int) () {
   w32.DrawFocusRect(p.hDC, &w32.RECT{int32(x), int32(y), int32(x + width), int32(y + height)})
}

func (p *GoPainter) DrawFocusRectType(r *GoRectType) () {
   w32.DrawFocusRect(p.hDC, &w32.RECT{int32(r.Left()), int32(r.Top()), int32(r.Right()), int32(r.Bottom())})
}

func (p *GoPainter) DrawImage(img *GoImageObj, dest *GoRectType, srcX int, srcY int) {
  log.Println("GoPainter:DrawImage.......")
  log.Println("dest.X =", dest.X(), "dest.Y =", dest.Y())
  log.Println("dest.Width =", dest.Width(), "dest.Height =", dest.Height())
  log.Println("srcX =", srcX, "srcY =", srcY)


  if img == nil {
    return
  }
  if dest.Width() == 0 {
    dest.SetWidth(img.Width())
  }
  if dest.Height() == 0 {
    dest.SetHeight(img.Height())
  }

  hdcMem := w32.CreateCompatibleDC(p.hDC)
  old := w32.SelectObject(hdcMem, w32.HGDIOBJ(img.bitmap))

  w32.AlphaBlend(
    p.hDC,
    dest.X(), dest.Y(), dest.Width(), dest.Height(),
    hdcMem,
    srcX, srcY, img.Width()-srcX, img.Height()-srcY,
    w32.BLENDFUNC{
      BlendOp:             w32.AC_SRC_OVER,
      BlendFlags:          0,
      SourceConstantAlpha: 255,
      AlphaFormat:         w32.AC_SRC_ALPHA,
    },
  )

  w32.SelectObject(hdcMem, old)
  w32.DeleteDC(hdcMem)
}

func (p *GoPainter) DrawLine(x1, y1, x2, y2 int) () {
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(c.hdc, w32.COLORREF(color))
  w32.MoveToEx(p.hDC, x1, y1, nil)
  w32.LineTo(p.hDC, x2, y2)
}

func (p *GoPainter) DrawPolyline(a []w32.POINT) {
  if len(a) < 2 {
    return
  }
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(c.hdc, w32.COLORREF(color))
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.NULL_BRUSH))
  w32.Polyline(p.hDC, a)
}

func (p *GoPainter) DrawPolygon(a []w32.POINT) {
  if len(a) < 2 {
    return
  }
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(c.hdc, w32.COLORREF(color))
  //w32.SelectObject(c.hdc, w32.GetStockObject(w32.NULL_BRUSH))
  w32.Polygon(p.hDC, a)
}

func (p *GoPainter) DrawRect(x, y, width, height int) () {
  //x -= p.goRect.left
  //y -= p.goRect.top
  //w32.SelectObject(p.hDC, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(p.hDC, w32.COLORREF(color))
  //w32.SelectObject(p.hDC, w32.GetStockObject(w32.NULL_BRUSH))
  w32.Rectangle(p.hDC, x, y, x + width, y + height)
}

func (p *GoPainter) DrawRectType(r *GoRectType) () {
  //x -= p.goRect.left
  //y -= p.goRect.top
  //w32.SelectObject(p.hDC, w32.GetStockObject(w32.DC_PEN))
  //w32.SetDCPenColor(p.hDC, w32.COLORREF(color))
  //w32.SelectObject(p.hDC, w32.GetStockObject(w32.NULL_BRUSH))
  w32.Rectangle(p.hDC, r.Left(), r.Top(), r.Right(), r.Bottom())
}

func (p *GoPainter) DrawTextLine(text string, rc *GoRectType, hAlign GoAlignmentFlags, vAlign GoAlignmentFlags) () {
  w32.DrawText(p.hDC, text, -1, rc.W32Rect(), uint(hAlign) | w32.DT_SINGLELINE | uint(vAlign))
}

func (p *GoPainter) LineTo(x, y int) () {
  w32.LineTo(p.hDC, x, y)
}

func (p *GoPainter) MoveTo(x, y int) () {
  w32.MoveToEx(p.hDC, x, y, nil)
}

func (p *GoPainter) RestoreFont() () {
  w32.SelectObject(p.hDC, p.hFont)
}

func (p *GoPainter) RestoreStockFont() () {
  w32.SelectObject(p.hDC, p.hStockFont)
}

func (p *GoPainter) RestoreState(){
  p.RestoreDC(p.savedDC)
  for _, pen := range p.pens {
    w32.DeleteObject(w32.HGDIOBJ(pen.hpen))
  }
  for _, brush := range p.brushes {
    w32.DeleteObject(w32.HGDIOBJ(brush.hbrush))
  }
  // reinitialize, empty map
  p.pens = make(map[string]*GoPen)
  p.brushes = make(map[string]*GoBrush)
}

/*func (p *GoPainter) ResetPen() () {
  w32.SelectObject(p.hDC, p.stockPen)
}*/

func (p *GoPainter) RestoreDC(savedDC int) (success bool) {
  //log.Println("GoPainter::RestoreDC savedDC =", savedDC)
  ret := w32.RestoreDC(p.hDC, p.savedDC)

  //log.Println("GoPainter::RestoreDC ret =", ret)
  if ret != 0 {
    return true
  }
  return false
}

func (p *GoPainter) SaveDC() (savedDC int) {
  p.savedDC = w32.SaveDC(p.hDC)
  //log.Println("GoPainter::SaveDC ret =", savedDC)
  return p.savedDC
}

func (p *GoPainter) SaveState(){
  p.savedDC = p.SaveDC()
}

func (p *GoPainter) SetBackMode(mode GoBkMode) () {
  if mode == TransparentMode {
    w32.SetBkMode(p.hDC, w32.TRANSPARENT)
  } else {
    w32.SetBkMode(p.hDC, w32.OPAQUE)
  }
}

func (p *GoPainter) SetBrush(brush *GoBrush) () {
  hbrush := brush.Create()
  w32.SelectObject(p.hDC, w32.HGDIOBJ(hbrush))
  p.brushes[brush.ref] = brush
}

func (p *GoPainter) SetFont(font *GoFont) () {
  
  p.hFont = w32.SelectObject(p.hDC, w32.HGDIOBJ(font.handle))
  p.fonts[font.ref] = font
}

func (p *GoPainter) SetPen(pen *GoPen) () {
  hpen := pen.Create()
  w32.SelectObject(p.hDC, w32.HGDIOBJ(hpen))
  p.pens[pen.ref] = pen
}

func (p *GoPainter) SetNullBrush() {
  //log.Println("SetNullPen::SelectObject::GetStockObject(w32.NULL_PEN)")
  w32.SelectObject(p.hDC, w32.GetStockObject(w32.NULL_BRUSH))
}

func (p *GoPainter) SetNullPen() {
  //log.Println("SetNullPen::SelectObject::GetStockObject(w32.NULL_PEN)")
  w32.SelectObject(p.hDC, w32.GetStockObject(w32.NULL_PEN))
}

func (p *GoPainter) SetRop2(rop2 GoRasterOp) (oldRop2 int) {
  oldRop2 = w32.SetROP2(p.hDC, int(rop2))
  return oldRop2
}

func (p *GoPainter) SetStockBrush(color GoColor) () {
  //log.Println("SetStockBrushColor::SelectObject::GetStockObject(w32.DC_BRUSH)")
  w32.SelectObject(p.hDC, w32.GetStockObject(w32.DC_BRUSH))
  //log.Println("SetDCPenColor::Color")
  w32.SetDCBrushColor(p.hDC, colorToW32(color))
}

func (p *GoPainter) SetStockFont(stockFont int) () {
  hFont := w32.HFONT(w32.GetStockObject(stockFont))  //w32.ANSI_VAR_FONT);
  p.hStockFont = w32.SelectObject(p.hDC, w32.HGDIOBJ(hFont))
}

func (p *GoPainter) SetStockPen(color GoColor) () {
  //log.Println("SetStockPenColor::SelectObject::GetStockObject(w32.DC_PEN)")
  w32.SelectObject(p.hDC, w32.GetStockObject(w32.DC_PEN))
  //log.Println("SetDCPenColor::Color")
  w32.SetDCPenColor(p.hDC, colorToW32(color))
}



func (p *GoPainter) SetTextColor(color GoColor) () {
  w32.SetTextColor(p.hDC, colorToW32(color))
}
/*func (p *GoPainter) SetInnerRect(rect *GoRectType) {
  p.irect = rect
}*/

func (p *GoPainter) arcLike(x, y, width, height int, fromClockAngle, dAngle float64,
  draw func(w32.HDC, int, int, int, int, int, int, int, int) bool) {
  toRad := func(clock float64) float64 {
    return (90 - clock) * math.Pi / 180
  }
  a, b := fromClockAngle+dAngle, fromClockAngle
  if dAngle < 0 {
    a, b = b, a
  }
  y1, x1 := math.Sincos(toRad(a))
  y2, x2 := math.Sincos(toRad(b))
  x1, x2, y1, y2 = 100*x1, 100*x2, -100*y1, -100*y2
  round := func(f float64) int {
    if f < 0 {
      return int(f - 0.5)
    }
    return int(f + 0.5)
  }
  cx := float64(x) + float64(width)/2.0
  cy := float64(y) + float64(height)/2.0
  draw(
    p.hDC,
    x, y, x+width, y+height,
    round(cx+100*x1), round(cy+100*y1), round(cx+100*x2), round(cy+100*y2),
  )
}

/*HPEN CreatePen(
  [in] int      iStyle,
  [in] int      cWidth,
  [in] COLORREF color
);*/

/*typedef struct tagLOGBRUSH {
  UINT      lbStyle;
  COLORREF  lbColor;
  ULONG_PTR lbHatch;
} LOGBRUSH, *PLOGBRUSH, *NPLOGBRUSH, *LPLOGBRUSH;*/

/*typedef struct tagLOGFONTW {
  LONG  lfHeight;
  LONG  lfWidth;
  LONG  lfEscapement;
  LONG  lfOrientation;
  LONG  lfWeight;
  BYTE  lfItalic;
  BYTE  lfUnderline;
  BYTE  lfStrikeOut;
  BYTE  lfCharSet;
  BYTE  lfOutPrecision;
  BYTE  lfClipPrecision;
  BYTE  lfQuality;
  BYTE  lfPitchAndFamily;
  WCHAR lfFaceName[LF_FACESIZE];
} LOGFONTW, *PLOGFONTW, *NPLOGFONTW, *LPLOGFONTW;*/

/*typedef struct tagLOGPEN {
  UINT     lopnStyle;
  POINT    lopnWidth;
  COLORREF lopnColor;
} LOGPEN, *PLOGPEN, *NPLOGPEN, *LPLOGPEN;*/

/*HBRUSH CreateSolidBrush(
  [in] COLORREF color
);*/

/*Qt::CopyROP - dst = src
Qt::OrROP - dst = src OR dst
Qt::XorROP - dst = src XOR dst
Qt::NotAndROP - dst = (NOT src) AND dst
Qt::EraseROP - an alias for NotAndROP
Qt::NotCopyROP - dst = NOT src
Qt::NotOrROP - dst = (NOT src) OR dst
Qt::NotXorROP - dst = (NOT src) XOR dst
Qt::AndROP - dst = src AND dst
Qt::NotEraseROP - an alias for AndROP
Qt::NotROP - dst = NOT dst
Qt::ClearROP - dst = 0
Qt::SetROP - dst = 1
Qt::NopROP - dst = dst
Qt::AndNotROP - dst = src AND (NOT dst)
Qt::OrNotROP - dst = src OR (NOT dst)
Qt::NandROP - dst = NOT (src AND dst)
Qt::NorROP - dst = NOT (src OR dst)*/