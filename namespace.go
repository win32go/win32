/* win32/namespace.go */

package win32

type GoAlignmentFlags uint

const(
	AlignAuto GoAlignmentFlags = 0x0000
	AlignLeft GoAlignmentFlags = 0x0000
	AlignRight GoAlignmentFlags = 0x0002
	AlignHCenter GoAlignmentFlags = 0x0001
	AlignJustify GoAlignmentFlags = 0x0008
	AlignHorizontal_Mask GoAlignmentFlags = AlignLeft | AlignRight | AlignHCenter | AlignJustify
	AlignTop GoAlignmentFlags = 0x0000
	AlignBottom GoAlignmentFlags = 0x0008
	AlignVCenter GoAlignmentFlags = 0x0004
	AlignVertical_Mask GoAlignmentFlags = AlignTop | AlignBottom | AlignVCenter
	AlignCenter GoAlignmentFlags = AlignVCenter | AlignHCenter
)

type GoBkMode int

const (
	TransparentMode GoBkMode = 1
	OpaqueMode GoBkMode = 2
)

type GoBrushStyle int

const (
	NoBrush GoBrushStyle = 0
	SolidPattern GoBrushStyle = 1
	Dense1Pattern GoBrushStyle = 2
	Dense2Pattern GoBrushStyle = 3
	Dense3Pattern GoBrushStyle = 4
	Dense4Pattern GoBrushStyle = 5
	Dense5Pattern GoBrushStyle = 6
	Dense6Pattern GoBrushStyle = 7
	Dense7Pattern GoBrushStyle = 8
	HorPattern GoBrushStyle = 9
	VerPattern GoBrushStyle = 10
	CrossPattern GoBrushStyle = 11
	BDiagPattern GoBrushStyle = 12
	FDiagPattern GoBrushStyle = 13
	DiagCrossPattern GoBrushStyle = 14
	CustomPattern GoBrushStyle = 24
)

type GoButtonState int

const (
	NoButton GoButtonState = 0x0000
	LeftButton GoButtonState = 0x0001
	RightButton GoButtonState = 0x0002
	MiddleButton GoButtonState = 0x0004
	MouseButtonMask GoButtonState = 0x0007
	ShiftButton GoButtonState = 0x0100
	ControlButton GoButtonState = 0x0200
	AltButton GoButtonState = 0x0400
	MetaButton GoButtonState = 0x0800
	KeyButtonMask GoButtonState = 0x0f00
	Keypad GoButtonState = 0x4000
)

// Stock Logical Objects
const (
	WHITE_BRUSH         = 0
	LTGRAY_BRUSH        = 1
	GRAY_BRUSH          = 2
	DKGRAY_BRUSH        = 3
	BLACK_BRUSH         = 4
	NULL_BRUSH          = 5
	HOLLOW_BRUSH        = NULL_BRUSH
	WHITE_PEN           = 6
	BLACK_PEN           = 7
	NULL_PEN            = 8
	OEM_FIXED_FONT      = 10
	ANSI_FIXED_FONT     = 11
	ANSI_VAR_FONT       = 12
	SYSTEM_FONT         = 13
	DEVICE_DEFAULT_FONT = 14
	DEFAULT_PALETTE     = 15
	SYSTEM_FIXED_FONT   = 16
	DEFAULT_GUI_FONT    = 17
	DC_BRUSH            = 18
	DC_PEN              = 19
)

// Font weight constants
const (
	FW_Default    = 0
	FW_Thin       = 100
	FW_ExtraLight = 200
	FW_UltraLight = FW_ExtraLight
	FW_Light      = 300
	FW_Normal     = 400
	FW_Regular    = 400
	FW_Medium     = 500
	FW_SemiBold   = 600
	FW_DemiBold   = FW_SemiBold
	FW_Bold       = 700
	FW_ExtraBold  = 800
	FW_UltraBold  = FW_ExtraBold
	FW_Heavy      = 900
	FW_Black      = FW_Heavy
)

type GoMenuFlags int

const (
	Bitmap GoMenuFlags 		= 0x0004
	Checked GoMenuFlags 	= 0x0008
	Disabled GoMenuFlags     = 0x0002
	Enabled GoMenuFlags      = 0x0000
	Grayed GoMenuFlags       = 0x0001
	MenuBarBreak GoMenuFlags = 0x0020
	MenuBreak GoMenuFlags    = 0x0040
	OwnerDraw GoMenuFlags    = 0x0100
	Popup GoMenuFlags        = 0x0010
	Separator GoMenuFlags    = 0x0800
	String GoMenuFlags       = 0x0000
	Unchecked GoMenuFlags    = 0x0000
	ByCommand GoMenuFlags    = 0x0000
	ByPosition GoMenuFlags   = 0x0400
)

// type GoMessageBoxIcon defined in messagebox.go
/*type GoMessageBoxIcon int

const(
	NoIcon GoMessageBoxIcon 	= 0x00000000
	Question GoMessageBoxIcon 	= 0x00000020
	Information GoMessageBoxIcon = 0x00000040
	Warning GoMessageBoxIcon 	= 0x00000030
	Critical GoMessageBoxIcon 	= 0x00000010
)*/

// type GoMessageBoxResponse defined as IDxxx
/*type GoMessageBoxResponse int

const (
	IDOK GoMessageBoxResponse      	= 1
	IDCANCEL GoMessageBoxResponse  	= 2
	IDABORT GoMessageBoxResponse   	= 3
	IDRETRY GoMessageBoxResponse   	= 4
	IDIGNORE GoMessageBoxResponse  	= 5
	IDYES GoMessageBoxResponse     	= 6
	IDNO GoMessageBoxResponse      	= 7
	IDCLOSE GoMessageBoxResponse   	= 8
	IDHELP GoMessageBoxResponse    	= 9
	IDTRYAGAIN GoMessageBoxResponse	= 10
	IDCONTINUE GoMessageBoxResponse	= 11
	IDTIMEOUT GoMessageBoxResponse 	= 32000
)*/

const (
	IDOK int  		= 1
	IDCANCEL int 	= 2
	IDABORT int		= 3
	IDRETRY int  	= 4
	IDIGNORE int 	= 5
	IDYES int    	= 6
	IDNO int     	= 7
	IDCLOSE int  	= 8
	IDHELP int 		= 9
	IDTRYAGAIN	int = 10
	IDCONTINUE int	= 11
	IDTIMEOUT int 	= 32000
)

type GoOrientation int

const (
	Horizontal GoOrientation = 0
	Vertical GoOrientation = 1
)

type GoPenStyle int

const (
	NoPen GoPenStyle = 0
	SolidLine GoPenStyle = 1
	DashLine GoPenStyle = 2
	DotLine GoPenStyle = 3
	DashDotLine GoPenStyle = 4
	DashDotDotLine GoPenStyle = 5
	MPenStyle GoPenStyle = 15
)

type GoPenCapStyle int

const ( 
	FlatCap GoPenCapStyle = 0x00
	SquareCap GoPenCapStyle = 0x10
	RoundCap GoPenCapStyle = 0x20
	MPenCapStyle GoPenCapStyle = 0x30
)

type GoPenJoinStyle int

const (
	MiterJoin GoPenJoinStyle = 0x00
	BevelJoin GoPenJoinStyle = 0x40
	RoundJoin GoPenJoinStyle = 0x80
	MPenJoinStyle GoPenJoinStyle = 0xc0
)

type GoTextFlags int

const (
	SingleLine GoTextFlags = 0x0080
	DontClip GoTextFlags = 0x0100
	ExpandTabs GoTextFlags = 0x0200
	ShowPrefix GoTextFlags = 0x0400
	WordBreak GoTextFlags = 0x0800
	BreakAnywhere GoTextFlags = 0x1000
	NoAccel GoTextFlags = 0x4000
)

type GoWindowState int

const (
	WindowNoState GoWindowState = 0x00000000
	WindowMinimized GoWindowState = 0x00000001
	WindowMaximized GoWindowState = 0x00000002
	WindowFullScreen GoWindowState = 0x00000004
	WindowActive GoWindowState = 0x00000008
)

// Foreground mix mode for raster drawing
type GoRasterOp int

const (
	BlackRop = 1
	NotMergeRop = 2
	MaskNotRop = 3
	NotCopyRop = 4
	MaskRopNot = 5
	NotRop = 6
	XorRop = 7
	NotMaskRop = 8
	MaskRop = 9
	NotXorRop = 10
	NoRop = 11
	MergeNotRop = 12
	CopyRop = 13
  	MergeRopNot = 14
	MergeRop = 15
	WhiteRop = 16
)