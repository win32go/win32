/* win32/menu.go */

package win32

import (
	"errors"
	"log"
	"syscall"
	"unsafe"

	"gitlab.com/win32go/win32/w32"
)

func GoMenuBar(owner GoObject) (*GoMenu, error) {
	if owner != nil {
		hmenubar := w32.CreateMenu()
		if(hmenubar != 0) {
			menuBar := &GoMenu{owner, nil, hmenubar, "", map[int]*GoAction{}}
			w32.SetMenu(owner.wid().hWnd, hmenubar)
			return menuBar, nil
		}
		log.Println("Failed to create GoMenuBar.")
		return nil, errors.New("Failed to create GoMenuBar.")
	}
	log.Println("GoMenuBar must have owner to attach to.")
	return nil, errors.New("GoMenuBar must have owner to attach to.")
}

/*type goMenuBar struct {
	goObject
	hMenuBar   w32.HMENU
	menu
	actions 	map[int]*goMenuAction
	//getDPI  func() int
}*/



/*func (m *goMenuBar) AppendBitmapMenuItem(hBitmap w32.HBITMAP) {
	
}*/

func CreateMenu(owner GoObject, parent *GoMenu, text string) (menu *GoMenu) {
	hmenu := w32.CreatePopupMenu()
	menu = &GoMenu{owner, parent, hmenu, text, map[int]*GoAction{}}
	return menu
}

type GoMenu struct {
	owner 	GoObject
	parent 	*GoMenu
	hMenu 	w32.HMENU
	text 	string
	actions	map[int]*GoAction
}

func (m *GoMenu) AddMenu(text string) (*GoMenu) {
	menu := CreateMenu(m.owner, m, text)
	w32.AppendMenu(m.hMenu, uint(Popup), uintptr(menu.hMenu), text)
	return menu
}

func (m *GoMenu) AddAction(text string, iconSet ...*GoIcon) (*GoAction) {
	var typ GoMenuFlags = String
	var icon *GoIcon
	if len(iconSet) > 0 {
		icon = iconSet[0]
	}
	id, menuAction := GoMenuAction(m.parent, typ, text, icon)
	m.actions[id] = menuAction
	ret := w32.AppendMenu(m.hMenu, uint(typ), uintptr(id), text)
	/*if icon != nil {
		w32.SetMenuItemBitmaps(m.hmenu, id, MF_BITMAP, hBitmap,hBitmap);
	}*/
	if ret {
		goApp.AddAction(menuAction, id)
		w32.DrawMenuBar(m.owner.wid().hWnd)
		return menuAction
	}
	return nil
}

func (m *GoMenu) AddSeparator() {
	var typ GoMenuFlags = Separator
	var icon *GoIcon
	id, menuSeparator := GoMenuAction(m.parent, typ, "", icon)
	m.actions[id] = menuSeparator
	ret := w32.AppendMenu(m.hMenu, uint(typ), uintptr(id), "")
	if ret {
		goApp.AddAction(menuSeparator, id)
		w32.DrawMenuBar(m.owner.wid().hWnd)
	}
}


/***************************************************************************/
/* GoAction                                                                */
/*                                                                         */
/***************************************************************************/

func GoMenuAction(menu *GoMenu, typ GoMenuFlags, text string, icon ...*GoIcon) (id int, action *GoAction) {
	
	id = nextMenuId()
	action = &GoAction{
		menu: menu,
		typ: typ,
		id: id,
		handle: 0,
		
		checked: false,
		iconSet: nil,
		statusTip: text,
		text: text,
		toggleAction: false,
		toggleOn: false,
		toolTip: text,
		onActivate: nil,
	}
	
	if len(icon) > 0 {
		action.iconSet = icon[0]
	}
	return id, action
}

type GoAction struct {
	menu 			*GoMenu
	typ 			GoMenuFlags
	id      		int
	handle  		w32.HANDLE
	checked 		bool
	iconSet 		*GoIcon
	statusTip 		string
	text    		string
	toggleAction 	bool
	toggleOn 		bool
	toolTip 		string
	
	onActivate 		func()
}

func (a *GoAction) Checked() bool {
	return a.checked
}

func (a *GoAction) Icon() (icon *GoIcon) {
	return a.iconSet
}

func (a *GoAction) Text() string {
	return a.text
}

func (a *GoAction) SetChecked(c bool) {
	if a.typ != Separator {
		a.checked = c
		if a.menu != nil {
			var info w32.MENUITEMINFO
			info.Mask = w32.MIIM_STATE
			if c {
				info.State = w32.MFS_CHECKED
			} else {
				info.State = w32.MFS_UNCHECKED
			}
			w32.SetMenuItemInfo(a.menu.hMenu, uint(a.id), false, &info)
		}
	}
}

func (a *GoAction) SetIcon(icon *GoIcon) {
	if a.typ != Separator {
		a.iconSet = icon
	}
}

func (a *GoAction) SetText(s string) {
	if a.typ != Separator {
		a.text = s
		if a.menu != nil {
			var info w32.MENUITEMINFO
			info.Mask = w32.MIIM_STRING
			info.TypeData = uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(s)))
			w32.SetMenuItemInfo(a.menu.hMenu, uint(a.id), false, &info)

			w32.DrawMenuBar(a.menu.owner.wid().hWnd)
		}
		a.statusTip = s
		a.toolTip = s
	}
}

func (a *GoAction) SetToolTip(toolTip string) {
	if a.typ != Separator {
		a.toolTip = toolTip
	}
}

func (a *GoAction) OnActivate() func() {
	return a.onActivate
}

func (a *GoAction) SetOnActivate(f func()) {
	if a.typ != Separator {
		a.onActivate = f
	}
}