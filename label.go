/* label.go */
/* The GoLabel object GoLabelObj is used to display either text or an image. GoLabel has no user interaction. */
package win32

import (
	"log"
	"reflect"
	"syscall"
	"gitlab.com/win32go/win32/w32"
)

func GoLabel(parent GoObject, args ...interface{}) (hLabel *GoLabelObj) { 	//parent GoObject
	var sizeToView bool
	var text string
	var backgroundColor GoColor
	for i, v := range args {
		log.Println("GoLabel() - arg:", i, "value:", v)
		switch v := reflect.ValueOf(v); v.Kind() {
		case reflect.String:
			log.Println("GoLabel() - v.String():", v.String())
			text = v.String()
		case reflect.Uint32:
			log.Println("GoLabel() - v.Uint32():", args[i].(GoColor))
			backgroundColor = args[i].(GoColor)
		default:
			log.Println("GoLabel() - Not String or GoColor")
		}
	}
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 
	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 		"STATIC",
		windowName: 	text,
		style: 			w32.WS_CHILD | w32.SS_CENTERIMAGE | w32.SS_LEFT | w32.SS_OWNERDRAW | w32.SS_NOTIFY,		//  | w32.SS_WHITEFRAME
		exStyle:    	0,
		state:      	w32.SW_HIDE,
		x:      		0,
		y:      		0,
		width:  		100,
		height: 		28,
		hWndParent:		p_widget.hWnd,
		id:				p_nextid,
		instance:		p_widget.instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		autoSize:		false,
		border: 		BorderNone,
		cursor:     	nil,	// *goCursor
		font:       	nil, 	// *goFont
		margin:			&GoMarginType{2, 2, 2, 2},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 		text,
		window:     	false,
		widgets:    	map[int]*goWidget{},
		alpha:			0, 		//uint8
		backgroundMode:	TransparentMode,
		onShow:        	nil, 	//func()
		onClose:       	nil, 	//func()
		onCanClose:    	nil, 	//func() bool
		onMouseMove:   	nil, 	//func(x, y int)
		onMouseWheel:  	nil, 	//func(x, y int, delta float64)
		onMouseDown:   	nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     	nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     	nil, 	//func(key int)
		onKeyUp:       	nil, 	//func(key int)
		onResize:      	nil, 	//func()
	}
	object := goObject{parent, []GoObject{}}
	l := &GoLabelObj{widget, object, 0, "", AlignLeft, AlignVCenter, nil, nil}
	
	_, err := l.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		if backgroundColor != 0 {
			l.SetBackgroundColor(backgroundColor)
		}
		if text != "" {
			l.SetText(text)
		}
		//l.SetBackgroundMode(OpaqueMode)
		//l.SetBackgroundMode(TransparentMode)
		parent.addControl(l)
		GoApp().AddControl(l, p_nextid)
	}
	// Subclass Label Window to allow custom painting of label
	w32.SetWindowSubclass(l.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		//log.Println("GoLabel WndProc:: - ")
		//lwParamH := (lParam & 0xFFFF0000) >> 16
		//lwParamL := lParam & 0xFFFF
		//lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
		//lParamL := lParam & 0xFFFFFFFF
		switch msg {
			case w32.WM_PAINT:
				//log.Println("GoLabel:WM_PAINT.......")
				var ps w32.PAINTSTRUCT
				hDC := w32.BeginPaint(hWnd, &ps)
				rect := l.GetClientRect()
				rc := rect.DeductPadding(l.padding)
				//if l.backgroundMode == TransparentMode {
				
				//}
				pt := CreatePainter(hDC, rect)
				if l.backgroundMode == OpaqueMode {
					pt.SetStockBrush(l.backcolor)
					pt.SetStockPen(l.backcolor)
				} else {
					pt.SetNullBrush()
					pt.SetNullPen()
				}
				
				pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
				pt.DrawImage(l.image, rect, 0, 0)
				pt.SetTextColor(l.forecolor)
				pt.SetBackMode(TransparentMode)
				pt.DrawTextLine(l.text, rc, l.hAlign, l.vAlign)  
				//w32.DrawFocusRect(hdc, rect.W32RECT())
				pt.Destroy()
			    w32.EndPaint(hWnd, &ps)
				return 0
			/*case w32.WM_LBUTTONUP, w32.WM_MBUTTONUP, w32.WM_RBUTTONUP:
				log.Println("GoLabel:WM_WM_LBUTTONUP.......")
				mouseX := int(lParamL)
				mouseY := int(lParamH)
				if l.onMouseUp != nil {
					b := MouseButtonLeft
					if msg == w32.WM_MBUTTONUP {
						b = MouseButtonMiddle
					}
					if msg == w32.WM_RBUTTONUP {
						b = MouseButtonRight
					}
					l.onMouseUp(b, mouseX, mouseY)
					
					return 0
				} else {
					return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
				}*/
			/*case w32.WM_MOUSEMOVE:
				//log.Println("GoLabel:WM_WM_MOUSEMOVE.......")
				return w32.DefSubclassProc(hWnd, msg, wParam, lParam)*/
			default:
				return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		}
	}), 0, 0)
	//l.SetBorderStyle(PanelBorderRaised)	
	if sizeToView == true {
		GoTestView.SizeToControl(l)
	}
	return l
}

type GoLabelObj struct {
	goWidget
	goObject
	
	groupID int
	//autoSize bool
	text string
	hAlign GoAlignmentFlags
	vAlign GoAlignmentFlags
	image *GoImageObj
	onClick func(*GoLabelObj)
}

func (l *GoLabelObj) AddImage(image *GoImageObj) {
	l.image = image
}

func (l *GoLabelObj) AutoSize() (autoSize bool) {
	return l.autoSize
}

func (l *GoLabelObj) SetAlignment(hAlign GoAlignmentFlags) (old_align GoAlignmentFlags) { //  *GoLabelObj
	old_align = l.hAlign
	l.hAlign = hAlign
	if l.hWnd != 0 {
		style := uint(w32.GetWindowLongPtr(l.hWnd, w32.GWL_STYLE))
		style = style &^ w32.SS_LEFT &^ w32.SS_CENTER &^ w32.SS_RIGHT
		w32.SetWindowLongPtr(l.hWnd, w32.GWL_STYLE, uintptr(style | uint(l.hAlign)))
		w32.InvalidateRect(l.hWnd, nil, true)
	}
	return old_align
}

/*func (l *GoLabelObj) SetAlignment(align uint) {
	SetTextAlign(hdc HDC, alignment uint)
}*/

/*func (l *GoLabelObj) SetLeftAlign() *GoLabel {
	return l.setAlign(w32.SS_LEFT)
}

func (l *GoLabelObj) SetCenterAlign() *GoLabel {
	return l.setAlign(w32.SS_CENTER)
}

func (l *GoLabelObj) SetRightAlign() *GoLabel {
	return l.setAlign(w32.SS_RIGHT)
}*/

func (l *GoLabelObj) SetAutoSize(autoSize bool) {
	l.autoSize = autoSize
	l.setGeometry()
}

func (l *GoLabelObj) SetGroupId(groupId int) {
	l.groupID = groupId
}

func (l *GoLabelObj) SetText(text string) {
	l.text = text
	/*if l.hWnd != 0 {
		// TODO this does not work after closing a dialog window with a Label
		w32.SetWindowText(l.hWnd, text)
	}*/
	//w32.InvalidateRect(l.hWnd, nil, true)
}

func (l *GoLabelObj) SetTextColor(color GoColor) {
	l.forecolor = color
	//w32.InvalidateRect(l.hWnd, nil, true)
}

func (l *GoLabelObj) OnClick(*GoLabelObj) func(*GoLabelObj) {
	return l.onClick
}

func (l *GoLabelObj) SetOnClick(f func(*GoLabelObj)) {
	l.onClick = f
}

func (l *GoLabelObj) Show() {
	log.Println("GoLabelObj.Show()")
	w32.ShowWindow(l.hWnd, w32.SW_SHOW)
	l.state = w32.SW_SHOW
	l.visible = true
	parent := l.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (l *GoLabelObj) Text() string {
	/*if l.hWnd != 0 {
		log.Println("GoLabelObj::HWND =", l.hWnd)
		l.text = w32.GetWindowText(l.hWnd)
	}*/
	log.Println("GoLabelObj::Text =", l.text)
	return l.text
}

func (l *GoLabelObj) TextColor() (GoColor) {
	return l.forecolor
}

func (l *GoLabelObj) destroy() {
	/*for id, control := range l.controls {
		delete(l.controls, id)
	}*/
}

func (l *GoLabelObj) groupId() (id int) {
	return l.groupID
}

func (l *GoLabelObj) handleNotification(cmd uintptr) {
	log.Println("GoLabelObj::handleNotification..............")
	//buf := bufio.NewReader(os.Stdin)
    //buf.ReadBytes('\n')
	if cmd == w32.BN_CLICKED && l.onClick != nil {
		l.onClick(l)
	}/* else if cmd == w32.BN_DISABLE && l.onDisable != nil {
		l.onDisable()
	} else if cmd == w32.BN_DOUBLECLICKED && l.onDoubleClick != nil {
		l.onDoubleClick()
	} else if cmd == w32.BN_HILITE && l.onHiLite != nil {
		l.onHiLite()
	} else if cmd == w32.BN_PAINT && l.onPaint != nil {
		l.onPaint()
	} else if cmd == w32.BN_UNHILITE && l.onUnHiLite != nil {
		l.onUnHiLite()
	}*/
}

func (l *GoLabelObj) isDialog() (bool) {
	return false
}

func (l *GoLabelObj) isLayout() (bool) {
	return false
}

func (l *GoLabelObj) objectType() (string) {
	return "GoLabelObj"
}

func (l *GoLabelObj) wid() (*goWidget) {
	return &l.goWidget
}