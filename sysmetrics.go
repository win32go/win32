/* desktop.go */

package win32

import "gitlab.com/win32go/win32/w32"

/*func GetSystemMetrics() *goSystemMetrics {
	metrics := &goSystemMetrics{}
	metrics.setSystemMetrics()
	return metrics
}*/

type goSystemMetrics struct {
	//aspectX 	int
	//aspectY 	int
	//aspectXY 	int
	clientHeight int
	clientWidth int
	height 		int
	width 		int
	//horizRes 	int
	//vertRes 	int
	//horizSize int
	//vertSize	int

	borderWidth int
	borderHeight int
	fixedBorderWidth int
	fixedBorderHeight int
	resizeBorderWidth int
	resizeBorderHeight int

	captionHeight int
	captionButtonHeight int
	captionButtonWidth int
	smallIconHeight int
	smallIconWidth int
	iconHeight int
	iconWidth int
	scrollbarWidth int
	scrollbarHeight int
	menuHeight int
}

func (sm *goSystemMetrics) setSystemMetrics() {
	sm.clientHeight = w32.GetSystemMetrics(w32.SM_CYFULLSCREEN)
	sm.clientWidth = w32.GetSystemMetrics(w32.SM_CXFULLSCREEN)
	sm.height = w32.GetSystemMetrics(w32.SM_CYSCREEN)
	sm.width = w32.GetSystemMetrics(w32.SM_CXSCREEN)

	//sm.vertSize = w32.GetDeviceCaps(hDC, w32.VERTSIZE)
	//sm.horizSize = w32.GetSystemMetrics(w32.HORZSIZE)
	//sm.vertRes = w32.GetSystemMetrics(w32.LOGPIXELSY)
	//sm.horizRes = w32.GetSystemMetrics(w32.LOGPIXELSX)

	sm.borderWidth = w32.GetSystemMetrics(w32.SM_CXBORDER)
	sm.borderHeight = w32.GetSystemMetrics(w32.SM_CYBORDER)
	sm.fixedBorderWidth = w32.GetSystemMetrics(w32.SM_CXFIXEDFRAME)
	sm.fixedBorderHeight = w32.GetSystemMetrics(w32.SM_CYFIXEDFRAME)
	sm.resizeBorderWidth = w32.GetSystemMetrics(w32.SM_CXSIZEFRAME)
	sm.resizeBorderHeight = w32.GetSystemMetrics(w32.SM_CYSIZEFRAME)
	sm.captionHeight = w32.GetSystemMetrics(w32.SM_CYCAPTION)
	sm.captionButtonHeight = w32.GetSystemMetrics(w32.SM_CYSIZE)
	sm.captionButtonWidth = w32.GetSystemMetrics(w32.SM_CXSIZE)

	sm.smallIconHeight = w32.GetSystemMetrics(w32.SM_CYSMICON)
	sm.smallIconWidth = w32.GetSystemMetrics(w32.SM_CXSMICON)
	sm.iconHeight = w32.GetSystemMetrics(w32.SM_CYICON)
	sm.iconWidth = w32.GetSystemMetrics(w32.SM_CXICON)

	sm.scrollbarWidth = w32.GetSystemMetrics(w32.SM_CXHSCROLL)
	sm.scrollbarHeight = w32.GetSystemMetrics(w32.SM_CYHSCROLL)
	sm.menuHeight = w32.GetSystemMetrics(w32.SM_CYMENU)
	
	
}

func (sm *goSystemMetrics) ClientHeight() int {
	return sm.clientHeight
}

func (sm *goSystemMetrics) ClientWidth() int {
	return sm.clientWidth
}

func (sm *goSystemMetrics) Height() int {
	return sm.height
}

/*func (sm *goSystemMetrics) HorizontalRes() int {
	return sm.horizRes
}

func (sm *goSystemMetrics) HorizontalSize() int {
	return sm.horizSize
}

func (sm *goSystemMetrics) VerticalRes() int {
	return sm.vertRes
}

func (sm *goSystemMetrics) VerticalSize() int {
	return sm.vertSize
}*/

func (sm *goSystemMetrics) Width() int {
	return sm.width
}

func (sm *goSystemMetrics) FixedBorderHeight() int {
	return sm.fixedBorderHeight
}

func (sm *goSystemMetrics) FixedBorderWidth() int {
	return sm.fixedBorderWidth
}

func (sm *goSystemMetrics) ResizeBorderHeight() int {
	return sm.resizeBorderHeight
}

func (sm *goSystemMetrics) ResizeBorderWidth() int {
	return sm.resizeBorderWidth
}

func (sm *goSystemMetrics) BorderHeight() int {
	return sm.borderHeight
}

func (sm *goSystemMetrics) BorderWidth() int {
	return sm.borderWidth
}

func (sm *goSystemMetrics) SmallIconWidth() int {
	return sm.smallIconWidth
}

func (sm *goSystemMetrics) SmallIconHeight() int {
	return sm.smallIconHeight
}

func (sm *goSystemMetrics) IconWidth() int {
	return sm.iconWidth
}

func (sm *goSystemMetrics) IconHeight() int {
	return sm.iconHeight
}

func (sm *goSystemMetrics) CaptionHeight() int {
	return sm.captionHeight
}

func (sm *goSystemMetrics) CaptionButtonHeight() int {
	return sm.captionButtonHeight
}

func (sm *goSystemMetrics) CaptionButtonWidth() int {
	return sm.captionButtonWidth
}

func (sm *goSystemMetrics) MenuHeight() int {
	return sm.menuHeight
}

func (sm *goSystemMetrics) ScrollbarHeight() int {
	return sm.scrollbarHeight
}

func (sm *goSystemMetrics) ScrollbarWidth() int {
	return sm.scrollbarWidth
}

func (sm *goSystemMetrics) WindowSizeX(clientWidth int, scrollbar bool) int {
	width := clientWidth + (sm.borderWidth * 2)
	if scrollbar == true {
		width += sm.scrollbarWidth * 2
	}
	return width
}

func (sm *goSystemMetrics) WindowSizeY(clientHeight int, menu bool, scrollbar bool) int {
	height := clientHeight + (sm.borderWidth * 2)
	height += sm.captionHeight
	if menu {
		height += sm.menuHeight
	}
	if scrollbar {
		height += sm.scrollbarHeight * 2
	}
	return height
}