package win32

import (
	"errors"
	"gitlab.com/win32go/win32/w32"
)

func GoCursor(hWnd w32.HWND) (*goCursor, error) {
	handle := w32.GetCursor(hWnd)		// w32.HCURSOR
	if handle != 0 {
		cursor := &goCursor{hcursor: handle}
		return cursor, nil
	}
		return nil, errors.New("Window has no cursor.")
}
func GoStockCursor(typ uint32) (*goCursor, error) {
	cursor := &goCursor{hcursor: 0, stock: typ, custom: "",}
	hcursor, err := cursor.create()
	if err == nil {
		cursor.hcursor = hcursor
	}
	return cursor, err
}

func GoCustomCursor(name string) (*goCursor, error) {
	cursor := &goCursor{hcursor: 0, stock: w32.IDC_CUSTOM, custom: name,}
	hcursor, err := cursor.create()
	if err == nil {
		cursor.hcursor = hcursor
	}
	return cursor, err
}



/* Predefined cursor constants
const (
	IDC_ARROW       = 32512
	IDC_IBEAM       = 32513
	IDC_WAIT        = 32514
	IDC_CROSS       = 32515
	IDC_UPARROW     = 32516
	IDC_SIZENWSE    = 32642
	IDC_SIZENESW    = 32643
	IDC_SIZEWE      = 32644
	IDC_SIZENS      = 32645
	IDC_SIZEALL     = 32646
	IDC_NO          = 32648
	IDC_HAND        = 32649
	IDC_APPSTARTING = 32650
	IDC_HELP        = 32651
	IDC_ICON        = 32641
	IDC_SIZE        = 32640
	IDC_CUSTOM 		= 0
) */

type goCursor struct {
	hcursor 	w32.HCURSOR
	stock 		uint32
	custom 		string
}

func (c *goCursor) create() (w32.HCURSOR, error) {
	var hcursor w32.HCURSOR
	var err error
	if c.stock != w32.IDC_CUSTOM {
		hcursor = w32.LoadCursor(0, c.stock)
		err = nil
		if hcursor == 0 {
			err = errors.New("Failed to load stock cursor.")
		}
	} else  {
		handle, _ := w32.LoadImage(0, c.custom, w32.IMAGE_CURSOR, 0, 0, w32.LR_LOADFROMFILE)
		hcursor = w32.HCURSOR(handle)
	}
	return hcursor, err
}

type goCursorMetrics struct {
	Idx 	int 		// current position in string
	End 	int 		// current selection end position
	Start 	int 		// current selection start position
}

