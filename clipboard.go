/* clipboard.go */

package win32

import (
	"gitlab.com/win32go/win32/w32"

	"errors"
	"syscall"
	"unsafe"
)

const (
	UnicodeText = w32.CF_UNICODETEXT
)

type GoClipboard struct {
	
}

func (c *GoClipboard) Clear() {

}

func (c *GoClipboard) SetText(text string) (err error) {
	//runtime.LockOSThread()
	//defer runtime.UnlockOSThread()
	if !c.openClipboard() {
		return errors.New("Failed to open clipboard.")
	}
	if !c.emptyClipboard() {
		c.closeClipboard()
		return errors.New("Failed to clear clipboard.")
	}
	data := syscall.StringToUTF16(text)

	// "If the hMem parameter identifies a memory object, the object must have
	// been allocated using the function with the GMEM_MOVEABLE flag."
	h := w32.GlobalAlloc(w32.GMEM_MOVEABLE, uint32(len(data)*int(unsafe.Sizeof(data[0]))))
	if h == 0 {
		c.closeClipboard()
		return errors.New("Kernel panic. Failed to create global memory block for data.")
	}
	defer func() {
		if h != 0 {
			w32.GlobalFree(h)
		}
	}()

	l := w32.GlobalLock(w32.HGLOBAL(h))
	if l == nil {
		c.closeClipboard()
		return errors.New("Kernel panic. Failed to Lock memory block.")
	}

	w32.LstrcpyPointer(uintptr(l), uintptr(unsafe.Pointer(&data[0])))

	if !w32.GlobalUnlock(w32.HGLOBAL(h)) {
		c.closeClipboard()
		return errors.New("Kernel panic. Failed to Unlock memory block.")
	}

	r, err := w32.SetClipboardData(UnicodeText, w32.HANDLE(h))
	if r == 0 {
		c.closeClipboard()
		return err
	}

	h = 0 // suppress deferred cleanup
	if !c.closeClipboard() {
		return errors.New("Failed to close clipboard.")
	}
	
	return nil
}

func (c *GoClipboard) GetText() (text string, err error) {
	//runtime.LockOSThread()
	//defer runtime.UnlockOSThread()
	if c.openClipboard() {
		text, err = c.getClipboardText()
		if err != nil {
			c.closeClipboard()
			return "", err
		}
		if c.closeClipboard() {
			return text, nil
		}
		return "", errors.New("Failed to close clipboard.")
	}
	return "", errors.New("Failed to open clipboard.")
}

func (c *GoClipboard) closeClipboard() (ret bool) {
	ret = w32.CloseClipboard()
	return ret
}

func (c *GoClipboard) emptyClipboard() (ret bool) {
	ret = w32.EmptyClipboard()
	return ret
}

func (c *GoClipboard) getClipboardText() (text string, err error) {
	h, gerr := w32.GetClipboardData(UnicodeText)
	if h == 0 {
		return "", gerr
	}
	l := w32.GlobalLock(w32.HGLOBAL(h))
	if l == nil {
		return "", errors.New("Kernel panic. Failed to Lock memory block.")
	}
	text = syscall.UTF16ToString((*[1 << 20]uint16)(unsafe.Pointer(l))[:])
	if !w32.GlobalUnlock(w32.HGLOBAL(h)) {
		return "", errors.New("Kernel panic. Failed to Unlock memory block.")
	}
	return text, nil
}

func (c *GoClipboard) openClipboard() (ret bool) {
	ret = w32.OpenClipboard(0)
	return ret
}

