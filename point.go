/* win32/point.go */

package win32

import (
	"gitlab.com/win32go/win32/w32"
)

func GoPoint() *goPoint {
	return &goPoint{}
}

type goPoint struct {
	x 	int
	y 	int
}

func (p *goPoint) Pos() (x, y int) {
	return p.x, p.y
}

func (p *goPoint) SetPos(x, y int) {
	p.x = x
	p.y = y
}

func (p *goPoint) X() int{
	return p.x
}

func (p *goPoint) Y() int{
	return p.y
}

func (p *goPoint) SetX(x int) {
	p.x = x
}

func (p *goPoint) SetY(y int) {
	p.y = y
}

func (p *goPoint) W32Point() (w32.POINT) {
	return w32.POINT{int32(p.x), int32(p.y)}
}

func GoPointArray() *goPointArray {
	return &goPointArray{}
}

type goPointArray struct {
	array []goPoint
	w32Array []w32.POINT
}

func (a *goPointArray) AddPoint(p *goPoint) {
	a.array = append(a.array, *p)
	a.w32Array = append(a.w32Array, p.W32Point())
}

func (a *goPointArray) InsertPoint(idx int, p *goPoint) {
	array := append(a.array[:idx], *p)
	array = append(array, a.array[idx:]...)
	a.array = array
	w32array := append(a.w32Array[:idx], p.W32Point())
	w32array = append(w32array, a.w32Array[idx:]...)
	a.w32Array = w32array
}

func (a *goPointArray) DeletePoint(idx int) {
	a.array = append(a.array[:idx - 1], a.array[idx:]...)
}

func (a *goPointArray) Array() ([]goPoint) {
	return a.array
}

func (a *goPointArray) W32Array() ([]w32.POINT) {
	return a.w32Array
}