/* win32go/win32/messagebox.go */

package win32

import (
	//"errors"
	"log"
	//"strconv"
	"gitlab.com/win32go/win32/w32"
)

type GoMessageBoxStyle int

const(
	NoIcon GoMessageBoxStyle 	= 0x00000000
	Critical GoMessageBoxStyle 	= 0x00000010
	Question GoMessageBoxStyle 	= 0x00000020
	Warning GoMessageBoxStyle	= 0x00000030
	Information GoMessageBoxStyle = 0x00000040
)

type GoMessageBoxObj struct {
	goWidget		// embedded widget
	goObject 		// embedded object
	
	style          	GoMessageBoxStyle
	title         	string
	caption         string
	message 		string
	button0 		int
	button1 		int
	button2 		int
	
	//background    w32.HBRUSH

	lastFocus		w32.HWND
	
	layout 			*GoLayoutObj

	captionLabel 	*GoLabelObj
	messageLabel 	*GoLabelObj

	buttonOK 		*GoButtonObj	// IDOK = 1
	buttonCancel 	*GoButtonObj	// IDCANCEL = 2
	buttonAbort 	*GoButtonObj	// IDABORT = 3
	buttonRetry 	*GoButtonObj	// IDRETRY = 4
	buttonIgnore 	*GoButtonObj	// IDIGNORE = 5
	buttonYes 		*GoButtonObj	// IDYES = 6
	buttonNo 		*GoButtonObj	// IDNO = 7
	buttonClose 	*GoButtonObj	// IDCLOSE = 8
	buttonHelp 		*GoButtonObj	// IDHELP = 9
	buttonTryAgain 	*GoButtonObj	// IDTRYAGAIN = 10
	buttonContinue 	*GoButtonObj	// IDCONTINUE = 11

	buttonId 		int
	
}

func (w *GoMessageBoxObj) Caption() string {
	return w.caption
}

// Overide goWidget.Create() function for window
func (w *GoMessageBoxObj) Create() (*goWidget, error) {
	var hWnd w32.HWND
	var err error
	
	hWnd, err = w32.CreateWindowExStr(
		w.goWidget.exStyle,
		w.goWidget.className,
		w.goWidget.windowName,
		w.goWidget.style,
		w.goWidget.x, w.goWidget.y, w.goWidget.width, w.goWidget.height,
		w.goWidget.hWndParent,
		w32.HMENU(0),
		w.goWidget.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("goWidget id = ", w.goWidget.id)
		log.Println("Failed to create widget.")
	} else {
		w.goWidget.hWnd = hWnd
		// Register Window with parent.
		//Top Level Window does not have parent.
		if w.goWidget.parent != nil {
			w.goWidget.parent.widgets[w.goWidget.id] = &(w.goWidget)
		}
		//w.ch = make(chan int)

	}
	return &(w.goWidget), err
}

func (w *GoMessageBoxObj) Exec() (int) {
	log.Println("GoMessageBoxObj::Exec()")

	if w.goWidget.parent != nil {
		log.Println("GoMessageBoxObj::Parent X, Width:", w.goWidget.parent.X(), " ,", w.goWidget.parent.Width())
		log.Println("GoMessageBoxObj::Parent Y, Height:", w.goWidget.parent.Y(), " ,", w.goWidget.parent.Height())
		midX := w.goWidget.parent.X() + (w.goWidget.parent.Width() / 2)
		midY := w.goWidget.parent.Y() + (w.goWidget.parent.Height() / 2)
		log.Println("GoMessageBoxObj::Parent Mid X Y:", midX, " ,", midY)
		log.Println("GoMessageBoxObj::Width:, Height", w.Width(), " ,", w.Height())
		x := midX - (w.Width() / 2)
		y := midY - (w.Height() / 2)

		log.Println("GoMessageBoxObj::SetPosition:", x, " ,", y)
		w.SetPosition(x, y)
		w.Show()
		
		w32.EnableWindow(w.goWidget.hWndParent, false)
		log.Println("Exec(): waiting")

		response := w.run()
		
		log.Println("GoMessageBoxObj::run(response:", response, ")")
		w32.EnableWindow(w.goWidget.hWndParent, true)
		w.Hide()
		return response
	}
	return 0
}

func (w *GoMessageBoxObj) run() (int) {
	var msg w32.MSG
	log.Println("GoMessageBoxObj running")
	log.Print("w.goWidget.hWnd:", w.goWidget.hWnd)
	w.buttonId = -1
	for w.buttonId < 0 {
		//log.Println(".")
		msg = w32.MSG{}

		gotMessage, err := w32.GetMessage(&msg, 0, 0, 0)
		
		if gotMessage == 0 {
			log.Println("Run::wParam:", int(msg.WParam))
			//w.buttonId = int(msg.WParam)
			return int(msg.WParam)
		} else if gotMessage == -1 {
			log.Println("GetMessage Error:", err)
			//w.buttonId = -1
			return -1
		}
		//log.Println("hWnd:", msg.Hwnd, "Message:", msg.Message)

		w32.TranslateMessage(&msg)
		w32.DispatchMessage(&msg)
	}
	return w.buttonId
}



func (w *GoMessageBoxObj) CaptionBox() (*GoLabelObj) {
	return w.captionLabel
}

func (w *GoMessageBoxObj) LayoutStyle() (GoLayoutStyle) {
	return w.layout.Style()
}

func (w *GoMessageBoxObj) Layout() (*GoLayoutObj) {
	return w.layout
}

func (w *GoMessageBoxObj) Message() (string) {
	return w.message
}

func (w *GoMessageBoxObj) MessageBox() (*GoLabelObj) {
	return w.messageLabel
}

func (w *GoMessageBoxObj) SetCaption(caption string) {
	w.caption = caption
	w.captionLabel.SetText(caption)
}

func (w *GoMessageBoxObj) SetCaptionColor(color GoColor) {
	w.captionLabel.SetTextColor(color)
}

func (w *GoMessageBoxObj) SetLayoutStyle(style GoLayoutStyle) {
	w.layout.SetStyle(style)
}

func (w *GoMessageBoxObj) SetMessage(message string) {
	w.message = message
	w.messageLabel.SetText(message)
}

func (w *GoMessageBoxObj) SetMessageColor(color GoColor) {
	w.messageLabel.SetTextColor(color)
}

func (w *GoMessageBoxObj) SetTitle(title string) {
	w.title = title
	if w.hWnd != 0 {
		w32.SetWindowText(w.hWnd, title)
	}
}

func (w *GoMessageBoxObj) TextOut(text string) {
	w.text = w.text + text
	w32.InvalidateRect(w.hWnd, nil, true)
}

func (w *GoMessageBoxObj) Title() (string) {
	return w.title
}

func (w *GoMessageBoxObj) Update() {
	w32.UpdateWindow(w.hWnd)
}

func (w *GoMessageBoxObj) onWM_COMMAND( wParam uintptr, lParam uintptr) {
	log.Println("GoMessageBox::onWM_COMMAND:")
	wParamH := (wParam & 0xFFFF0000) >> 16
	wParamL := wParam & 0xFFFF
	lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF
	log.Println("GoMessageBox::wParamH:", wParamH)
	log.Println("GoMessageBox::wParamL:", wParamL)
	log.Println("GoMessageBox::lParamH:", lParamH)
	log.Println("GoMessageBox::lParamL:", lParamL)
	if lParam != 0 {
		// control clicked
		index := wParamL	// controlID
		cmd := wParamH		// BN_CLICKED
		if cmd == w32.BN_CLICKED {
			log.Println("GoMessageBox::w32.BN_CLICKED..............")
			log.Println("GoMessageBox::Comtrolindex:", index)
			// get widget from GoApp.winStack using controlID and trigger function widget.onClick
			// which in turn triggers function button.onClick()
			w.buttonId = GoApp().Controls(index).groupId()
			log.Println("GoMessageBox::index:", w.buttonId)
		}
	} else {
		if wParamH == 0 {
			// low word of w contains menu ID
			/*id := int(wParamL)
			if goApp.Actions(id).OnClick() != nil {
				goApp.Actions(id).onClick()
			}*/

			

		} else if wParamH == 1 {
				// low word of w contains accelerator ID
			//index := int(wParamL)
			/*if f := w.shortcuts[index].f; f != nil {
				f()
			}*/
		}

	}
}

func (w *GoMessageBoxObj) onWM_DRAWITEM(wParam uintptr, lParam uintptr) {
}

func (w *GoMessageBoxObj) onWM_NOTIFY(wParam uintptr, lParam uintptr) {
}

func (w *GoMessageBoxObj) destroy() () {
	for _, ctl := range w.controls {
		ctl = nil	// delete control structure
		w.removeControl(ctl)
		//delete(w.controls, id)	// remove from Control Stack
	}
}

func (w *GoMessageBoxObj) groupId() (id int) {
	return 0
}

func (w *GoMessageBoxObj) isDialog() (bool) {
	return true
}

func (w *GoMessageBoxObj) isLayout() (bool) {
	return false
}

func (w *GoMessageBoxObj) objectType() (string) {
	return "GoMessageBoxObj"
}

func (w *GoMessageBoxObj) wid() (*goWidget) {
	return &w.goWidget
}