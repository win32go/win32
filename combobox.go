/* win32go/win32/combobox.go */

package win32

import (
	"log"
	"syscall"
	"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoComboBox(parent GoObject) (hComboBox *GoComboBoxObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 
	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 		"COMBOBOX",
		windowName: 	"",
		style: 			w32.WS_CHILD | w32.CBS_AUTOHSCROLL | w32.CBS_DROPDOWN,
		exStyle:    	0,
		state:      	w32.SW_HIDE,
		x:      		0,
		y:      		0,
		width:  		400,
		height: 		300,
		hWndParent:		p_widget.hWnd,
		id:				p_nextid,
		instance:		p_widget.instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		border: 		BorderNone,
		cursor:     	nil,	// *goCursor
		font:       	nil, 	// *GoFont
		margin:			&GoMarginType{2, 2, 2, 2},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 		"",
		window:     	false,
		widgets:    	map[int]*goWidget{},
		alpha:			0, 		//uint8
		onShow:        	nil, 	//func()
		onClose:       	nil, 	//func()
		onCanClose:    	nil, 	//func() bool
		onMouseMove:   	nil, 	//func(x, y int)
		onMouseWheel:  	nil, 	//func(x, y int, delta float64)
		onMouseDown:   	nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     	nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     	nil, 	//func(key int)
		onKeyUp:       	nil, 	//func(key int)
		onResize:      	nil, 	//func()
	}
	object := goObject{parent, []GoObject{}}
	c := &GoComboBoxObj{widget, object, 0, make([]string, 0), 0, nil}
	
	_, err := c.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(c)
		GoApp().AddControl(c, p_nextid)
	}
	// Subclass Label Window to allow custom painting of label
	//l.SetBorderStyle(PanelBorderRaised)	
	if sizeToView == true {
		GoTestView.SizeToControl(c)
	}
	return c
}

type GoComboBoxObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	groupID			int
	items 			[]string
	selected 		int
	onChange func(newIndex int)
}

func (c *GoComboBoxObj) Font() *GoFont {
	return c.font
}

func (c *GoComboBoxObj) AddItem(s string) {
	c.items = append(c.items, s)
	if c.hWnd != 0 {
		c.addItem(s)
	}
}

func (c *GoComboBoxObj) Clear() {
	c.items = nil
	if c.hWnd != 0 {
		w32.SendMessage(c.hWnd, w32.CB_RESETCONTENT, 0, 0)
	}
}

func (c *GoComboBoxObj) Items() []string {
	return c.items
}

func (c *GoComboBoxObj) SetItems(items []string) {
	c.items = items
	if c.hWnd != 0 {
		w32.SendMessage(c.hWnd, w32.CB_RESETCONTENT, 0, 0)
		for _, s := range c.items {
			c.addItem(s)
		}
	}
}

func (c *GoComboBoxObj) SelectedIndex() int {
	if c.hWnd != 0 {
		c.selected = int(w32.SendMessage(c.hWnd, w32.CB_GETCURSEL, 0, 0))
	}
	return c.selected
}

// SetSelectedIndex sets the current index. Set -1 to remove any selection and
// make the combo box empty. If you pass a value < -1 it will be set to -1
// instead. The index is not clamped to the number of items, you may set the
// index 10 where only 5 items are set. This lets you set the index and items at
// design time without one invalidating the other. At runtime though,
// SelectedIndex() will return -1 if you set an invalid index.
func (c *GoComboBoxObj) SetSelectedIndex(i int) {
	if i < -1 {
		i = -1
	}
	c.selected = i
	if c.hWnd != 0 {
		w32.SendMessage(c.hWnd, w32.CB_SETCURSEL, uintptr(i), 0)
	}
}

func (c *GoComboBoxObj) SetOnChange(f func(newIndex int)) {
	c.onChange = f
}



func (c *GoComboBoxObj) SetFont(font *GoFont) {
	c.font = font
	if c.hWnd != 0 {
		w32.SendMessage(c.hWnd, w32.WM_SETFONT, uintptr(c.fontHandle()), 1)
	}
}

func (c *GoComboBoxObj) SetText(text string) {
	c.text = text
	if c.hWnd != 0 {
		w32.SetWindowText(c.hWnd, text)
	}
}

func (c *GoComboBoxObj) Show() {
	log.Println("GoComboBoxObj.Show()")
	w32.ShowWindow(c.hWnd, w32.SW_SHOW)
	c.state = w32.SW_SHOW
	c.visible = true
	parent := c.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (c *GoComboBoxObj) Text() string {
	if c.hWnd != 0 {
		c.text = w32.GetWindowText(c.hWnd)
	}
	return c.text
}

func (c *GoComboBoxObj) addItem(s string) {
	ptr, _ := syscall.UTF16PtrFromString(s)
	w32.SendMessage(c.hWnd, w32.CB_ADDSTRING, 0, uintptr(unsafe.Pointer(ptr)))
}

func (c *GoComboBoxObj) destroy() {
	for _, ctl := range c.controls {
		ctl = nil	// delete control structure
		c.removeControl(ctl)
		//delete(f.controls, id)
	}
}

func (c *GoComboBoxObj) fontHandle() w32.HFONT {
	if c.font != nil {
		return c.font.handle
	}
	/*if e.parent != nil {
		font := e.parent.Font()
		if font != nil {
			return font.handle
		}
	}*/
	return 0
}

func (c *GoComboBoxObj) groupId() (id int) {
	return c.groupID
}

func (c *GoComboBoxObj) handleNotification(cmd uintptr) {
	if cmd == w32.CBN_SELCHANGE && c.onChange != nil {
		c.onChange(c.SelectedIndex())
	}
}

func (c *GoComboBoxObj) isDialog() (bool) {
	return false
}

func (c *GoComboBoxObj) isLayout() (bool) {
	return false
}

func (c *GoComboBoxObj) objectType() (string) {
	return "GoComboBoxObj"
}

func (c *GoComboBoxObj) wid() (*goWidget) {
	return &c.goWidget
}