/* object.go */

package win32

import (
	"log"
	"gitlab.com/win32go/win32/w32"
)



type goObject struct {
	parent GoObject
	//controls  map[int]GoObject
	controls []GoObject
}



func (ob *goObject) addControl(control GoObject) {
	//ob.controls[id] = control
	ob.controls = append(ob.controls, control)
	//ob.index = append(object.index, id)
}

/*func (ob *goObject) control(id int) (GoObject) {
	return ob.controls[id]
}*/

/*func (ob *goObject) mainWindow() (GoObject) {
	obj := *ob
	for {
		if obj.parent == nil {
			break
		}
		obj = obj.parent
	}
	return obj	
}*/

func (ob *goObject) drawItem(drawItemStruct *w32.DRAWITEMSTRUCT) {

}


func (ob *goObject) handleNotification(cmd uintptr) {
	log.Println("GoObject::handleNotification()")
}

func (ob *goObject) onWM_COMMAND(w, l uintptr) {
	ob.parentControl().onWM_COMMAND(w, l)
}


func (ob *goObject) onWM_DRAWITEM(w, l uintptr) {
	ob.parentControl().onWM_DRAWITEM(w, l)
}

func (ob *goObject) onWM_NOTIFY(w, l uintptr) {
	ob.parentControl().onWM_NOTIFY(w, l)
}

func (ob *goObject) parentControl() (GoObject) {
	return ob.parent
}

func (ob *goObject) removeControl(object GoObject) {
	k := 0
	for _, v := range ob.controls {
	    if v != object {
	        ob.controls[k] = v
	        k++
	    }
	}
	ob.controls = ob.controls[:k] // set slice len to remaining elements
}

/*func (ob *goObject) removeControl(id int) {
	delete(ob.controls, id)
}*/


	
