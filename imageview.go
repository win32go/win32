/* view.go */

package win32

import (
	"log"
	"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoView(parent GoObject) (hView *GoViewObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
		//case "GoLayoutObj":
			//parent = parent
	} 

	p_nextid := nextControlId()
	p_widget := parent.wid()

	widget := goWidget{
		className: 	"STATIC",
		windowName: "",
		style: 		w32.WS_CHILD | w32.SS_OWNERDRAW, // | w32.WS_THICKFRAME,
		exStyle:    0,
		state:      w32.SW_HIDE,
		x:      	0,
		y:      	0,
		width:  	400,
		height: 	300,
		hWndParent:	p_widget.hWnd,
		id:			p_nextid,
		instance:	p_widget.instance,
		param:		0,
		hWnd: 		0,
		parent:  	p_widget,
		disabled: 	false,
		visible:   	false,
		border: 	BorderNone,
		cursor:     nil,	// *goCursor
		font:       nil, 	// *goFont
		margin:		&GoMarginType{0, 0, 0, 0},
		padding:	&GoPaddingType{0, 0, 0, 0},
		sizePolicy:	&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 	"",
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		onShow:        nil, 	//func()
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}

	object := goObject{parent, []GoObject{}}
	v := &GoViewObj{widget, object, nil, nil}
	
	_, err := v.Create()

	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(v)
		GoApp().AddControl(v, p_nextid)
		v.layout = GoLayout(v, NoLayout)
		v.layout.Show()
	}
	// Subclass Frame Window to allow passage of messages back up to top level windows
	w32.SetWindowSubclass(v.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		case w32.WM_PAINT:
			log.Println("GoView:WM_PAINT.......")
			var ps w32.PAINTSTRUCT
			hdc := w32.BeginPaint(hWnd, &ps)
		    rect := v.GetClientRect()
		    pt := CreatePainter(hdc, rect)
		    pt.SetStockBrush(v.backcolor)
		    pt.SetNullPen()
		    log.Println("dest.X =", rect.X(), "dest.Y =", rect.Y())
		    log.Println("dest.Width =", rect.Width(), "dest.Height =", rect.Height())
		    pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
		    pt.DrawImage(v.image, rect, 0, 0)
		    //w32.DrawFocusRect(hdc, rect.W32RECT())
		    pt.Destroy()
		    w32.EndPaint(hWnd, &ps)
			return 0
		case w32.WM_COMMAND:
			log.Println("GoView:WM_COMMAND")
			v.onWM_COMMAND(wParam, lParam)
			return 0
		case w32.WM_DRAWITEM:
			log.Println("GoView:WM_DRAWITEM")
			v.onWM_DRAWITEM(wParam, lParam)
			return 0
		case w32.WM_NOTIFY:
			log.Println("GoView:WM_NOTIFY")
			v.onWM_NOTIFY(wParam, lParam)
			return 0
		case w32.WM_SIZE:
			log.Println("GoView:WM_SIZE.....")
			layout := v.Layout()
			rc := w32.GetClientRect(v.hWnd)
			layout.SetSize(int(rc.Right), int(rc.Bottom))
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		default:
			return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
		}
	}), 0, 0)
	//p.SetBorderStyle(BorderRaised)
	if sizeToView == true {
		GoTestView.SizeToControl(v)
	}
	return v
}

type GoViewObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	layout		*GoLayoutObj
	image 		*GoImageObj
}

func (v *GoViewObj) AddControl(control GoObject) {
	v.layout.AddControl(control)
}

func (v *GoViewObj) AddLayout(style GoLayoutStyle) (*GoLayoutObj) {
	layout := GoLayout(v.layout, style)
	return layout
}

func (v *GoViewObj) AddImage(image *GoImageObj) {
	v.image = image

}

func (v *GoViewObj) Layout() (layout *GoLayoutObj) {
	return v.layout
}

func (v *GoViewObj) LayoutMarginSetLeft(left int) {
	v.layout.margin.Left = left
}

func (v *GoViewObj) LayoutMarginSetTop(top int) {
	v.layout.margin.Top = top
}

func (v *GoViewObj) LayoutMarginSetRight(right int) {
	v.layout.margin.Right = right
}

func (v *GoViewObj) LayoutMarginSetBottom(bottom int) {
	v.layout.margin.Bottom = bottom
}

func (v *GoViewObj) LayoutPaddingSetLeft(left int) {
	v.layout.padding.Left = left
}

func (v *GoViewObj) LayoutPaddingSetTop(top int) {
	v.layout.padding.Top = top
}

func (v *GoViewObj) LayoutPaddingSetRight(right int) {
	v.layout.padding.Right = right
}

func (v *GoViewObj) LayoutPaddingSetBottom(bottom int) {
	v.layout.padding.Bottom = bottom
}

func (v *GoViewObj) LayoutStyle() (style GoLayoutStyle) {
	return v.layout.Style()
}

func (v *GoViewObj) SetLayoutHeight(height int) {
	v.layout.height = height
	v.layout.setGeometry()
}

func (v *GoViewObj) SetLayoutMargin(left int, top int, right int, bottom int) {
	v.layout.margin = &GoMarginType{left, top, right, bottom}
}

func (v *GoViewObj) SetLayoutPadding(left int, top int, right int, bottom int) {
	v.layout.padding = &GoPaddingType{left, top, right, bottom}
}

func (v *GoViewObj) SetLayoutSizePolicy(horiz GoSizeType, vert GoSizeType, fixed bool) {
	sizePolicy := GetSizePolicy(horiz, vert, fixed)
	v.layout.sizePolicy = sizePolicy
}

func (v *GoViewObj) SetLayoutStyle(style GoLayoutStyle) {
	v.layout.SetStyle(style)
}

func (v *GoViewObj) SetLayoutWidth(width int) {
	v.layout.width = width
	v.layout.setGeometry()
}

func (v *GoViewObj) Show() {
	log.Println("GoViewObj.Show()")
	w32.ShowWindow(v.hWnd, w32.SW_SHOW)
	v.state = w32.SW_SHOW
	v.visible = true
	parent := v.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}

}

func (v *GoViewObj) Update() {
	w32.UpdateWindow(v.layout.hWnd)
}

func (v *GoViewObj) destroy() {
	for _, ctl := range v.controls {
		ctl = nil	// delete control structure
		v.removeControl(ctl)
		//delete(v.controls, id)
	}
}

func (v *GoViewObj) drawItem(drawItemStruct *w32.DRAWITEMSTRUCT) {

}

func (v *GoViewObj) groupId() (id int) {
	return 0
}

func (v *GoViewObj) handleNotification(cmd uintptr) {
}

func (v *GoViewObj) isDialog() (bool) {
	return false
}

func (v *GoViewObj) isLayout() (bool) {
	return false
}

func (v *GoViewObj) objectType() (string) {
	return "GoViewObj"
}

func (v *GoViewObj) wid() (*goWidget) {
	return &v.goWidget
}



