/* win32go/win32/progressbar.go */

package win32

import (
	"log"
	"reflect"
	//"runtime"
	//"syscall"
	//"unsafe"
	"gitlab.com/win32go/win32/w32"
)

const maxProgressBarSteps = 10000
const PB_Marquee = "marquee"
const PB_Vertical = true

// GoProgressBar(parent GoObject, marquee string, totalSteps int, vertical bool)
func GoProgressBar(parent GoObject, args ...interface{}) (hProgressBar *GoProgressBarObj) {
	var className string
	var style uint32
	var sizeToView bool
	var marquee bool 		 // default false
	var totalSteps int = 100 // default 100
	var vertical bool		 //default false

	style = w32.WS_CHILD | w32.PBS_SMOOTH
	for i, v := range args {
		log.Println("GoProgressBar() - arg:", i, "value:", v)
		switch v := reflect.ValueOf(v); v.Kind() {
		case reflect.String:
			log.Println("GoProgressBar() - v.String():", v.String())
			if v.String() == PB_Marquee {
				marquee = true
				style |= w32.PBS_MARQUEE
			}
		case reflect.Int:
			log.Println("GoProgressBar() - v.Uint32():", v.Int())
			totalSteps = int(v.Int())
		case reflect.Bool:
			log.Println("GoProgressBar() - v.Bool():", v.Bool())
			vertical = v.Bool()
			if vertical == true {
				style |= w32.PBS_VERTICAL
			}
		default:
			log.Println("GoProgressBar() - Not Marquee flag, totalSteps or vertical flag")
		}
	}

	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	}

	p_nextid := nextControlId()
	p_widget := parent.wid()
	/*if runtime.GOARCH == "amd64" {
		className = "msctls_progress"
	} else if runtime.GOARCH == "win32" {
		className = "msctls_progress32"
	}*/
	className = "msctls_progress32"
	log.Println("className =", className)

	widget := goWidget{
		className: 		className,
		windowName: 	"",
		style: 			style,
		exStyle:    	0,
		state:      	w32.SW_HIDE,
		x:      		0,
		y:      		0,
		width:  		400,
		height: 		24,
		hWndParent:		p_widget.hWnd,
		id:				p_nextid,
		instance:		p_widget.instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		border: 		BorderNone,
		cursor:     	nil,	// *goCursor
		font:       	nil, 	// *goFont
		margin:			&GoMarginType{2, 2, 2, 2},
		padding:		&GoPaddingType{0, 0, 0, 0},
		sizePolicy:		&GoSizePolicy{ST_ExpandingWidth, ST_FixedHeight, true},
		text: 	 		"",
		window:     	false,
		widgets:    	map[int]*goWidget{},
		alpha:			0, 		//uint8
		onShow:        	nil, 	//func()
		onClose:       	nil, 	//func()
		onCanClose:    	nil, 	//func() bool
		onMouseMove:   	nil, 	//func(x, y int)
		onMouseWheel:  	nil, 	//func(x, y int, delta float64)
		onMouseDown:   	nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     	nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     	nil, 	//func(key int)
		onKeyUp:       	nil, 	//func(key int)
		onResize:      	nil, 	//func()
	}
	object := goObject{parent, []GoObject{}}
	p := &GoProgressBarObj{widget, object, 0, marquee, totalSteps, 0, vertical}

	_, err := p.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p)
		GoApp().AddControl(p, p_nextid)
	}

	if sizeToView == true {
		GoTestView.SizeToControl(p)
	}
	return p
}

type GoProgressBarObj struct {
	goWidget
	goObject
	//border 		PanelBorderStyle
	groupID			int
	marquee 		bool
	totalSteps 		int
	progress        int
	vertical     	bool
}

func (p *GoProgressBarObj) GetBackgroundColor() (GoColor) {
	color := w32.SendMessage(p.hWnd, w32.PBM_GETBKCOLOR, uintptr(0), uintptr(0))
	return colorFromW32(w32.COLORREF(color))
}

func (p *GoProgressBarObj) Jump(steps int) {
	if !p.marquee {
		p.progress = p.progress + steps
		if p.progress > p.totalSteps {
			p.progress = p.totalSteps
		} 
		w32.SendMessage(p.hWnd, w32.PBM_DELTAPOS, uintptr(steps), 0)
	}
}

func (p *GoProgressBarObj) MarqueeStyle() bool {
	return p.marquee
}

func (p *GoProgressBarObj) Progress() int {
	return p.progress
}

func (p *GoProgressBarObj) Reset() {
	p.progress = 0
	if !p.marquee {
		w32.SendMessage(p.hWnd, w32.PBM_SETPOS, uintptr(0), uintptr(0))
	}
}

func (p *GoProgressBarObj) SetBackgroundColor(color GoColor) {
	w32.SendMessage(p.hWnd, w32.PBM_SETBKCOLOR, uintptr(0), uintptr(colorToW32(color)))
}

func (p *GoProgressBarObj) SetBarColor(color GoColor) {
	w32.SendMessage(p.hWnd, w32.PBM_SETBARCOLOR, uintptr(0), uintptr(colorToW32(color)))
}

func (p *GoProgressBarObj) SetProgress(progress int) {
	if progress < 0 {
		progress = 0
	}
	if progress > p.totalSteps {
		progress = p.totalSteps
	}
	p.progress = progress
	if !p.marquee {
		w32.SendMessage(p.hWnd, w32.PBM_SETPOS, uintptr(progress), 0)
	}
}

func (p *GoProgressBarObj) SetTotalSteps(totalSteps int) {
	if totalSteps < 0 {
		totalSteps = 0
	}
	if totalSteps > maxProgressBarSteps {
		totalSteps = maxProgressBarSteps
	}
	p.totalSteps = totalSteps
	if !p.marquee {
		w32.SendMessage(p.hWnd, w32.PBM_SETRANGE32, uintptr(0), uintptr(totalSteps))
	}
}

func (p *GoProgressBarObj) ShowMarquee() {
	if p.marquee {
		w32.SendMessage(p.hWnd, w32.PBM_SETMARQUEE, uintptr(1), uintptr(0))
	}
}

func (p *GoProgressBarObj) Step() {
	if !p.marquee {
		if p.progress < p.totalSteps {
			p.progress++
			w32.SendMessage(p.hWnd, w32.PBM_STEPIT, uintptr(0), uintptr(0))
		}
	}
}

func (p *GoProgressBarObj) TotalSteps() int {
	return p.totalSteps
}

func (p *GoProgressBarObj) VerticalStyle() bool {
	return p.vertical
}

func (p *GoProgressBarObj) destroy() {
	for _, ctl := range p.controls {
		ctl = nil	// delete control structure
		p.removeControl(ctl)
		//delete(f.controls, id)
	}
}

func (p *GoProgressBarObj) groupId() (id int) {
	return p.groupID
}

func (p *GoProgressBarObj) isDialog() (bool) {
	return false
}

func (p *GoProgressBarObj) isLayout() (bool) {
	return false
}

func (p *GoProgressBarObj) objectType() (string) {
	return "GoProgressBarObj"
}

func (p *GoProgressBarObj) wid() (*goWidget) {
	return &p.goWidget
}