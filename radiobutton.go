/* radiobutton.go */

package win32

import (
	"log"
	"syscall"
	"unsafe"
	"gitlab.com/win32go/win32/w32"
)

func GoRadioButton(parent GoObject, text string) (hRadioButton *GoRadioButtonObj) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}
	// If parent control is a container then add the controls to the container layout
	// to facilitate widget layout
	switch parent.objectType() {
		case "GoWindowObj":
			parent = parent.(*GoWindowObj).layout
		case "GoPanelObj":
			parent = parent.(*GoPanelObj).layout
	} 

	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{
		className: 	"BUTTON",
		windowName: text,
		style: 		w32.WS_CHILD | w32.WS_TABSTOP | w32.BS_AUTORADIOBUTTON | w32.BS_OWNERDRAW | w32.BS_NOTIFY,
		exStyle:    0,
		state:      w32.SW_HIDE,
		x:      	0,
		y:      	0,
		width:  	100,
		height: 	20,
		hWndParent:	p_widget.hWnd,
		id:			p_nextid,
		instance:	p_widget.instance,
		param:		0,
		hWnd: 		0,
		parent:  	p_widget,
		disabled: 	false,
		visible:   	false,
		autoSize:	false,
		border:		BorderNone,
		cursor:     nil,	// *goCursor
		font:       nil, 	// *goFont
		margin:		&GoMarginType{5, 5, 5, 5},
		padding:	&GoPaddingType{2, 2, 2, 2},
		sizePolicy:	&GoSizePolicy{ST_FixedWidth, ST_FixedHeight, true},
		text: 	 	text,
		window:     false,
		widgets:    map[int]*goWidget{},
		alpha:		0, 		//uint8
		backgroundMode:	TransparentMode,
		onShow:        nil, 	//func()
		onClose:       nil, 	//func()
		onCanClose:    nil, 	//func() bool
		onMouseMove:   nil, 	//func(x, y int)
		onMouseWheel:  nil, 	//func(x, y int, delta float64)
		onMouseDown:   nil, 	//func(button MouseButton, x, y int)
		onMouseUp:     nil, 	//func(button MouseButton, x, y int)
		onKeyDown:     nil, 	//func(key int)
		onKeyUp:       nil, 	//func(key int)
		onResize:      nil, 	//func()
	}
	//c.textControl.create(id, 0, "BUTTON", w32.WS_TABSTOP|w32.BS_AUTOCHECKBOX)
	//w32.SendMessage(c.handle, w32.BM_SETCHECK, toCheckState(c.checked), 0)
									   
	object := goObject{parent, []GoObject{}}
	r := &GoRadioButtonObj{widget, object, 0, false, nil}
	
	_, err := r.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(r)
		GoApp().AddControl(r, p_nextid)
		//w32.SendMessage(r.hWnd, w32.BM_SETCHECK, w32.BST_UNCHECKED, 0)
	}
	// Subclass Label Window to allow custom painting of label
	// Subclass Label Window to allow custom painting of label
	w32.SetWindowSubclass(r.hWnd, syscall.NewCallback(func(
		hWnd w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		case w32.WM_ERASEBKGND:
			log.Println("RadioButton::WM_ERASEBACKGROUND - ")
			//widget := win.wid()
			rect := r.GetClientRect()
			hDC := w32.HDC(wParam)
			r.paintBackground(hDC, rect)
			return 1

		case w32.WM_PAINT:
			//log.Println("GoRadioButton:WM_PAINT.......")
			//log.Println("mousehover =", r.mousehover)
			//log.Println("mousedown =", r.mousedown)
			var ps w32.PAINTSTRUCT
			hDC := w32.BeginPaint(hWnd, &ps)
			rect := r.GetClientRect()
			rc := rect.DeductPadding(r.padding)
			pt := CreatePainter(hDC, rect)
			if r.backgroundMode == OpaqueMode {
				pt.SetStockBrush(r.backcolor)
				pt.SetStockPen(r.backcolor)
			} else {
				pt.SetNullBrush()
				pt.SetNullPen()
			}
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())

		    pt.SetStockPen(r.buttonText)
		    pt.SetNullBrush()
		    pt.DrawEllipse(rc.left + 2, rc.top + 2, rc.bottom - rc.top - 4, rc.bottom - rc.top - 4)
		    if r.checked {
		    	pt.SetStockBrush(r.buttonText)
		    } else {
		    	pt.SetStockPen(r.backcolor)
		    	pt.SetStockBrush(r.backcolor)
		    }
		    pt.DrawEllipse(rc.left + 6, rc.top + 6, rc.bottom - rc.top - 12, rc.bottom - rc.top - 12)
		    rc.SetLeft(rc.left + rc.bottom - rc.top + 5)
		    pt.SetBackMode(TransparentMode)
			if r.mousehover {
				if r.mousedown {
					pt.SetTextColor(r.buttonText)
				} else {
					pt.SetTextColor(r.highlight)
				}
			}
			pt.DrawTextLine(r.text, rc, w32.DT_CENTER, w32.DT_VCENTER)  
			//w32.DrawFocusRect(hdc, rect.W32RECT())
			pt.Destroy()
		    w32.EndPaint(hWnd, &ps)
		    return 0
		case w32.WM_MOUSEMOVE:          
			if !r.mousehover {
				//log.Println("GoRadioButton:WM_MOUSEHOVER.......")
			    r.mousehover = true
			    w32.InvalidateRect(r.hWnd, nil, true)
			    //SendMessage(hWnd, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)hBmpHiLight );
			    tme := w32.TRACKMOUSEEVENT{}
			    tme.CbSize = uint32(unsafe.Sizeof(tme))
			    tme.DwFlags = w32.TME_LEAVE
			    tme.HwndTrack = r.hWnd
			    tme.DwHoverTime = 400
			    w32.TrackMouseEvent(&tme)

			}
			return 0
        case w32.WM_MOUSELEAVE:
        	//log.Println("GoRadioButton:WM_MOUSELEAVE.......")
			r.mousehover = false
			w32.InvalidateRect(r.hWnd, nil, true)
			return 0			
		}
		return w32.DefSubclassProc(hWnd, msg, wParam, lParam)
	}), 0, 0)


	if sizeToView == true {
		GoTestView.SizeToControl(r)
	}
	return r
}

type GoRadioButtonObj struct {
	goWidget
	goObject
	groupID  int
	//autoSize bool
	checked  bool
	onChange func(bool)
}

func (r *GoRadioButtonObj) AutoSize() (autoSize bool) {
	return r.autoSize
}

func (r *GoRadioButtonObj) Checked() bool {
	return r.checked
}

func (r *GoRadioButtonObj) SetAutoSize(autoSize bool) {
	r.autoSize = autoSize
	r.setGeometry()
}

func (r *GoRadioButtonObj) SetChecked(checked bool) {
	if checked == r.checked {
		return
	}
	r.checked = checked
	if r.hWnd != 0 {
		w32.SendMessage(r.hWnd, w32.BM_SETCHECK, toCheckState(r.checked), 0)
	}
	if r.onChange != nil {
		r.onChange(r.checked)
	}
	return
}

// Declared in GoCheckBox Module
/*func toCheckState(checked bool) uintptr {
	if checked {
		return w32.BST_CHECKED
	}
	return w32.BST_UNCHECKED
}*/

func (r *GoRadioButtonObj) SetGroupId(groupId int) {
	r.groupID = groupId
}

func (r *GoRadioButtonObj) SetOnChange(f func(checked bool)) {
	r.onChange = f
}

func (r *GoRadioButtonObj) Show() {
	w32.ShowWindow(r.hWnd, w32.SW_SHOW)
	r.state = w32.SW_SHOW
	r.visible = true
	parent := r.parentControl()
	for {
		
		log.Println("parent.objectType() ==", parent.objectType())
		if parent == nil {
			break
		}
		if parent.objectType() == "GoLayoutObj" {
			parent.(*GoLayoutObj).onWM_SIZE(0, 0)
			break
		}
		parent = parent.parentControl()
	}
}

func (r *GoRadioButtonObj) destroy() {
	/*for id, control := range w.controls {
		delete(w.controls, id)
	}*/
}

func (r *GoRadioButtonObj) groupId() (id int) {
	return r.groupID
}

func (r *GoRadioButtonObj) isDialog() (bool) {
	return false
}

func (r *GoRadioButtonObj) isLayout() (bool) {
	return false
}

func (r *GoRadioButtonObj) objectType() (string) {
	return "GoRadioButtonObj"
}

func (r *GoRadioButtonObj) wid() (*goWidget) {
	return &r.goWidget
}

func (r *GoRadioButtonObj) drawItem(s *w32.DRAWITEMSTRUCT) {
	log.Println("GoRadioButtonObj::drawItemStruct..............")
	/*state := s.ItemState
	if (state & w32.ODS_SELECTED) == 1 {
		r.mousedown = true
	} else {
		r.mousedown = false
	}
	hDC := s.HDC
	w32.SetBkMode(hDC, w32.TRANSPARENT)
	rect := GoRect().FromW32Rect(&s.RcItem)
	rc := rect.DeductPadding(r.padding)
	pt := CreatePainter(hDC, rect)
	pt.SetTextColor(r.buttonText)
	pt.SetNullPen()
			
	if r.mousedown {
		pt.SetTextColor(r.buttonText)
	} else {
		pt.SetTextColor(r.highlight)
	}
			
    pt.DrawRect(rect.left, rect.top, rect.right, rect.bottom)
    pt.SetStockPen(r.buttonText)
    pt.DrawEllipse(rc.left + 2, rc.top + 2, rc.bottom - rc.top - 4, rc.bottom - rc.top - 4)
    log.Println("GoRadioButton::checked", r.checked)
    if r.checked {
    	pt.SetStockBrush(r.buttonText)
    	pt.DrawEllipse(rc.left + 6, rc.top + 6, rc.bottom - rc.top - 12, rc.bottom - rc.top - 12)
    }
    rc.SetLeft(rc.left + rc.bottom - rc.top + 5)
	pt.DrawTextLine(r.text, rc, w32.DT_CENTER, w32.DT_VCENTER)  
	//w32.DrawFocusRect(hdc, rect.W32RECT())
	pt.Destroy()*/
}

func (r *GoRadioButtonObj) handleNotification(cmd uintptr) {
	log.Println("GoRadioButtonControl::handleNotification..............")
	if cmd == w32.BN_CLICKED {
		r.checked = !r.checked
		r.Refresh()
		if r.onChange != nil {
			r.onChange(r.checked)
		}
	}
}